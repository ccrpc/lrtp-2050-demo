---
title: "Data and Models"
date: 2024-11-08
draft: false
weight: 50
bannerHeading: Data and Models
bannerText: >
  Learn more about the CUUATS modeling suite including local transportation,
  land use, emissions, accessibility, safety, and mobility data.

---

{{<image src="CUUATS_modeling_suite_LRTP 2050.jpg"
  alt="Diagram showing the types of software used to develop the future vision. CUUATS staff started with the Travel Demand Model and Urban Sim. These two programs analyze the vehicle miles traveled, mode choice, traffic volume by road link, congestion speed, population projections, employment projections, and areas of future growth. From there, the data collected from the Travel Demand Model and Urban Sim are used in three other programs: Motor Vehicle Emissions Simulator (MOVES), Neighborhood Level Accessibility Analysis (Access Score), and the Safety Forecasting Tool. MOVES calculates GHG emissions, urban/rural emissions, and PM 25 and other emissions. Access Score measures the level of traffic stress, by transportation mode, accessibility score by transportation mode, and the accessibility scores by destination type. Safety Forecasting Tool calculates crash risk level."
  caption="Diagram of the modeling process used by CUUATS staff to develop the Long-Range Transportation Plan 2050 vision. Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

The following links provide additional documentation about the methodologies and input data used for each of the models.

## Travel Demand Model (TDM)

**The [TDM Documentation page](https://ccrpc.gitlab.io/lrtp-2050/appendices/tdm/) details the different components, inputs, outputs, and processes that make up the Champaign County TDM.**

**A brief summary of the main TDM outputs for the LRTP 2050 is included in the [Scenario Modeling](https://ccrpc.gitlab.io/lrtp-2050/vision/model) section of the plan.**

## Land Use Model: UrbanSim

**To learn more about the inputs used in the land use model, visit the [CUUATS land use model site on GitLab](https://gitlab.com/ccrpc/land-use-model/-/wikis/home).** 

**A summary of the main land use model outputs for LRTP 2050 is included in the [Scenario Modeling](https://ccrpc.gitlab.io/lrtp-2050/vision/model/) section of the plan.**

**For more about UrbanSim and its core models, visit the [UrbanSim documentation site](https://udst.github.io/urbansim/).**

## Motor Vehicle Emission Simulator (MOVES)  

**For more information on EPA’s MOVES model, visit [MOtor Vehicle Emission Simulator (MOVES)](https://www.epa.gov/moves).** 

**The CUUATS case study, part of a document published on the EPA’s website titled [Applying TEAM in Regional Sketch Planning,](https://nepis.epa.gov/Exe/ZyPDF.cgi?Dockey=P100VOWM.pdf) finalized in November 2018, looks at ways for Champaign County to realize a significant decrease in greenhouse gas emissions if certain strategies are implemented in the Champaign-Urbana region.**

**A brief summary of the main MOVES outputs for LRTP 2050 is included in the [Scenario Modeling](https://ccrpc.gitlab.io/lrtp-2050/vision/model)section of the plan.**

## Neighborhood Level Accessibility Analysis: Access Score  

**To learn more about the code and documentation for level of traffic stress calculations, visit the [CUUATS LTS site in GitLab.](https://gitlab.com/ccrpc/cuuats.snt.lts)**

**For more information about the code and documentation regarding Access Score calculations, visit the [CUUATS Accessibility site in GitLab.](https://gitlab.com/ccrpc/cuuats.snt.accessibility)**

## Safety Forecasting Tool

**The [Safety Forecasting Tool appendix](https://ccrpc.gitlab.io/lrtp-2050/appendices/sft/) details the different components, inputs, outputs, and processes that make up the CUUATS Safety Forecasting Tool.**