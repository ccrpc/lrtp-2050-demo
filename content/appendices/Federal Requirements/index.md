---
title: Federal LRTP Requirements
date: 2024-08-08
draft: false
weight: 20
bannerHeading: Federal LRTP Requirements
bannerText: >
  Every Metropolitan Planning Organization must comply with federal requirements when creating the Long Range Transportation Plan. 
---

## Federal Requirements

As Champaign County’s Metropolitan Planning Organization (MPO), CUUATS must adhere to Federal regulations [23 CFR Part 450 Subpart C](https://www.ecfr.gov/current/title-23/part-450/subpart-C). One of the objectives set forth for MPOs is to develop a Metropolitan Transportation Plan (MTP), previously referred to in the statutes as a Long Range Transportation Plan, for the respective metropolitan area. Federal planning requirements include the following: 

1. Support the economic vitality of the metropolitan area, especially by enabling global competitiveness, productivity, and efficiency;

2. Increase the safety of the transportation system for motorized and non-motorized users;

3. Increase the security of the transportation system for motorized and non-motorized users;

4. Increase accessibility and mobility of people and freight;

5. Protect and enhance the environment, promote energy conservation, improve the quality of life, and promote consistency between transportation improvements and State and local planned growth and economic development patterns;

6. Enhance the integration and connectivity of the transportation system, across and between modes, for people and freight;

7. Promote efficient system management and operation;

8. Emphasize the preservation of the existing transportation system;

9. Improve the resiliency and reliability of the transportation system and reduce or mitigate stormwater impacts of surface transportation; and

10. Enhance travel and tourism. [23 CFR 450.306](https://www.ecfr.gov/current/title-23/section-450.306)

<br>

### 23 CFR 450.306(d)(4)

(4) An MPO shall integrate in the metropolitan transportation planning process, directly or by reference, the goals, objectives, performance measures, and targets described in other State transportation plans and transportation processes, as well as any plans developed under [49 U.S.C. chapter 53](https://www.govinfo.gov/link/uscode/49/5301) by providers of public transportation, required as part of a performance-based program including:
    
&ensp;&ensp;(i) The State asset management plan for the NHS, as defined in [23 U.S.C. 119(e)](https://www.govinfo.gov/link/uscode/23/119) and the Transit Asset Management Plan, as discussed in [49 U.S.C. 5326](https://www.govinfo.gov/link/uscode/49/5326);
   
&ensp;&ensp;(ii) Applicable portions of the HSIP, including the SHSP, as specified in [23 U.S.C. 148](https://www.govinfo.gov/link/uscode/23/148);
   
&ensp;&ensp;(iii) The Public Transportation Agency Safety Plan in [49 U.S.C. 5329(d)](https://www.govinfo.gov/link/uscode/49/5329);
   
&ensp;&ensp;(iv) Other safety and security planning and review processes, plans, and programs, as appropriate;
    
&ensp;&ensp;(v) The Congestion Mitigation and Air Quality Improvement Program performance plan in [23 U.S.C. 149(l)](https://www.govinfo.gov/link/uscode/23/149), as applicable;
   
&ensp;&ensp;(vi) Appropriate (metropolitan) portions of the State Freight Plan (MAP-21 section 1118);
   
&ensp;&ensp;(vii) The congestion management process, as defined in [23 CFR 450.322](https://www.ecfr.gov/current/title-23/section-450.322), if applicable; and

&ensp;&ensp;(viii) Other State transportation plans and transportation processes required as part of a performance-based program.

(Source: [eCFR :: 23 CFR Part 450 Subpart C -- Metropolitan Transportation Planning and Programming](https://www.ecfr.gov/current/title-23/chapter-I/subchapter-E/part-450/subpart-C))

<br>

### 23 CFR 450.324 Development and content of the metropolitan transportation plan.
    
(a) The metropolitan transportation planning process shall include the development of a transportation plan addressing no less than a 20-year planning horizon as of the effective date. In formulating the transportation plan, the MPO shall consider factors described in [§ 450.306](https://www.ecfr.gov/current/title-23/section-450.306) as the factors relate to a minimum 20-year forecast period. In nonattainment and maintenance areas, the effective date of the transportation plan shall be the date of a conformity determination issued by the FHWA and the FTA. In attainment areas, the effective date of the transportation plan shall be its date of adoption by the MPO.

(b) The transportation plan shall include both long-range and short-range strategies/actions that provide for the development of an integrated multimodal transportation system (including accessible pedestrian walkways and bicycle transportation facilities) to facilitate the safe and efficient movement of people and goods in addressing current and future transportation demand.

(c) The MPO shall review and update the transportation plan at least every 4 years in air quality nonattainment and maintenance areas and at least every 5 years in attainment areas to confirm the transportation plan's validity and consistency with current and forecasted transportation and land use conditions and trends and to extend the forecast period to at least a 20-year planning horizon. In addition, the MPO may revise the transportation plan at any time using the procedures in this section without a requirement to extend the horizon year. The MPO shall approve the transportation plan (and any revisions) and submit it for information purposes to the Governor. Copies of any updated or revised transportation plans must be provided to the FHWA and the FTA.

(d) In metropolitan areas that are in nonattainment for ozone or carbon monoxide, the MPO shall coordinate the development of the metropolitan transportation plan with the process for developing transportation control measures (TCMs) in a State Implementation Plan (SIP).

(e) The MPO, the State(s), and the public transportation operator(s) shall validate data used in preparing other existing modal plans for providing input to the transportation plan. In updating the transportation plan, the MPO shall base the update on the latest available estimates and assumptions for population, land use, travel, employment, congestion, and economic activity. The MPO shall approve transportation plan contents and supporting analyses produced by a transportation plan update.

(f) The metropolitan transportation plan shall, at a minimum, include:

1. The current and projected transportation demand of persons and goods in the metropolitan planning area over the period of the transportation plan;

2. Existing and proposed transportation facilities (including major roadways, public transportation facilities, intercity bus facilities, multimodal and intermodal facilities, nonmotorized transportation facilities (e.g., pedestrian walkways and bicycle facilities), and intermodal connectors) that should function as an integrated metropolitan transportation system, giving emphasis to those facilities that serve important national and regional transportation functions over the period of the transportation plan.

3. A description of the performance measures and performance targets used in assessing the performance of the transportation system in accordance with § 450.306(d).

4. A system performance report and subsequent updates evaluating the condition and performance of the transportation system with respect to the performance targets described in § 450.306(d), including—

(i) Progress achieved by the metropolitan planning organization in meeting the performance targets in comparison with system performance recorded in previous reports, including baseline data; and

(ii) For metropolitan planning organizations that voluntarily elect to develop multiple scenarios, an analysis of how the preferred scenario has improved the conditions and performance of the transportation system and how changes in local policies and investments have impacted the costs necessary to achieve the identified performance targets.

5. Operational and management strategies to improve the performance of existing transportation facilities to relieve vehicular congestion and maximize the safety and mobility of people and goods;

6. Consideration of the results of the congestion management process in TMAs that meet the requirements of this subpart, including the identification of SOV projects that result from a congestion management process in TMAs that are nonattainment for ozone or carbon monoxide.

7. Assessment of capital investment and other strategies to preserve the existing and projected future metropolitan transportation infrastructure, provide for multimodal capacity increases based on regional priorities and needs, and reduce the vulnerability of the existing transportation infrastructure to natural disasters. The metropolitan transportation plan may consider projects and strategies that address areas or corridors where current or projected congestion threatens the efficient functioning of key elements of the metropolitan area's transportation system.

8. Transportation and transit enhancement activities, including consideration of the role that intercity buses may play in reducing congestion, pollution, and energy consumption in a cost-effective manner and strategies and investments that preserve and enhance intercity bus systems, including systems that are privately owned and operated, and including transportation alternatives, as defined in [23 U.S.C. 101(a)](https://www.govinfo.gov/link/uscode/23/101), and associated transit improvements, as described in [49 U.S.C. 5302(a)](https://www.govinfo.gov/link/uscode/49/5302), as appropriate;

9. Design concept and design scope descriptions of all existing and proposed transportation facilities in sufficient detail, regardless of funding source, in nonattainment and maintenance areas for conformity determinations under the EPA's transportation conformity regulations ([40 CFR part 93, subpart A](https://www.ecfr.gov/current/title-40/part-93/subpart-A)). In all areas (regardless of air quality designation), all proposed improvements shall be described in sufficient detail to develop cost estimates;

10. A discussion of types of potential environmental mitigation activities and potential areas to carry out these activities, including activities that may have the greatest potential to restore and maintain the environmental functions affected by the metropolitan transportation plan. The discussion may focus on policies, programs, or strategies, rather than at the project level. The MPO shall develop the discussion in consultation with applicable Federal, State, and Tribal land management, wildlife, and regulatory agencies. The MPO may establish reasonable timeframes for performing this consultation;

11. A financial plan that demonstrates how the adopted transportation plan can be implemented.

12. Pedestrian walkway and bicycle transportation facilities in accordance with [23 U.S.C. 217(g)](https://www.govinfo.gov/link/uscode/23/217).

For an extended version of Title 23, view here: [eCFR :: 23 CFR Part 450 Subpart C -- Metropolitan Transportation Planning and Programming](https://www.ecfr.gov/current/title-23/chapter-I/subchapter-E/part-450/subpart-C).