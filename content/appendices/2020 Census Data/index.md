---
title: 2020 Census Data Comparisons
date: 2024-08-08
draft: false
weight: 30
---

## 2020 Census Data Comparisons

This Appendix portion provides additional graphical data regarding the analysis completed in the 2020 Decennial Census section of the LRTP 2050. Subsections of this Appendix align with subsections there. Brief descriptions and narratives are provided as needed to the referenced 2020 Decennial Census text.

As a note, some of the graphs may be difficult to view all data. Users can use the legend in each graph to select and deselect viewable items by hovering over an item and clicking it. The item with then show a mark-out line over the item removing it from the graph. This will change the zoom of the graph depending on what the new highest value is in the dataset.

### Population Changes

The graphs below compare year-to-year changes in estimated populations for each of the MPA municipalities to the population counts completed for the 2020 Decennial Census. 

<rpc-chart url="Ch_ACS_Census_Pop.csv"
  chart-title="Champaign Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,orange"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="2020 Census Population Count"
data="-,-,-,-,-,-,-,-,88302,-,-">
</rpc-dataset></rpc-chart>

<rpc-chart url="Sav_ACS_Census_Pop.csv"
  chart-title="Savoy Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,orange"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="2020 Census Population Count"
data="-,-,-,-,-,-,-,-,8857,-,-">
</rpc-dataset></rpc-chart>

<rpc-chart url="Tol_ACS_Census_Pop.csv"
  chart-title="Tolono Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,orange"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="2020 Census Population Count"
data="-,-,-,-,-,-,-,-,3604,-,-">
</rpc-dataset></rpc-chart>

<rpc-chart url="Bond_ACS_Census_Pop.csv"
  chart-title="Bondville Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,orange"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="2020 Census Population Count"
data="-,-,-,-,-,-,-,-,388,-,-">
</rpc-dataset></rpc-chart>

<rpc-chart url="Ma_ACS_Census_Pop.csv"
  chart-title="Mahomet Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,orange"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="2020 Census Population Count"
data="-,-,-,-,-,-,-,-,9434,-,-">
</rpc-dataset></rpc-chart>

### Age

2020 Decennial Census counts broken down by the age of the population in each municipality shows trends based on demographic characteristics of youth, young adults, working-age adults, and older adults. Besides Urbana, some of the other smaller municipalities saw significant changes. Savoy may have experienced some reduction in student-aged populations with the University's COVID policies as well, though not as strongly. Mahomet and Tolono saw significant rises in youth and working adult populations suggesting growth based around schooling with new families moving to the areas. Bondville continued to see a loss in population, though their population is too small to make any significant suggestions on reasons for this change based on age demographics.

<rpc-chart url="Sav_ACS_Census_Pop_Age.csv"
  chart-title="Savoy Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo, orange, green"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="green"
background-color="green"
border-width="0"
point-radius="8"
label="65+ years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,1656,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="25-64 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,4302,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="indigo"
background-color="indigo"
border-width="0"
point-radius="8"
label="15-24 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,1234,-,-">
</rpc-dataset>
<rpc-dataset
type="line"
fill="false"
border-color="blue"
background-color="blue"
border-width="0"
point-radius="6"
label="0-14 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,1665,-,-">
</rpc-dataset>
</rpc-chart>

<rpc-chart url="Ma_ACS_Census_Pop_Age.csv"
  chart-title="Mahomet Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo, orange, green"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="green"
background-color="green"
border-width="0"
point-radius="8"
label="65+ years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,1106,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="25-64 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,4813,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="indigo"
background-color="indigo"
border-width="0"
point-radius="6"
label="15-24 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,1100,-,-">
</rpc-dataset>
<rpc-dataset
type="line"
fill="false"
border-color="blue"
background-color="blue"
border-width="0"
point-radius="8"
label="0-14 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,2415,-,-">
</rpc-dataset>
</rpc-chart>

<rpc-chart url="Tolo_ACS_Census_Pop_Age.csv"
  chart-title="Tolono Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo, orange, green"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="green"
background-color="green"
border-width="0"
point-radius="8"
label="65+ years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,464,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="25-64 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,1889,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="indigo"
background-color="indigo"
border-width="0"
point-radius="6"
label="15-24 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,487,-,-">
</rpc-dataset>
<rpc-dataset
type="line"
fill="false"
border-color="blue"
background-color="blue"
border-width="0"
point-radius="8"
label="0-14 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,764,-,-">
</rpc-dataset>
</rpc-chart>

<rpc-chart url="Bond_ACS_Census_Pop_Age.csv"
  chart-title="Bondville Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo, orange, green"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="green"
background-color="green"
border-width="0"
point-radius="8"
label="65+ years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,72,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="25-64 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,239,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="indigo"
background-color="indigo"
border-width="0"
point-radius="8"
label="15-24 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,31,-,-">
</rpc-dataset>
<rpc-dataset
type="line"
fill="false"
border-color="blue"
background-color="blue"
border-width="0"
point-radius="7"
label="0-14 years old, 2020 Census Count"
data="-,-,-,-,-,-,-,-,46,-,-">
</rpc-dataset>
</rpc-chart>

### Race

Other municipalities also show significant increases in categories of Two or More Races and Some Other Race due to the change in the US Census Bureau's methodology for tabulating responses. The municipalities are shown below for Savoy, Mahomet, Tolono, and Bondville. As a note, users can de-select data lines or points by clicking on the item in the legend. This may be helpful for viewing data for non-White populations in some municipalities.

<rpc-chart url="Race_SA_Min_Pop_ACS.csv"
  chart-title="Savoy Estimated Non-White Populations 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="gray,blue,indigo,orange,green,red,lime"
  legend-position="right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="lime"
background-color="lime"
border-width="0"
point-radius="8"
label="Two or More Races: 2020 Census"
data="-,-,-,-,-,-,-,-,605,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="red"
background-color="red"
border-width="0"
point-radius="8"
label="Some Other Race alone, 2020 Census"
data="-,-,-,-,-,-,-,-,137,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="green"
background-color="green"
border-width="0"
point-radius="8"
label="Native Hawaiian and Other Pacific Islander alone, 2020 Census"
data="-,-,-,-,-,-,-,-,1,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="Asian, 2020 Census"
data="-,-,-,-,-,-,-,-,1630,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="indigo"
background-color="indigo"
border-width="0"
point-radius="8"
label="American Indian and Alaska Native alone, 2020 Census"
data="-,-,-,-,-,-,-,-,12,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="blue"
background-color="blue"
border-width="0"
point-radius="8"
label="Black or African American, 2020 Census Count"
data="-,-,-,-,-,-,-,-,614,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="gray"
background-color="gray"
border-width="0"
point-radius="8"
label="White, 2020 Census Count"
data="-,-,-,-,-,-,-,-,5858,-,-">
</rpc-dataset>
</rpc-chart>

<rpc-chart url="Race_MA_Min_Pop_ACS.csv"
  chart-title="Mahomet Estimated Non-White Populations 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="gray,blue,indigo,orange,green,red,lime"
  legend-position= "right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data" ><rpc-dataset
    type="line"
    fill="false"
    border-color="lime"
    background-color="lime"
    border-width="0"
    point-radius="8"
    label="Two or More Races: 2020 Census"
    data="-,-,-,-,-,-,-,-,529,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="red"
    background-color="red"
    border-width="0"
    point-radius="8"
    label="Some Other Race alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,76,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="green"
    background-color="green"
    border-width="0"
    point-radius="8"
    label="Native Hawaiian and Other Pacific Islander alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,6,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="orange"
    background-color="orange"
    border-width="0"
    point-radius="8"
    label="Asian, 2020 Census"
    data="-,-,-,-,-,-,-,-,262,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="8"
    label="American Indian and Alaska Native alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,18,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="blue"
    background-color="blue"
    border-width="0"
    point-radius="8"
    label="Black or African American, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,102,-,-">
    </rpc-dataset>
    <rpc-dataset
    type="line"
    fill="false"
    border-color="gray"
    background-color="gray"
    border-width="0"
    point-radius="8"
    label="White, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,8441,-,-">
    </rpc-dataset>
</rpc-chart>

<rpc-chart url="Race_TO_Min_Pop_ACS.csv"
  chart-title="Tolono Estimated Non-White Populations 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="gray,blue,indigo,orange,green,red,lime"
  legend-position= "right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data" ><rpc-dataset
    type="line"
    fill="false"
    border-color="lime"
    background-color="lime"
    border-width="0"
    point-radius="8"
    label="Two or More Races: 2020 Census"
    data="-,-,-,-,-,-,-,-,206,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="red"
    background-color="red"
    border-width="0"
    point-radius="8"
    label="Some Other Race alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,41,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="green"
    background-color="green"
    border-width="0"
    point-radius="8"
    label="Native Hawaiian and Other Pacific Islander alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,1,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="orange"
    background-color="orange"
    border-width="0"
    point-radius="8"
    label="Asian, 2020 Census"
    data="-,-,-,-,-,-,-,-,15,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="8"
    label="American Indian and Alaska Native alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,17,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="blue"
    background-color="blue"
    border-width="0"
    point-radius="8"
    label="Black or African American, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,40,-,-">
    </rpc-dataset>
    <rpc-dataset
    type="line"
    fill="false"
    border-color="gray"
    background-color="gray"
    border-width="0"
    point-radius="8"
    label="White, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,3284,-,-">
    </rpc-dataset>
</rpc-chart>

<rpc-chart url="Race_BO_Min_Pop_ACS.csv"
  chart-title="Bondville Estimated Non-White Populations 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="gray,blue,indigo,orange,green,red,lime"
  legend-position= "right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data" ><rpc-dataset
    type="line"
    fill="false"
    border-color="lime"
    background-color="lime"
    border-width="0"
    point-radius="8"
    label="Two or More Races: 2020 Census"
    data="-,-,-,-,-,-,-,-,43,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="red"
    background-color="red"
    border-width="0"
    point-radius="8"
    label="Some Other Race alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,2,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="green"
    background-color="green"
    border-width="0"
    point-radius="6"
    label="Native Hawaiian and Other Pacific Islander alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,0,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="orange"
    background-color="orange"
    border-width="0"
    point-radius="6"
    label="Asian, 2020 Census"
    data="-,-,-,-,-,-,-,-,2,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="6"
    label="American Indian and Alaska Native alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,1,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="blue"
    background-color="blue"
    border-width="0"
    point-radius="6"
    label="Black or African American, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,11,-,-">
    </rpc-dataset>
    <rpc-dataset
    type="line"
    fill="false"
    border-color="gray"
    background-color="gray"
    border-width="0"
    point-radius="8"
    label="White, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,329,-,-">
    </rpc-dataset>
</rpc-chart>