---
title: "Safety Forecasting Tool"
date: 2024-11-27
draft: false
weight: 70
bannerHeading: Safety Forecasting Tool
bannerText: >
  This appendix details the development of the CUUATS Safety Forecasting Tool and the results from the analysis completed for the LRTP 2050.

---
## 1. Introduction
Transportation planners and engineers often find it difficult to list and prioritize transportation projects based on safety measures as the required data and tools are not readily available to conduct regular and comprehensive safety evaluations for the region. This project aims to develop a safety forecasting tool and assist transportation professionals to understand where traffic crash risk is high in our community.

This project is funded by the Illinois Department of Transportation and carried out by staff at the Champaign County Regional Planning Commission (CCRPC).

### 1.1. Project Background
The AASHTO Highway Safety Manual states, “Traffic crashes are rare and random events. By rare, it is implied that crashes represent only a very small proportion of the total number of events that occur on the transportation system” (AASHTO, 2010, p. 3-5). Random means that crashes occur as a function of a set of events influenced by several factors, which are partly deterministic and can be controlled, such as vehicle speed, and partly stochastic random and unpredictable, such as a branch of a tree falling on the road. Circumstances that lead to a crash in one event will not necessarily lead to a crash in a similar event. Events before, during, and after crash determine the risk of a crash occurring, crash severity, and emergency response time (AASHTO, 2010).

The AASHTO Highway Safety Manual states, “crashes have the following three general categories of contributing factors:
* Human: including age, judgment, driver skill, attention, fatigue, experience, and sobriety.
* Vehicle: including age, design, manufacture, and maintenance.
* Roadway/Environment: including geometric alignment, cross-section, traffic control device, surface friction, grade, signage, weather, and visibility” 
(AASHTO, 2010, p. 3-6).

According to Singh (2018), approximately 94 percent of crashes at the national level can be attributed to driver errors as critical reasons of the pre-crash events. About two percent of the crashes can be attributed to environmental reasons. Out of the crashes with environmental reasons being the critical reasons, “in about 50 percent (±14.5%) of the 52,000 crashes the critical reason was attributed to slick roads. Glare as a critical reason accounted for about 17 percent (±16.7%) of the environment-related crashes, and view obstruction was assigned in 11 per-cent (±7.2%) of the crashes. Signs and signals accounted for 3percent (±2.5%) of such crashes. In addition, in 52,000 of the crashes with a critical reason attributed to the environment, the weather condition (fog/rain/snow) was cited in 4 percent(±2.9%) of the crashes” (Singh, S, March 2018). Singh adds that “Critical reasons are the immediate reason for the critical pre-crash event and is often the last failure in the causal chain of events leading up to the crash. Although the critical reason is an important part of the description of events leading up to the crash, it is not intended to be interpreted as the cause of the crash nor as the assignment of the fault to the driver, vehicle, or environment (Singh, S, March 2018).

A significant proportion of crashes are caused by a combination of the three categories of contributing factors (e.g.  slow driver reactions during adverse weather conditions, speeding in foggy weather). This project focuses on the roadway and environmental factors before crashes with only limited roadway geometry, traffic, and demographic data to predict roadway crash risks. These incomplete roadway and environmental factors are only part of the contributing factors for crash occurrences. Therefore, the ability of the models tested in this project to accurately capture the totality of crash risks and outcomes is severely limited. While it is probably impossible to precisely predict crashes for a specific time and place, this project aims for a higher accuracy of prediction compared to historical data alone based on interpretation of patterns of the factors that influence crashes.

## 2. Literature Review

### 2.1 Highway Safety Manual (HSM) Predictive Method
The 2010 AASHTO Highway Safety Manual (HSM) is the premier guidance document for incorporating quantitative safety analysis in the highway transportation project planning and development processes. The HSM crash frequency predictive method estimates the expected average crash frequency (by total crashes, crash severity, or collision type) of a site, facility, or roadway network for a given time period, geometric design and traffic control features, and traffic volumes. This is done using a statistical model developed from data for a number of similar sites and adjusted to account for specific site conditions and local conditions (AASHTO, 2010).

#### 2.1.1 Basic elements of the HSM predictive method
The predictive models vary by facility and site type, but all have the same basic elements:
*	Safety Performance Functions (SPFs): statistical base models used to estimate the average crash frequency for a facility type with specified base conditions.
*	Crash Modification Factors (CMFs): CMFs are the ratio of the effectiveness of one condition in comparison to another condition. CMFs are multiplied with the crash frequency predicted by the SPF to account for the difference between site conditions and specified base conditions.
* Calibration factor (C): multiplied with the crash frequency predicted by the SPF to account for differences between the jurisdiction and time period for which the predictive models were developed and the jurisdictional and time period to which they are applied by HSM users. Estimating the calibration factor C through the calibration process is necessary because the general level of crash frequencies may vary substantially from one jurisdiction to another for a variety of reasons including crash reporting thresholds and crash reporting system procedures.

The SPF used for one specific site cannot be directly applied for another site since the facility conditions may be different and the traffic pattern in different regions are different. CMFs and Cs must be applied to adjust the predicted crash frequency according to site-specific and local conditions. The adjusted predicted crash frequency can be calculated as:

##### *N<sub>predicted</sub> = N<sub>SPFx</sub>  *  CMF<sub>1x</sub>  *  CMF<sub>2x</sub>  * … *  CMF<sub>yx</sub>  *  Cx</sub>*

&nbsp;&nbsp;Where:</br>
&nbsp;&nbsp;&nbsp;&nbsp;*N<sub>predicted</sub>* :&nbsp; predictive model estimates of crash frequency for a specific year on site type x (crashes/year);<br/>

&nbsp;&nbsp;&nbsp;&nbsp;*N<sub>SPFx</sub>* :&nbsp; predictive average crash frequency determined for base conditions with the Safety Performance Function representing site type x (crashes/year);<br/>

&nbsp;&nbsp;&nbsp;&nbsp;*CMF<sub>yx</sub>* :&nbsp; crash Modification Factors specific to site type x;

&nbsp;&nbsp;&nbsp;&nbsp;*C<sub>x</sub>* :&nbsp; calibration Factor to adjust for local conditions for site type x. 
(AASHTO, 2010, p. 10-2)

#### 2.1.2 Combine observed crash frequency
The AASHTO Highway Safety Manual (2010) uses Empirical Bayes (EM) method to combine the estimation from the statistical model with observed crash frequency at the specific site. A weighting factor is applied to the two estimates to reflect the model's statistical reliability. When observed crash data is not available or applicable, the EM method does not apply.

##### *N<sub>expected</sub> = w  *  N<sub>predicted</sub> + (1-w)  *  N<sub>observed</sub>*

&nbsp;&nbsp;Where:<br/>
&nbsp;&nbsp;&nbsp;&nbsp;*N<sub>expected</sub>* : &nbsp;expected average crashes frequency for the study period;<br/>

&nbsp;&nbsp;&nbsp;&nbsp;*N<sub>predicted</sub>* : &nbsp;predicted average crash frequency predicted using SPF for the study period under the given conditions;<br/>

&nbsp;&nbsp;&nbsp;&nbsp;*N<sub>observed</sub>* : &nbsp;observed cash frequency at the site over the study period;<br/>

&nbsp;&nbsp;&nbsp;&nbsp;*w* : &nbsp;weighted adjustment to be placed on the SPF prediction.&nbsp;(AASHTO, 2010, p. 10-2)

#### 2.1.3 Safety Performance Functions (SPF)
The AASHTO Highway Safety Manual (2010) states, “SPFs are regression equations developed through statistical multiple regression technique using historic crash data collected over a number of yerasat  sites with similar characteristics and covering a wide range of AADTs” (AASHTO, 2010, p. C-14). HSM SPFs assume that crash frequencies follow a negative binomial distribution, which is an extension of the Poisson distribution, and is better suited than the Poisson distribution to model over-dispersed crash data with the variance that exceeds the mean. They are a function of AADT, and in the case of roadway segments, the segment length. HSM provide SPFs for the follow facility types, each with specified based conditions: signalized intersections, unsignalized intersections, divided roadway segments, and undivided roadway segments for rural two-lane two-way roads, rural multilane highways, and urban and suburban arterials. SPFs in the HSM are power functions to represent the relationship between crash frequencies and traffic volume, which many researchers have pointed out to not be the most appropriate functional form (Srinivasan, R., Carter, D. & Bauer, K., 2013).

Federal Highway Administration (2013), states: 

&nbsp;&nbsp;&nbsp;&nbsp;“For highway segments, exposure is represented by the segment length and annual average daily traffic (AADT) associated with the study section as shown by the sample SPF in the equation below.

##### &nbsp;&nbsp;&nbsp;&nbsp;Predicted Crashes = exp[a + β * ln(AADT) + ln(Segment Length)]

&nbsp;&nbsp;&nbsp;&nbsp;For intersections, exposure is represented by the AADT on the major and minor intersecting roads as shown by the sample SPF in the equation below.

##### &nbsp;&nbsp;&nbsp;&nbsp;Predicted Crashes = exp[a + β1 * ln(AADTmajor) + β2 * ln(AADTminor)]"
&nbsp;&nbsp;&nbsp;&nbsp;(Federal Highway Administration, 2013, p. 1).

In order to apply SPF, not only basic geometric and geographic information of the site is necessary to determine the facility type and to determine whether a SPF is available for that facility and site type, detailed geometric design and traffic control features of the site are also needed to determine whether and how the site conditions vary from SPF baseline conditions. For instance, before being calibrated to local conditions using CMFs, local calibration factors, and observed crash data, the base condition for roadway segments on rural two-lane, two-way roads using SPF are:

*	Lane width
*	Shoulder width
*	Shoulder type
*	Roadside hazard rating
*	Driveway density
*	Horizontal curvature
*	Vertical curvature
*	Centerline rumble strips
*	Passing lanes
*	Two-way left-turn lanes
*	Lighting
*	Automated speed enforcement
*	Grade level
*	AADT information (AASHTO, 2010, pp. 10-14, 10-15).

Due to the extensive and detailed facility geometric data required to successfully apply the HSM at the network level, which CCRPC does not have at this time, this project uses data analytics and Machine Learning algorithms to take advantage of the existing crash history, roadway network, demographic, and land use data to predict crash risks for regional roadways.

### 2.2 Crash Data Considerations

Crash data has several characteristics that make predicting crash risks challenging:

**Over-dispersion.** For crash data, the variance normally exceeds the mean of the crash counts, which makes most common count-data modeling approaches (Poisson regression models) limited in their ability to produce reliable predictions (Lord, 2010). This is also why the HSM assumes that crash frequencies follow a negative binomial distribution, which is an extension of the Poisson distribution, to account for over-dispersion of crash data.

**Regression-to-the-mean tendency.** Because crashes are random events, crash frequencies naturally fluctuate overtime at any given site. “When a period with a comparatively high crash frequency is observed, it is statistically probable that the following period will be followed by a comparatively low crash frequency. This tendency is known as regression-to-the-mean (RTM) and applies to the high probability that a low crash frequency period will be followed by a high crash frequency period” (AASHTO, 2010, p. 3-11).

**Time-varying explanatory variables.** Because crash-frequency data are considered over some time period, the fact that explanatory variables may change significantly over this time period is not usually considered due to the lack of detailed data within the time period (Lord, D., & Mannering, F., 2010). The variance of site conditions, such as traffic volume, weather, traffic control, land use, etc. over time makes it difficult to attribute changes in the expected average crash-frequency to specific conditions. In addition, the implications of crash frequency fluctuation and variation of site conditions are often in conflict. “On one hand, the year-to-year fluctuation in crash frequencies tends toward acquiring more years of data to determine the expected average crash frequency. On the other hand, changes in site conditions can shorten the length of time for which crash frequency are valid for considering average” (AASHTO, 2010, p. 3-13).

**Class imbalance.** Traffic accidents are rare incidents. If one crash occurrence is a positive sample, then all road segments at all time points that did not experience crashes can be treated as negative samples, which can lead to a huge pool of negative samples. If we construct class labels based on crash vs. no-crash for each road, the classes will be severely imbalanced (Yuan, 2017). There are different approaches to sample the negative no-crash cases, including Synthetic Minority Over-sampling Technique (SMOTE) and other informative negative sampling methods (Al Mamlook, R. Kwayu, K., Alkasisbeh, M., & Frefer, A., 2019; Lord, D., & Mannering, F., 2010; Dong, N., Huang, H., & Zheng, L. (2015); and Yu, R., & Abdel-Aty, M., 2013).

**Spatial heterogeneity.** The prediction model parameters may vary from place to place. For example, factors causing traffic accidents in urban centers with dense population and lower speed limits might be very different from those in rural areas with low population density but high-speed limit (Yuan, 2017). A global model might not be very accurate everywhere. In fact, even some of the most sophisticated models find it hard to predict crash risks accurately at the County level.

**Endogenous variables.** There are times when the explanatory variables in models can be endogenous, in that their values may depend on the frequency of crashes. An example of this problem is the frequency of ice-related accidents and the effectiveness of ice-warning signs in reducing this frequency. When developing a crash-frequency model, an indicator variable for the presence of an ice-warning sign would be one way of understanding the impact of the warning signs. However, “ice-warning signs are more likely to be placed at locations with high numbers of ice-related crashes, this indicator variable may be endogenous (the explanatory variable will change as the dependent variable changes). If this endogeneity is ignored, the parameter estimates will be biased. In the case of the ice-warning sign indicator, ignoring the endogeneity may lead to the erroneous conclusion that ice-warning signs actually increase the frequency of ice-related crashes because the signs are going to be associated with locations of high ice-crash frequencies” (Lord, D., & Mannering, F.).

### 2.3 Model Considerations
There has been considerable research conducted over the last 20 years focused on predicting motor vehicle crashes on transportation facilities. The range of statistical models commonly applied includes binomial, Poisson, Poisson-gamma (or Negative Binomial), Zero-Inflated Poisson and Negative Binomial Models (ZIP and ZINB), and Multinomial probability models. Given the range of possible modeling approaches and the host of assumptions with each modeling approach, making an intelligent choice for modeling motor vehicle crash data is difficult at best” (Lord, D., Washington, S., & Ivan, J., 2004). The following section lists methods with their strengths and weaknesses identified. 

#### 2.3.1 Statistical Modeling Approaches
Traditional statistical modeling approaches seek to identify specific formulas for crash risks and optimize the model parameters to better capture the cause-effect relationship between traffic, risk conditions, and crashes. Due to that fact that the crash frequencies are random, discrete and non-negative numbers, many researchers choose to use the Poisson model to predict crashes. The Poisson model can be represented as *p(n)=(λ^n exp⁡(-λ))/n!* where *p(n)* is the probability of having n crashes over the study period. However, the Poisson model requires the mean and variance to be the same (mean=λ, variance=λ), which is not realistic for crash data.

The negative Binomial model releases this constraint by adding an error term to calibrate the expected crash frequency such that *λ=exp⁡⁡(βX+ε)* where *exp⁡(ε)* follows Gamma distribution. It has been proven in many studies that this method is more appropriate than the Poisson method to model the vehicle crash data of which variance exceeds the mean (Hadi, M., Aruldhas, J., Chow, L., & Wattleworth, J., 1995; Poch & Mannering, 1996). The Highway Safety Manual adapted the Negative Binomial model and named it the Safety Performance Function (SPF) approach, as discussed above, to provide guidance for transportation engineers to estimate the crash frequency. Although being widely adopted, the Poisson model and the Negative Binomial model have some deficiencies. For example, as indicated by Chang (2005), if the rule of the Gamma distribution is not satisfied, the Negative Binomial model would not be valid for crash prediction. Some researchers suggested using the Poisson-lognormal model, which has a more relaxed constraint. The error term, *exp⁡(ε)*, of the Poisson-lognormal model follows the Lognormal distribution instead of the Gamma distribution.

To overcome the problems of excessive non-crash observations, some researchers proposed using Zero-inflated Poisson and Zero-inflated Negative Binomial model (Lee, A., Stevenson, M., Wang, K., & Yau, K. , 2002; Miaou, S., 1994; & Shankar, V., Milton, J., & Mannering, F., 1997). To incorporate the spatial and temporal correlations, additional random effect variables were introduced in the Poisson model and the Negative Binomial model (Johansson, 1996; & Shankar, V. N., Albin, R., Milton, J., & Mannering, F., 1998). Observations were divided into several groups according to when or where the crash happened. The expected crash frequency of the road section i belonging to the group *j* was calculated as *λ_ij=exp⁡(βX_ij)exp⁡(ε_j)*. The group-specific effect of the observation group j was reflected by ε_j and exp⁡(ε_j) followed Gamma distribution. Some researchers also suggested using the Negative Multinomial Regression model to address the correlation problem. Caliendo, Guida, and Parisi, (2007), compared the capability of Poisson, Negative Binomial and Negative Multinomial regression models regarding crash detection on multilane rural roads. The result of the study showed that the Negative Multinomial regression model considering over-dispersion impacts had the best prediction performance.

There have been many other innovative statistical models proposed in the past two to three decades. Each of them has its distinguished contribution to the travel safety study. However, for various reasons such as model complexity or transferability, their applications are very limited. The inherent limitations of the crash data as well as the strengths and deficiencies of the proposed statistical models were comprehensively analyzed in the study of (Lord, D., & Mannering, F., 2010) and (Mannering, F., & Bhat, C., 2014).

#### 2.3.2 Big Data Driven (Machine Learning) Approaches
Machine learning methods have some advantages over the statistical methods. First, while the statistical methods predefine the underlying relationship between the explanatory variables and the dependent variables, which would lead to the failure of the model if the assumptions were violated, machine learning methods do not require inherent assumptions. Second, according to Chang (2005), machine learning methods can handle the problem of associations between the explanatory variables and are able to capture complicated relationships which might be difficult to achieve in statistical models. Third, as mentioned by Lord and Mannering (2010) and Mannering and Bhat (2014), the progress of transportation related research would be greatly promoted by new data resources provided by the emerging technologies. New data resources such as video surveillance data, satellite images, social media data, mobile sensing data and GPS trajectory data are available for travel safety studies in some districts. Statistical models might not be able to handle these data resources very well. Finally, machine learning methods can predict vehicle crashes over a large area and a long time period, which might be difficult for statistical models.

Researchers use big-data driven approaches, or machine learning approaches, to predict the occurrence of crashes by maximizing the ability of interpreting massive amounts of data without attempting to explain the cause-effect relationship for crash occurrence. The most widely adopted models include K Nearest Neighbor, Support Vector Machine, Random Forest, and Neural Networks models.

**K Nearest Neighbor (KNN) method**. Lv and Zhao (2009) applied the K-Nearest Neighbor (KNN) method to predict highway vehicle crashes using real-time traffic flow data. The average Euclidean distance of different classes was calculated to help select the accident precursors before training the KNN classifier.

**Support Vector Machine (SVM) method**. SVM models are a set of related supervised learning methods used for classification and regression that possess the well-known ability of being able to approximate any multivariate function to any desired degree of accuracy. Statistical learning theory and structural risk minimization are the theoretical foundations for the learning algorithms of support vector machine models (Lord, 2010). SVM method, of which the decision is made by finding the hyperplane that has the max distance to the closet element of each class, was also examined by many researchers. Li, Lord, Zhang, and Xie (2008) applied the SVM model and concluded that the SVM method outperforms the Negative Binomial method in terms of predicting vehicle crash frequency on rural frontage roads. Chen, Wang, & van Zuylen (2009) ensembled several SVM models to help detect freeway traffic crashes. Different combination schemes based on bagging, boosting and cross-validation committee were tested in the study and the results indicated that the ensemble technique can improve the accuracy of a single SVM in most cases. Dong, Huang, & Zheng (2015) investigated the possibility of using the SVM method for predicting crash risk (frequency) of different traffic analysis zones. The spatial association of analysis zones was revealed by four different spatial dependence matrices including adjacency, shared boundary length, geometry centroid distance, and crash-weighted centroid distance. The study showed that SVM with radial based kernel outperformed the SVM with linear kernel and the Bayesian spatial model. However, the SVM model has some limitations (Yu and Abdel-Aty, 2013; Li, 2008).

First, SVMs work as black boxes. There is no formal functional form between crashes and the covariates.

Second, parameters (C, gamma) need to be determined before the training phase. Algorithms such as the grid searching algorithm have been developed to select the parameter values automatically based on the data. But this process may not provide a global optimum.

Third, while traditional statistical methods have the advantages of being able to ascertain whether the model’s coefficients are significant, the SVMs cannot be used for such purpose. The inclusion of insignificant variables may not improve prediction accuracy and may face the risk of over-fitting. However, once a list of candidate variables is identified from prior knowledge or traditional statistical models, SVMs can identify better relationships and improve the model’s fit as well as the predictive performance.

Fourth, the model may have unsatisfactory performance when handling datasets with a large number of samples.

**Decision Tree method**. While the methods mentioned above use all the input features collectively to decide the final result, the tree-based machine learning method has multiple decision steps that are organized in a hierarchical structure. One feature is used to make a binary decision in each step, and the final decision is made according to the leaf node. Thanks to this nature, it can graphically represent the analyzing process and thus provide straightforward guidance for the engineers to understand the interrelation. Many researchers examined the tree-based machine learning method for crash prediction. For instance, Chang and Chen (2005) proposed using the classification tree method to predict accident rates on freeway segments. Highway geometry characteristics, traffic characteristics, and environment conditions are investigated. The graphical representation of the tree provided by the authors indicated that high traffic volume, high precipitation, high grade and large curvature would lead to a relative high crash accident rate.

**Random Forest method**. While a single tree is built on the entire dataset and may not be robust, ensemble methods combine many weak learners to be a single strong learner. There are two ensemble methods, bagging and boosting. The bagging method uses the bootstrap sampling strategy to sample several subsets from the entire dataset with replacement and trains a decision tree on each subset. The result is the majority of, or the average of, the decisions made by trees. The boosting method creates new learners in sequence; each one is formed to help improve the learner built in the previous step. The most representative model that applied the bagging method is the Random Forest model. Not only the observations but also the features are sampled randomly in the training process of the Random Forest model. Though being more robust than a single decision tree, it still has some deficiencies. Since the final prediction of random forest is based on the majority vote or the average of all outcomes, it may not be able to capture the precise value for the regression problem or may vote the wrong result if the parameters are not tuned well for the classification problem.

**Extreme Gradient Boosting (XGB) method**. Many studies have found that Extreme Gradient Boosting, which is an implementation of the boosting method being proposed by Chen and Guestrin (2016), outperforms the Random Forest model. For example, Schlögl, Stütz, Laaha, and Melcher (2019) compared the classification capability of regression methods, SVM, bagging (Random Forest and Extremely Randomized Trees) methods, boosting (XGB) method, and Bayesian Neural Network for incident detection. While both bagging and boosting methods had remarkably better performance than the other methods, the XGB method was better than the Random Forest method in general. Researchers have also adopted the XGB method for many other travel safety related tasks such as incident detection (Parsa, A., Movahedi, A., Taghipour, H., Derrible, S., & Mohammadian, A., 2020), crash severity forecast (Mokoatle, M., Marivate, V., & Esiefarienrhe, M., 2019) and accident duration prediction (Shan, L., Yang, Z., Zhang, H., Shi, R., & Kuang, L., 2019). In the study of Shan, Yang, Zhang, Shi, and Kuang (2019), the accident duration prediction problem is solved by using an ensembled XGB model. Several XGB binary classifiers were built and ensembled by a neural network. The study provided a new idea for exploring the complex traffic accident data.

**Neural Network method**. Neural networks are machine learning algorithms used for prediction and classification. Their structure is inspired by basic connectivity features of biological neurons. They were first explored widely in the '80s and '90s. Crucial breakthroughs in the mid '00s led to rapid development, and neural nets are now achieving record accuracies in important classification tasks like image and voice recognition. Many researchers have examined the neural network method, from shallow neural networks to deep neural networks.

**Shallow neural networks**. Chang (2005) compared a 3-layer Artificial Neural Network (ANN) model with the Negative Binomial model for crash frequency prediction. The overall performance of the ANN model was better than the Negative Binomial model for highway sections with one or more accidents, while the Negative Binomial model had a better performance for sections with zero accidents. (Xie, Y., Lord, D., & Zhang, Y., 2007) compared Bayesian Neural Network (BNN) and Back Propagation Neural Network (BPNN) with the Negative Binomial model for traffic accident frequency prediction on rural frontage roads. The authors found that both neural network models had better performance than the Negative Binomial model while the BNN model outperformed the BPNN by incorporating the Bayesian inference. However, the study of Li, Lord, Zhang, and Xie (2008) compared this model with the Support Vector Machine (SVM) model and found that the two models had similar performance while less time was needed for SVM.

**Convolutional Neural Network (CNN)**. Compared with the shallow neural networks, CNN can capture the spatial information and has been widely adopted in image-enabled problems. Wenqi, Dongyu, and Menghua  (2017) mapped the explanatory variables into state matrices and fed them into a CNN model with two hidden layers to detect crashes on highway segments. The model had better performance than the BPNN model. Huang, Wang, and Sharma (2020) organized real-time traffic as images and fed them into a CNN model to detect crashes on highway segments given a specified traffic condition. The temporal information was also added into the model through the last fully connected layer. Chen, Song, Yamada and Shibasaki (2016) trained a deep model of Stack denoise Autoencoder (SdAE) by using traffic accident data and GPS data to help understand the relationship between human mobility and traffic crashes. The model can generate a real-time citywide accident risk map when given real-time GPS data. The CNN method also enables traffic crash detection using video data (Formosa, N., Quddus, M., Ison, S., Abdel-Aty, M., & Yuan, J., 2020). However, the vision-based method requires the backup of a large amount of computing resources and information storage space.

**Long Short-Term Memory (LSTM)**. While the CNN model can capture the spatial information, LSTM has very good performance in capturing the periodic information and has been widely adopted in many sequence learning problems. With respect to the crash prediction problem, Ren, Song, Liu, Hu, and Lei (2017) proposed an updated LSTM model to predict average crash frequency per hour for each predefined grid cell (1KM*1KM) in Beijing for the same time period of the most recent 3 days. The model stacked four LSTM layers and three fully connected layers together. Average accident frequency in previous time periods was fed into the first LSTM layer and the location information of the grid cell was fed into the first fully connected layer. Yuan, Abdel-Aty, Gong, and Cai (2019) used a two-layer LSTM model to predict whether there would be a crash for signalized intersections in Florida for the next five to ten minutes. The inputs of the model included individual vehicle speed data, signal timing data and vehicle-counts data aggregated in five-minute periods as well as weather records in the nearest time period. The model had better performance than the conditional logistic model.

**Combined networks**. Some researchers tried to capture spatial patterns and temporal dependency at the same time by using the CNN and LSTM together. Yuan, Zhou, and Yang (2018) represented the entire State of Iowa by a map with 128-by-64 grids and proposed a Hetero-ConvLSTM model to help predict daily crash frequency in each grid cell. Input features included road network, road conditions, satellite image, rainfall, weather conditions, traffic volume and time information, each being represented by a 128-by-64-by-1 tensor. The authors addressed the spatial heterogeneity issue by training several ConvLSTM models and combining their results together. Li, Abdel-Aty, and Yuan (2020) concatenated a two-layer LSTM model and a two-layer CNN model in parallel. Real-time signal timing, queuing and waiting time, traffic volume, average vehicle speed and weather-related variables were fed into the parallel LSTM-CNN model to help predict crash risk on arterials in real time. The authors also compared the t-Distributed Stochastic Neighbor Embedding (t-SNE) of raw data with the extracted features from the last layer of the LSTM-CNN model. The crash and non-crash events were almost separable when being presented by the extracted features, while they were tangled together when being represented by the raw dataset. Bao, Liu, and Ukkusuri (2019) joined CNN, LSTM and ConvLSTM as a synthesis model to help predict the sum of the severity level of all potential crashes in each predefined grid cell in New York. Variables that were spatially varied but temporally static were fed into the CNN model; variables that were temporally varied but spatially static were fed into the LSTM model, and variables that were both spatially and temporally varied were fed into the ConvLSTM model. The outputs of the three sub-models were combined as one dense vector and then transformed into the final output through several fully connected layers.

The above is a summary of major methods regarding crash prediction. Some of them have also been adopted for other topics such as travel time prediction and travel demand forecasting. It is worth noting that most of the models mentioned in this chapter used traffic flow data as the primary input. With increasing access to more real-time behavior data through smartphone applications, “many auto insurers are already using mobile telematics to assess risk and promote safer driving behaviors” (Business Wire, 2020). There are also start-ups working on apps that can alert drivers about potential nearby hazards using AI and machine learning models, smartphones, and commodity car hardware. 

While it must be admitted that real time traffic flow can reflect the true traffic condition, there are several deficiencies when applying it for crash prediction. The first and most important one is that real time traffic surveillance systems are not as prevalent as expected. For example, for the Champaign-Urbana region, real-time traffic flow data is not available. The second one is that, based on studies done in other regions with real-time traffic flow data, real time traffic flow data are not always accurately reported and sometimes are not reported at all, which would lead to an inaccurate result. Sensor malfunction has been a problem for a long time and has not been completely solved. In this sense, real time traffic flow data may not be a reliable resource for crash prediction. The third one is that traffic managers may not be able to capture the risk instantly and may not have enough time to prepare countermeasures if the real time traffic flow data are the main inputs of the crash prediction model.

In addition, some of the big data driven models mentioned above are complex to estimate and often cannot be generalized to other data sets. According to Mannerling and Bhat (2014), “empirical evidence from many studies also suggests that the superiority of one methodological approach over another can be very data dependent.” Models present various performance levels in studies with different data sizes, preprocessing methods, and variables modeled. Mannerling and Bhat (2014) state, “Even with the same data comparison of models which are often non-nested can leave much to be desired in terms of defensible statistical evidence." 

## 3. Research Methodology

### 3.1 Research Questions
This project requires a careful selection of the time/space scales for analysis, including an improved set of explanatory variables and/or unobserved heterogeneity effects to derive the most sensible results. This section details the research scope by answering the following questions:</br>

&nbsp;&nbsp;&nbsp;&nbsp;**What's predicted?**
 	Crash risk: the likelihood of crash occurrence

&nbsp;&nbsp;&nbsp;&nbsp;**What's the time horizon?**
  Training the data for 9 years (2014-2021), testing data for one year (2022), future prediction for one year (2050)

&nbsp;&nbsp;&nbsp;&nbsp;**What's the spatial dimension?**
  Segments

These questions will formulate the goal of the project: identify the roadway segments with the highest risk of crashes.

### 3.2 Models Tested
With the limitations of the models and the special characteristics of the crash data in mind, this project will experiment with the following methods to identify the best model for predicting crash risks in Champaign County.
* Logistic Regression
* Support Vector Machines
* Random Forest
* Extreme Gradient Boosting

This study analyzed nine years of crash data (2014-2022) from Champaign County. Recognizing the distinct crash patterns and roadway conditions in rural versus urban areas, separate models were developed for each area:
* A binary model to predict the likelihood of a crash.
* A multiclass model to predict the likelihood of low crash frequency (no crash), medium crash frequency (at least one crash), or high crash frequency (two or more crashes).

To check the accuracy of the prepared models, crashes for the Champaign-Urbana Metropolitan Planning Area (MPA) for the year 2022 were predicted and the results were then compared with the observed crashes for 2022.

This project also predicted crashes for future year 2050. Three scenarios were used for the year 2050 for this project. These are:
* Business-As-Usual: historic growth trends and mode choices continue
* Optimistic: Moderate, compact growth with fewer single-occupancy vehicles
* Transformative: Increased core and fringe growth with innovation-based mode choices

Although these scenarios have many elements to them, our models mainly used population and Annual Average Daily Traffic (AADT) for these different scenarios. Crashes and risk factors for different roadways for urban and rural roads in Champaign County were predicted for the above three scenarios. The future population and AADT values for 2050 were from the CUUATS Land Use Model and Travel Demand Model (TDM). More detail on these models can be found here: [Scenario Modeling](https://ccrpc.gitlab.io/lrtp-2050/vision/model/).

## 4. Data Sources
For this project, three types of data sources were used:
* Traffic Crash data
* Roadway Segment data
* Demographic data

### 4.1 Traffic Crash Data
Traffic crash data was retrieved from IDOT. Nine years of crash data (2014 to 2022) was used for this project. Although initially 2013 crash data was included in the project, it was found that 2013 experienced 10% fewer crashes than other years, so it was later excluded from the analysis. For training data, 2014 to 2021 crash data was used, and for testing data 2022 crash data was used.

### 4.2 Roadway Segment data
Highway network data from the Illinois Department of Transportation (IDOT) spanning 2014 to 2022 was utilized in this project, alongside data from 2009 to 2013, which was applied for feature extrapolation. Additionally, street segment centerline data from the Champaign County Regional Planning Commission’s Planning and Community Development (CCRPC PCD) was incorporated into the analysis.

### 4.3 Demographic Data
Demographic data on total population by block group was obtained from the U.S. Census Bureau. Population density was subsequently calculated by block group for each year from 2014 to 2022.

## 5. Exploratory Data Analysis
This section shows the descriptive statistics of different variables of the dataset for the year 2014 to 2021.

### 5.1 Crash Injury Severity
The bar plot (Figure 5.1) illustrates the total crash-related injuries and fatalities across different years (2014–2021). Each subplot focuses on one severity level: Total Fatalities, A Injuries, and B Injuries. The "Total Fatalities" subplot shows annual variations, with 2016 exhibiting the highest number of fatal crashes and 2019 the lowest. It is also notable that 2021 experienced a rise in fatal crashes compared to previous years. The A Injuries subplot captures severe, non-fatal injuries. The highest counts occurred in 2015, 2016, and 2017, with notable declines in 2019 and 2020. The B Injuries subplot, which includes moderate injuries, shows relatively high counts across most years, with a peak in 2017 and a consistent level of around 400–500 injuries in other years. The steadiness of B injuries compared to A injuries and fatalities may indicate that moderate injuries are more resilient to external influences.

<h4 style="text-align: center;">Figure 5.1: Crash Injury Severity</h4>
{{<image src="Fig5.1.png"
alt="Figure 5.1: Crash Injury Severity"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

### 5.2 Roadway and Environmental Conditions
The bar plots below (Figure 5.2) represent the distribution of various factors related to roadway, environmental, and crash conditions. Each subplot provides insights into the predominant conditions or characteristics present in the dataset, which may influence or correlate with crash frequency and severity. 

<h4 style="text-align: center;">Figure 5.2: Bar Plots for Roadway and Environmental Conditions</h4>
{{<image src="Fig5.2.png"
alt="Figure 5.2: Bar Plots for Roadway and Environmental Conditions"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

#### Road Surface Condition
The "RoadSurfaceCond" plot shows that the majority of crashes occur on dry surfaces, followed by wet conditions. Crashes on icy and snow/slush-covered surfaces are relatively infrequent, which could be attributed to less driving during severe weather or potentially effective driver caution in such conditions. 

#### Light Condition
The "LightCondition" plot highlights that most crashes happen during daylight, with fewer crashes occurring in darkness with lighted roads and darkness without lighting. This pattern likely reflects higher vehicle traffic volumes during daylight hours, making daylight crashes more frequent overall. However, the presence of crashes in dark conditions indicates that visibility issues at night contribute to crash risk, especially in areas without sufficient lighting.

#### Weather Condition
The "WeatherCondition" plot indicates that clear weather is the most common condition during crashes, followed by rain. Adverse weather conditions like fog, sleet/hail, and blowing snow contribute to fewer crashes, possibly due to reduced traffic in severe conditions or increased driver caution. Clear weather dominance suggests that, similar to dry road surfaces, the commonality of this condition leads to a higher number of crashes, independent of inherent safety.

#### Roadway Functional Class
The "RoadwayFunctionalClass" subplot categorizes crashes by road type. Local roads and minor arterials in urban areas have the highest crash counts, reflecting increased vehicle interaction and congestion in urban settings. In contrast, principal arterials and interstates show fewer crashes, possibly due to fewer intersections and controlled access, which reduce conflict points. 

#### Road Alignment
The "RoadAlignment" plot shows that most crashes happen on straight and level roads, with curves and grades accounting for fewer crashes. The high frequency on straight roads could be due to their prevalence in the road network and the potential for driver inattention in these less demanding alignments. However, crashes on curves and grades suggest areas where additional caution and road design considerations might mitigate risks associated with more complex alignments.

#### Type of First Crash
The "TypeofFirstCrash" plot shows that rear-end and angle crashes are the most common initial crash types. These types are indicative of typical urban and suburban driving environments, where intersections and high vehicle density lead to rear-end collisions (from sudden stops) and angle crashes (from crossing traffic). Fixed object and sideswipe same direction crashes are also common, which might indicate factors like lane changes and roadside hazards. The table below shows the distribution of crashes for each type of crash.

<rpc-table url="Table5.1.csv"
table-title="Table 5.1: Distribution of Crashes for Each Crash Type, 2014-2021"
source=" IDOT Roadway Crash Data"
text-alignment="1,c"></rpc-table>

These plots reveal that crashes are most frequently associated with common conditions: dry surfaces, daylight, clear weather, and urban local roads. This pattern suggests that environmental conditions, while impactful, may not be the primary risk factors in many crashes. Instead, factors related to road design, traffic density, and driver behavior likely play substantial roles, warranting further analysis and targeted safety interventions to reduce crash rates under these prevalent conditions.

### 5.3 Monthly, Weekly, Daily, and Hourly distribution
The bar plots in Figure 5.3 illustrate the temporal distribution of crashes across different timeframes: month, day of the month, day of the week, and hour of the day. These distributions provide insights into patterns of crash frequency over various temporal categories, which can help identify high-risk periods and inform safety interventions.

<h4 style="text-align: center;">Figure 5.3: Bar plots for Monthly, Weekly, Daily and Hourly Distributions</h4>
{{<image src="Fig5.3.png"
alt="Figure 5.3: Bar plots for Monthly, Weekly, Daily and Hourly Distributions"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

#### Crashes by Month
The plot shows that crashes are relatively evenly distributed throughout the year, with slight variations across months. The months from October to December show a marginal increase in crashes compared to other months, while mid-year months (April to July) have a slight dip. This trend correlates with the school season, as during summer the schools and universities are off, and students come back to class in the fall.

#### Crashes by Day of Month
The plot indicates a slight downward trend in crashes as the month progresses, with a notable decrease at the end of the month. This pattern could be attributed to reporting practices, changes in traffic volume, or other temporal behaviors, though the overall variation is modest. 

#### Crashes by Day of Week
The plot reveals a noticeable peak in crashes on Fridays, with Saturday and Sunday having relatively fewer crashes compared to weekdays. This distribution suggests that Fridays may have higher crash risks, possibly due to increased traffic volume from end-of-week activities, social events, and longer travel times associated with weekends. Lower crash rates over the weekend may result from fewer commuters and potentially lower traffic congestion in urban areas.

#### Crashes by Hour of Day
The plot demonstrates a clear pattern in crash frequency throughout the day. Crashes increase gradually in the morning, peaking around the late afternoon to early evening (3:00 pm to 6:00 pm), then gradually decline towards nighttime. This peak aligns with typical commuting hours, suggesting that higher traffic volumes and driver fatigue during these times contribute to increased crash rates. The lower frequency during early morning and late-night hours reflects reduced traffic volumes during these periods.

## 6. Data Processing
This chapter details the steps followed to prepare the roadway and demographic data into static and temporal features (variables). It also shows how the crash data was processed into the target feature (number of crashes per year on a roadway segment). Figure 6.1 shows the general workflow of the project.

<h4 style="text-align: center;">Figure 6.1: Project Workflow of the Traffic Safety Forecasting Tool Process</h4>
{{<image src="SFT_process_diagram.png"
alt="Figure 6.1: Project Workflow of the Traffic Safety Forecasting Tool Process"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

### 6.1 Prepare Static Features
IDOT network static features and CCRPC network static features were loaded into QGIS for the years 2009 to 2022, and then merged to get the final data frame for static features. As the merged data frame had missing values, Table 6.1 shows the approach that was followed to populate the missing values.

<rpc-table url="Table6.1.csv"
table-title="Table 6.1: Strategies to Populate Missing Values"
source=" CCRPC"></rpc-table>

### 6.2 Prepare Temporal Features
To prepare temporal features, features which change over time were extracted from CCRPC network features and IDOT network features, then merged.

#### 6.2.1 Annual Average Daily Traffic
A road segment may have AADT records for certain years but not for other years. The AADT records need to be extrapolated from the existing records. Therefore, AADT data for each of the segments were extrapolated and merged. However, doing this operation created a lot of missing values. Below are the guidelines that were followed to populate the missing AADT values:
* For each segment, if AADT numbers are missing for all years, average AADT for the corresponding functional classification for each year was calculated.
* For each segment, if AADT numbers are available for only one-year, average AADT yearly percentage change for the corresponding functional classification was applied to calculate AADT for  each year.
* For each segment, if AADT numbers were available for at least two years, average AADT yearly percentage change for this segment between years with AADT numbers was calculated, and the percentage change to calculate AADT for the in-between years was applied. Average yearly percentage change by functional classification was applied for not-in-between years with missing AADT numbers.

#### 6.2.2 Heavy Vehicle Counts (HCV)
HCV data was extracted and prepared for each year. Missing HCV values were extrapolated using the following rules:
* For each segment, if HCV numbers are missing for all years, pass.
* For each segment, if HCV numbers are available for only one year, apply average HCV yearly percentage change for the corresponding functional classification to calculate HCV for each year.
* For each segment, if HCV numbers are available for at least two years, calculate average HCV yearly percentage change for this segment between years with HCV number, and apply the percentage change to calculate HCV for the in-between years. Apply average yearly percentage change by functional classification for not-in-between years with missing HCV numbers.

Lastly, IDOT highway network HCV and CCRPC streets HCV by segments were combined.

#### 6.2.3 Pavement Condition 
All Conditioning Rating Score CRS data for each year was merged and pavement conditions were added from CCRPC streets. Pavement rate of deterioration was calculated by year and functional classification. It was found that 89.07% of roadway segments didn't show change in the pavement condition category during study period. For the rest of segments that did show change in the pavement condition category, there was no clear pattern to extrapolate the trend. Therefore, for roadway segments that don't have pavement condition rating from the IDOT network, the “pavement_condition” attribute from the CCRPC streets layer was applied, which includes roadway condition ratings reported by municipalities.

#### 6.2.4 Population Density
Total population by block group from the U.S. Census Bureau American Community Survey (ACS) was retrieved for years 2014 to 2022. Roadway networks were then assigned to the block groups. Some segments were found to have duplicate records, which were identified and cleaned. 

### 6.3 Crash Location and Time
Each crash was projected to its segment on the map. The years were then assigned to the crashes. After that, PCD roadway (sub) segments were projected using the “espg:3435” projection. Crashes were then reprojected using projection espg:3435. Finally, the crashes were summarized per segment per year.

### 6.4 Features Correlation Test
Pairwise correlation of attributes was computed, excluding the null values, which created the following heatmap, showing the correlation of different features. 

<h4 style="text-align: center;">Figure 6.2: Heatmap showing correlation of different features</h4>
{{<image src="Fig6.2.png"
alt="Figure 6.2: Heatmap showing correlation of different features"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

#### 6.4.1 Features & Crash/No Crash
**Crash/No Crash (0,1)**, the dependent variable is the categorical variable.
* Association with Categorical variable X axis: Categorical features, Y axis: Crash/No Crash
    * Cramer's V test should be used.
* **Association with Numerical variable** X axis: Crash/No Crash, Y axis: Numerical features
    * Point-biserial correlation test should be used to measure the relationship.
    * 0 implies no correlation. Correlations of -1 or +1 imply a determinative relationship.

#### 6.4.2 Features & Crash Counts
**Crash counts (number of crashes (0,1,2, 3,...))**, the dependent variable is the discrete variable
  * **Association with Categorical variable** X axis: Categorical features, Y axis: Crash Counts
    * Nominal (Binary) Y: Point biserial test to determine if the existence of a certain characteristic would affect crash counts.
    * Nominal (Multi-group) Y: One way ANOVA & Kruskal-Wallis H test to test if there are statistically significant differences between different groups of a feature.
    * Ordinal Y: Kendall tau test to measure the strength and direction of association.

* **Association with Numerical variable** X axis: Numerical features, Y axis: Crash Counts
  * Spearman test is used if there is a monotonic relationship between X (Numerical variable) and Y (number of crashes)
  * 0 implies no correlation. Correlations of -1 or +1 imply an exact monotonic relationship.

The above tests are done on balanced and imbalanced datasets to measure the correlation and strength of different features. The table below shows the statistics for the association of nominal (Multiclass) variables for urban crash data in terms of F_stats, p_val, confidence_level and critical_F.

<rpc-table url="Table6.2.csv"
table-title="Table 6.2: Association of Nominal (Multiclass) Variable"
source=" CCRPC"></rpc-table>

* **F_stats (F-statistic)**:
  * The **F-statistic** is the result of an **F-test**, which is a statistical test to compare two variances or determine if a model (often in regression or ANOVA) fits the data significantly better than a simpler one.
  * A high F-statistic value indicates a greater likelihood that at least one of the tested groups (e.g., different categories or variables) has a significantly different effect. It essentially compares the variance between groups to the variance within groups.

* **p_val (p-value)**:
  * The **p-value** is a measure of the probability that the observed results (or something more extreme) would occur if the null hypothesis were true (in this case, often that there is no significant effect or difference).
  * A lower p-value (typically below 0.05 or 0.01) suggests that the observed data are unlikely under the null hypothesis, leading to its rejection. In this table, all p-values are 0, which usually means that the results are statistically significant at conventional confidence levels (like 95% or 99%).

* **confidence_level**:
  * The **confidence level** (here 0.95, or 95%) is the probability that the confidence interval (around a parameter estimate) contains the true parameter value.
  * In hypothesis testing, a 95% confidence level means there’s a 5% chance of rejecting the null hypothesis if it’s true (Type I error rate of 0.05). This confidence level is set in advance and indicates the degree of certainty or robustness required in the statistical test.

* **critical_F (Critical F-value)**:
  * The **critical F-value** is the cutoff point on the **F-distribution** for the specified confidence level (95% in this case).
  * If the calculated F-statistic is greater than this critical F-value, the null hypothesis is rejected, meaning the variable or factor is statistically significant.
  * This value is determined based on the degrees of freedom and confidence level for the test. For example, if an F-statistic is above the critical F-value at a 95% confidence level, it suggests a statistically significant effect for that variable.

### 6.5 Data Preprocessing for Forecasting Crashes for the Year 2050
To prepare the data for predicting crashes for 2050, first the CCRPC roadway network for the future year was prepared. All the features except pop_den (population density) were then updated. If any feature value for 2050 was found to be missing, the missing value was filled with the value used for 2020. From the CUUATS Travel Demand Model (TDM), three scenarios were used for this project: Business-As-Usual, Optimistic, and Transformative. 

## 7. Binary Model Tuning and Implementation
This chapter demonstrates the process of fine-tuning and implementing the best algorithm identified for binary class modeling (XGB) for the rural area. 

### 7.1 Comparing Machine Learning Algorithms
Four machine learning algorithms are tested in this study.
* Logistic regression
* Support Vector Machine (SVC)
* Random Forest
* eXtreme Gradient Boosting (XGBoost)

The hyperparameter tuning process of SVC, Random Forest, and XGBoost are conducted on Amazon Web Services Elastic Compute Cloud (AWS EC2).
* The initial model performance evaluation is based on the F2 score.
* While the model tuning process is based on the F2 score, the final model was selected based on both F2 score and false positive rate. More specifically, the top five models that have the highest f2 score would be selected as candidates. Then, the model with the lowest false positive rate among the top five candidates would be selected as the final model.

#### 7.1.1 Model Output
The highest F2 scores obtained by each algorithm are listed in the following table. It can be observed that eXtreme Gradient Boosting (XGBoost) outperformed the other algorithms in terms of F2 score. Thus, the model fine tuning process was based on the XGBoost algorithm.

<rpc-table url="Table7.1.csv"
table-title="Machine Learning (ML) Model Results for Binary Crashes, Urban Roads"
source=" CCRPC"></rpc-table>

* For this project, the ability to capture risky roads is more essential than to be able to identify which roads are safe. Thus, the F2 score is selected as the evaluation criteria, and the model is guided to capture as many risky roads as possible.
* While the F2 score evaluates true positive counts, false positive counts, and false negative counts, the number of true negative cases is not evaluated. The true negative counts would not be evaluated if the F2 score is the only criterion used for the model selection.
* The true negative counts are an essential element for calculating the false positive rate. Omitting the true negative counts in the evaluation process might lead to the problem of having a high false positive rate.

Thus, while the model fine-tuning process is based on the F2 score, the final model was selected based on both the F2 score and the false positive rate.

## 8. Multiclass Classification Modeling
The purpose of the multiclass model is to find segments that have high crash risks (Class2), which have two or more crashes. The model is designed to capture as many Class2 roads as possible. The primary goal is to capture segments of high risks; the second goal is to identify segments that might have crashes (Class1), and the last goal is to minimize the false alarm rate. To achieve these goals, an evaluation score called weighted-confusion_matrix-score was designed.

### 8.1 Weighted-Confusion_Matrix-Score
When predicting (classifying) the roadway risk level in the real world, the costs of different misclassification cases are different and the benefits of correctly classifying different classes should be valued differently. For example, predicting (classifying) true Class2 as Class1 is much more dangerous than predicting true Class1 as Class0; predicting (classifying) true Class0 as Class2 is much more troublesome than predicting true Class0 as Class1, and correctly identifying Class2 is more valuable than correctly identifying Class0 in the context of roadway risk assessment. To address this need, a scoring matrix is designed to evaluate correct classifications and misclassifications of different scenarios.

In the scoring matrix, a negative value is assigned to each misclassification cell and a positive value is assigned to each correct classification cell. In other words, the "win(s)" of the model are measured by multiplying the correct classification cells with positive values in the scoring matrix, while the "loss(es)" of the model are measured by multiplying the misclassification cells with negative values. The more dangerous/troublesome the misclassification case is, a smaller negative value (higher absolute value) is assigned. The more valuable the correct classification case is, a higher positive value is assigned.

To evaluate all the models at the same scale, the weighted_confusion_matrix_score, which is between [-2,1], is developed. The weighted_confusion_matrix_score is calculated by dividing the total points earned by the model by the highest number of total points a model could earn (classifying all the classes correctly). The following is an example Scoring Matrix:

<rpc-table url="Table8a.csv"
text-alignment="1,c"></rpc-table>

For example, in a dataset, Class0 has 124 samples, Class1 has 33 samples, and Class2 has 14 samples. The best performance the model could achieve is:

<rpc-table url="Table8b.csv"
text-alignment="1,c"></rpc-table>

The highest number of points a model could earn, which is the denominator, is:<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(124 * 1) + (33 * 2) + (14 * 3) = 232

The worst performance a model could achieve is:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(124 * (-2)) + (33 * (-4)) + (14 * (-6)) = -464

Thus, the worst weighted_confusion_matrix_score a model could have is -464/232 = -2

Now, let’s suppose a model results in the following confusion_matrix:

<rpc-table url="Table8c.csv"
text-alignment="1,c"></rpc-table>

The total points earned by the model is calculated as:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(100 * 1) + (4 * (-1)) + (20 * (-2)) + (1 * (-4)) + (20 * 2) + (12 * (-3)) + (2 * (-6)) + (4 * (-5)) + (8 * 3) = 48

The weighted confusion_matrix_score of this model is 48/232 = 0.207

Now, to evaluate the model:
  * The top five models with the highest class-weighted f2 score were selected.
  * The model that had the highest weighted confusion_matrix score among the top five models was selected.

The table below shows the statistics from the best selected model, with 1 being the optimal score:

<rpc-table url="Table8.1.csv"
table-title="Table 8.1: Model Parameters of the Best Selected Model"
source=" CCRPC"></rpc-table>

## 9. Results Analysis, Discussion and Mapping

The comparison between observed and predicted crashes (Figure 9.1) for urban roadways in Champaign County for the year 2022 derived from binary crash models (crash/no crash) highlights both areas of success and opportunities for improvement in the model's predictions. For **Interstate** and **Major Arterial** roads, the model slightly underpredicts crash occurrences, with differences of about 4% and 5%, respectively, suggesting there may be some additional risk factors for these high-speed, high-traffic roads that could be better captured. On the other hand, the predictions for **Minor Arterial** and **Major Collector** roads align quite well with the observed data, with only small discrepancies of around 5% and 1%, indicating the model performs effectively for these functional classes. The model does tend to overestimate crashes for **Minor Collector** roads and for **Local Roads or Streets**, where it overpredicts by about 15%. This suggests the model may be slightly overemphasizing certain factors in these roadways. Overall, the model shows a good level of accuracy in predicting crashes for certain road types, especially **Major Collectors** and **Minor Arterials**, while there is still room for refinement in addressing underpredictions on higher-capacity roads and overpredictions on local streets.

<h4 style="text-align: center;">Figure 9.1: Percentage of Crashes by Roadway Functional Class, 2022</h4>
{{<image src="Fig9.1.png"
alt="Figure 9.1: Percentage of Crashes by Roadway Functional Class, 2022"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

The comparison between the observed and predicted results for urban roadway crashes in Champaign County for the year 2022 (Figures 9.2) derived from the multiclass model shows some important distinctions. In the observed data, **86.0%** of roads fall under the **low-risk** category, while **8.3%** are **medium-risk** and **5.7%** are classified as **high-risk**. The model's predictions, however, indicate a slightly higher proportion of **low-risk** roads at **91.8%**, with **3.2%** predicted to be **medium-risk** and **5.0%** as **high-risk**. The model slightly overpredicts the number of **low-risk** roads and underpredicts the **medium-risk** category, while its predictions for **high-risk** roads are relatively close to the observed data. Overall, the model appears to be conservative in predicting **medium-risk** roads, and while there is a gap between observed and predicted values in this category, the model performs reasonably well in estimating **high-risk** roads, showing only a 0.7% difference from the observed data. This indicates that while the model tends to err on the side of predicting more roads as **low risk**, it captures the **high-risk** road segments with decent accuracy.  

<h4 style="text-align: center;">Figure 9.2: Percentage of Crashes by Risk Factor, 2022</h4>
{{<image src="Fig9.2.png"
alt="Figure 9.2: Percentage of Crashes by Risk Factor, 2022"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

Three scenarios were used for the year 2050 for this project:<br>
  * Business-As-Usual: historic growth trends and mode choices continue
  * Optimistic: Moderate, compact growth with fewer single-occupancy vehicles
  * Transformative: Increased core and fringe growth with innovation-based mode choices

Although these scenarios have many elements to them, our models mainly used population and Annual Average Daily Traffic (AADT) of the roadway network for these different scenarios. Crashes and risk factors for different roadways for urban and rural roads in Champaign County were predicted for the above three scenarios. The future population and AADT values for 2050 were taken from the CUUATS Land Use Model and Travel Demand Model (TDM). More detail on these models can be found here: [Scenario Modeling](https://ccrpc.gitlab.io/lrtp-2050/vision/model/).

### 9.1 Comparative Analysis of Urban Roadway Crash Predictions Across Three Scenarios (Binary Models)
Figure 9.3 illustrates the percentage of predicted roadway crashes by functional class for three different urban traffic scenarios: Business-As-Usual, Optimistic, and Transformative. The functional classes include Interstates, Major Arterials, Minor Arterials, Major Collectors, Minor Collectors, and Local Roads or Streets. A detailed comparative analysis is presented below.

<h4 style="text-align: center;">Figure 9.3: Percentage Predicted Crashes by Functional Class for Each Scenario</h4>
{{<image src="Fig9.3.png"
alt="Figure 9.3: Percentage Predicted Crashes by Functional Class for Each Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

#### 9.1.1 General Overview and Trend Similarities
Across all three scenarios, a consistent pattern emerges where local roads or streets account for the highest percentage of predicted crashes, while minor collectors represent the lowest crash percentages. This trend is aligned with the general understanding of urban traffic patterns where local roads tend to see more interaction points, pedestrian movement, and complex intersections, contributing to higher crash probabilities. Conversely, minor collectors, serving a more limited traffic volume, experience fewer crashes.

#### 9.1.2 Predicted Crashes: Business as Usual Scenario
In the Business-As-Usual scenario, the distribution of crash predictions is as follows:
* Local Roads or Streets dominate with 38.3% of predicted crashes.
* Minor Arterials contribute significantly at 25.6%.
* Major Collectors and Major Arterials fall behind, predicting 17.8% and 11.3% of crashes, respectively.
* Interstates have the least impact at 4.7%, followed closely by Minor Collectors at 2.2%.

The substantial crash percentage for local roads suggests a lack of infrastructural changes or improvements that would mitigate urban crash occurrences in this scenario. This model assumes continued reliance on current urban infrastructure and traffic control systems without major interventions.

#### 9.1.3 Predicted Crashes: Optimistic Scenario
In the Optimistic Scenario, we observe a slight reduction in the crash percentages:
* Local Roads or Streets see a modest decline to 37.4%, although still maintaining the highest crash prediction rate.
* Minor Arterials increase slightly to 25.8%.
* Major Collectors increase slightly to 18.2%, indicating a marginal rise in predicted crashes.
* The percentages for Interstate roads increase to 4.8%, while Minor Collectors remain stable at 2.2%.

This scenario represents the highest level of intervention. However, local roads and streets still present a significant risk for crashes.

#### 9.1.4 Predicted Crashes: Transformative Scenario
In the Transformative Scenario, there is a slight reduction in the crash percentages compared to the Business-As-Usual scenario:
* Local Roads or Streets still maintain the highest predicted crashes, though slightly increased at 38.9%.
* Minor Arterials see a marginal decrease to 25.2%.
* Major Collectors remain constant at 17.8%, showing no change from the BAU scenario.
* Interstate roads and Minor Collectors stay nearly static at 4.6% and 2.2%, respectively.

#### 9.1.5 Predicted Crashes: Scenario Comparisons
* Local Roads or Streets consistently exhibit the highest percentage of crashes across all scenarios. The crash percentage is highest in the Transformative scenario at 38.9% and lowest in the Optimistic scenario at 37.4%. The marginal difference across scenarios suggests that the scenario changes do not radically change the crash dynamics in the most vulnerable roadways.
* Minor Arterials show a slight fluctuation across the scenarios, peaking at 25.8% in the Optimistic scenario. The higher percentage of crashes in this road type suggests that minor arterials are particularly prone to crashes, possibly due to increased traffic flow and interaction between faster-moving vehicles and pedestrian infrastructure.
* Major Collectors exhibit relative stability in the Business-As-Usual and Transformative scenarios (17.8% in both), but slightly increase in the Optimistic scenario to 18.2%. This minor increase may indicate that changes in population and network AADT in the Optimistic scenario create new points of interaction, leading to a marginal rise in crash occurrences.
* Interstate roads maintain the lowest crash percentages across scenarios, likely due to their controlled access and lower levels of interaction compared to local streets. There is a minor increase from 4.6% in the Transformative scenario to 4.8% in the Optimistic scenario.
* Minor Collectors remain constant at 2.2% across all scenarios, suggesting that these roads, which serve fewer vehicles, are not significantly impacted by any population or AADT changes in any of the three scenarios.

While each scenario presents some changes in crash predictions, Local Roads or Streets remain the most vulnerable, with only marginal reductions in the Optimistic scenario. Minor Arterials and Major Collectors fluctuate slightly, indicating that these functional classes are sensitive to urban development strategies and changing traffic patterns. Interstate and Minor Collectors consistently exhibit the lowest crash rates. The overall trend across all scenarios suggests that local roads and minor arterials will continue to pose significant challenges for crash reduction efforts. 

#### 9.1.6 Comparison of Binary Model 2050 Predicted Results with 2022 Observed Data
The comparison of predicted crash distributions for the three future scenarios with the observed crash data from 2022 reveals some key patterns.

<h4 style="text-align: center;">Figure 9.4: Percentage 2022 Observed Crashes vs. Predicted Crashes for 2050 Scenarios</h4>
{{<image src="Fig9.4.png"
alt="Figure 9.4: Percentage 2022 Observed Crashes vs. Predicted Crashes for 2050 Scenarios"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

As can be seen in Figure 9.4, the observed crashes in 2022 were 7.6%, while the predictions for 2050 across all scenarios show lower values, ranging from 4.6% to 4.8%, suggesting a slight decrease in crashes for this class.

For Major Arterial roads, the observed crash rate in 2022 was 14.2%, and predictions for 2050 are lower across all scenarios, between 11.3% and 11.7%, indicating a predicted reduction in crashes for this road type.

In the case of Minor Arterial roads, the observed value was 24.8% in 2022, and predictions for 2050 remain similar, between 25.2% and 25.8%, showing little variation from the observed crashes.

For Major Collectors, the observed crash rate was 14.7%, and the 2050 predictions are slightly higher, between 17.8% and 18.2%, indicating a slight increase in predicted crashes for this category.

For Minor Collectors, the observed crashes were minimal at 0.9% in 2022, while the 2050 predictions are consistently higher, at 2.2% in all scenarios, indicating an anticipated increase in crashes for this type of roadway.

Finally, for Local Roads or Streets, the observed crash rate was 37.7%, and predictions for 2050 range from 37.4% to 38.9%, showing little difference from the observed data.
Figure 9.5 shows the observed crashes for 2022 while Figures 9.6 to 9.8 show the predicted crash results by 2050 scenario from the binary models for urban crashes on the Champaign County base map.

<h4 style="text-align: center;">Figure 9.5: Map of Observed Crashes by Functional Class, 2022</h4>
{{<image src="Fig9.5.png"
alt="Figure 9.5: Map of Observed Crashes by Functional Class, 2022"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

<h4 style="text-align: center;">Figure 9.6: Map of Crash Predictions from Binary Model for 2050 Business-As-Usual Scenario</h4>
{{<image src="Fig9.6.png"
alt="Figure 9.6: Map of Crash Predictions from Binary Model for 2050 Business-As-Usual Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

<h4 style="text-align: center;">Figure 9.7: Map of Crash Predictions from Binary Model for 2050 Optimistic Scenario</h4>
{{<image src="Fig9.7.png"
alt="Figure 9.7: Map of Crash Predictions from Binary Model for 2050 Optimistic Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

<h4 style="text-align: center;">Figure 9.8: Map of Crash Predictions from Binary Model for 2050 Transformative Scenario</h4>
{{<image src="Fig9.8.png"
alt="Figure 9.8: Map of Crash Predictions from Binary Model for 2050 Transformative Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

In general, the predictions show a slight decrease in crashes for higher-capacity roads like Interstates and Major Arterials, while predicting small increases in crashes for lower-capacity roads such as Major and Minor Collectors. For Local Roads or Streets and Minor Arterials, the predicted crash rates remain like the observed values from 2022.

### 9.2 Comparative Analysis of Road Risk Levels Across Scenarios for Urban Roads (Multiclass Models):
Figure 9.9 represents the percentage of urban roads categorized by predicted risk levels—Low Risk (predicted to have no crash), Medium Risk (predicted to have at least 1 crash), and High Risk (predicted to have two or more crashes)—across the three future scenarios: Business-As-Usual, Transformative, and Optimistic. Below is a comparative analysis of the findings.

<h4 style="text-align: center;">Figure 9.9: Percentage of Roads by Predicted Risk Levels for 2050 Scenarios</h4>
{{<image src="Fig9.9.png"
alt="Figure 9.9: Percentage of Roads by Predicted Risk Levels for 2050 Scenarios"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

#### 9.2.1 Low Risk Roads
Across all 2050 scenarios, Low Risk roads dominate the urban landscape, comprising over 93% of the total roads. Specifically, in the Business-As-Usual and Transformative scenarios, the percentage of low-risk roads is 93.2%, while it is slightly higher in the Optimistic scenario at 93.8%.
The minimal difference between the scenarios suggests that, regardless of population and AADT increase, the vast majority of roads will continue to be classified as low risk. 

#### 9.2.2 Medium Risk Roads
Medium Risk roads are consistently the smallest category across all three 2050 scenarios. In the Business-As-Usual and Optimistic scenarios, 1.9% of the roads are classified as medium risk, while in the Transformative scenario, this percentage increases slightly to 2.1%.

#### 9.2.3 High Risk Roads
The percentage of High-Risk roads varies more noticeably across the 2050 scenarios. In the Business-As-Usual scenario, 4.9% of roads are categorized as high risk, which decreases slightly in the Optimistic scenario to 4.3%. In the Transformative scenario, the percentage of high-risk roads is 4.7%. 

#### 9.2.4 Scenario Comparisons
Low Risk Roads are consistently the largest portion of the road network, with a slight increase in the Transformative scenario. The stability across the scenarios shows that low-risk roads are less sensitive to population and AADT increase.

Medium Risk Roads show the smallest percentage, with a slight increase in the Optimistic scenario, indicating that population and AADT increase may introduce moderate risk.

High Risk Roads vary the most between scenarios. The Transformative scenario shows the most promising reduction in high-risk roads (4.3%). Conversely, the Business-As-Usual scenario sees the highest percentage of high-risk roads.

While Low Risk Roads remain the overwhelming majority in all scenarios, the proportion of High-Risk Roads sees the most fluctuation, with the Transformative scenario showing the greatest potential for improving road safety. The Optimistic scenario does not significantly reduce the percentage of high-risk roads, highlighting the challenges of balancing complex urban transportation systems with safety. 

#### 9.2.5 Comparison of Multiclass Model Results with Observed Data
The comparison between the observed crash risk results for 2022 and the predicted crash risk results for 2050 across three different scenarios reveals some differences in the distribution of road risk categories.

<h4 style="text-align: center;">Figure 9.10: Percentage 2022 Observed Crashes vs. Predicted Crashes for 2050 Scenarios by Risk Factor</h4>
{{<image src="Fig9.10.png"
alt="Figure 9.10: Percentage 2022 Observed Crashes vs. Predicted Crashes for 2050 Scenarios by Risk Factor"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

In 2022, 86.0% of roads were classified as low-risk, with 8.3% as medium-risk, and 5.7% as high-risk (Figure 9.10). In all three scenarios for 2050, the proportion of roads classified as low-risk increases significantly to around 93.2% - 93.8%, while the share of medium-risk roads decreases to between 1.9% - 2.1%. The percentage of high-risk roads also sees a slight decline, ranging from 4.3% - 4.9%.

Figure 9.11 shows the observed data for the year 2022 while figures 9.12 to 9.14 show the prediction of roadway risk factor distribution for the three scenarios.

<h4 style="text-align: center;">Figure 9.11: Map of Observed Risk Factor for Roadway Crashes, 2022</h4>
{{<image src="Fig9.11.png"
alt="Figure 9.11: Map of Observed Risk Factor for Roadway Crashes, 2022"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

<h4 style="text-align: center;">Figure 9.12: Map of Crash Risk Predictions from the Multiclass Model for the 2050 Business-As-Usual Scenario</h4>
{{<image src="Fig9.12.png"
alt="Figure 9.12: Map of Crash Risk Predictions from the Multiclass Model for the 2050 Business-As-Usual Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

<h4 style="text-align: center;">Figure 9.13: Map of Crash Risk Predictions from the Multiclass Model for the 2050 Optimistic Scenario</h4>
{{<image src="Fig9.13.png"
alt="Figure 9.13: Map of Crash Risk Predictions from the Multiclass Model for the 2050 Optimistic Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

<h4 style="text-align: center;">Figure 9.14: Map of Crash Risk Predictions from the Multiclass Model for the 2050 Transformative Scenario</h4>
{{<image src="Fig9.14.png"
alt="Figure 9.14: Map of Crash Risk Predictions from the Multiclass Model for the 2050 Transformative Scenario"
position="full">}}
<p style="text-align: center; font-size:80%; margin:0 auto; font-style: italic;}">Image: CCRPC</p>
<p style="text-align: center; font-size:80%;">Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'.</p>

In summary, the predictions for 2050 across all scenarios show a higher proportion of low-risk roads and fewer medium- and high-risk roads compared to the observed data for 2022. The multiclass model generally forecasts a safer distribution of road segments in the future, with a notable reduction in the percentage of roads classified as medium- and high-risk.

## 10. Conclusion

Looking to 2050, the binary model that calculates predicted crash percentages by roadway functional class suggests that all future scenarios will see increases in crashes compared to the 2022 observed crashes. The Transformative Scenario would be the most beneficial for Interstates, Major Arterials, Minor Arterials, and Major Collectors. However, Local Roads or Streets, where most crashes occur, would most benefit from implementation of the Optimistic Scenario, which is the preferred scenario in the Long-Range Transportation Plan 2050.

The multiclass model that calculates predicted crash percentages by level of risk suggests that all future scenarios will see increases in low-risk roadways (predicted to have no crashes) and decreases in medium (predicted to have at least one crash) and high risk (predicted to have two or more crashes) roadways. It is apparent that significant improvements in risk will occur no matter which scenario plays out by 2050, but the Transformative scenario shows the most improvement by a slight margin compared to the Business-As-Usual and Optimistic scenarios.    

<br>

## References
Al Mamlook, R.E., Kwayu, K.M., Alkasisbeh, M.R., & Frefer, A.A. (2019). Comparison of Machine Learning Algorithms for Predicting Traffic Accident Severity. 2019 IEEE Jordan International Joint Conference on Electrical Engineering and Information Technology (JEEIT), 272-276.

American Association of State Highway and Transportation Officials. Highway Safety Manual, First Edition, 2010.

Bao, J., Liu, P., & Ukkusuri, S. V. (2019). A spatiotemporal deep learning approach for citywide short-term crash risk prediction with multi-source data. Accident Analysis and Prevention, 122 (July 2018), 239–254.

Business Wire (2020, January 9). Auto Insurers Can Now Use Smartphones to Reconstruct Crashes. https://www.businesswire.com/news/home/20200109005096/en/Auto-Insurers-Smartphones-Reconstruct-Crashes

Caliendo, C., Guida, M., & Parisi, A. (2007). A crash-prediction model for multilane roads. Accident Analysis and Prevention, 39(4), 657–670.

Chang, L. Y. (2005). Analysis of freeway accident frequencies: Negative binomial regression versus artificial neural network. Safety Science, 43(8), 541–557.

Chang, L. Y., & Chen, W. C. (2005). Data mining of tree-based models to analyze freeway accident frequency. Journal of Safety Research, 36(4), 365–375.

Chen, Q., Song, X., Yamada, H., & Shibasaki, R. (2016). Learning deep representation from big and heterogeneous data for traffic accident inference. 30th AAAI Conference on Artificial Intelligence, AAAI 2016, 338–344.

Chen, S., Wang, W., & van Zuylen, H. (2009). Construct support vector machine ensemble to detect traffic incident. Expert Systems with Applications, 36(8), 10976–10986.

Chen, T., & Guestrin, C. (2016). XGBoost: A scalable tree boosting system. Proceedings of the ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, 13-17-August-2016, 785–794.

Dong, N., Huang, H., & Zheng, L. (2015). Support vector machine in crash prediction at the level of traffic analysis zones: Assessing the spatial proximity effects. Accident Analysis and Prevention, 82, 192–198.

Federal Highway Administration, Office of Safety (2013). Crash Modification Factors in Practice - Introduction to Safety Performance Functions Report No. FHWA-SA-13-016.

Formosa, N., Quddus, M., Ison, S., Abdel-Aty, M., & Yuan, J. (2020). Predicting real-time traffic conflicts using deep learning. Accident Analysis and Prevention, 136 (January).

Hadi, M. A., Aruldhas, J., Chow, L. F., & Wattleworth, J. A. (1995). Estimating safety effects of cross-section design for various highway types using negative binomial regression. Transportation Research Record, (1500), 169–177.

Huang, T., Wang, S., & Sharma, A. (2020). Highway crash detection and risk estimation using deep learning. Accident Analysis and Prevention, 135(April 2019), 105392.

Johansson, P. (1996). Speed limitation and motorway casualties: A time series count data regression approach. Accident Analysis and Prevention, 28(1), 73–87.

Lee, A. H., Stevenson, M. R., Wang, K., & Yau, K. K. W. (2002). Modeling young driver motor vehicle crashes: Data with extra zeros. Accident Analysis and Prevention, 34(4), 515–521.

Li, X., Lord, D., Zhang, Y., & Xie, Y. (2008). Predicting motor vehicle crashes using Support Vector Machine models. Accident Analysis and Prevention, 40(4), 1611–1618.

Lord, D., & Mannering, F. (2010). The statistical analysis of crash-frequency data: A review and assessment of methodological alternatives. Transportation Research Part A: Policy and Practice, 44(5), 291–305.

Lord, D., Washington, S., & Ivan, J. (2004). Statistical Challenges with Modeling Motor Vehicle Crashes: Understanding the Implications of Alternative Approaches, Center for Transportation Safety, Texas Transportation Institute.

Lv, Y., Tang, S., & Zhao, H. (2009). Real-time highway traffic accident prediction based on the k-nearest neighbor method. 2009 International Conference on Measuring Technology and Mechatronics Automation, ICMTMA 2009, 3, 547–550.

Mannering, F., & Bhat, C. (2014). Analytic methods in accident research: Methodological frontier and future directions. Analytic Methods in Accident Research, 1, 1–22.

Miaou, S. P. (1994). The relationship between truck accidents and geometric design of road sections: Poisson versus negative binomial regressions. Accident Analysis and Prevention, 26(4), 471–482.

Mokoatle, M., Marivate, V., & Esiefarienrhe, M. (2019). Predicting road traffic accident severity using accident report data in South Africa. ACM International Conference Proceeding Series, 11–17.

Parsa, A. B., Movahedi, A., Taghipour, H., Derrible, S., & Mohammadian, A. (Kouros). (2020). Toward safer highways, application of XGBoost and SHAP for real-time accident detection and feature analysis. Accident Analysis and Prevention, 136(December 2019), 105405.

Poch, M., & Mannering, F. (1996). Negative binomial analysis of intersection-accident frequencies. Journal of Transportation Engineering, Vol. 122, pp. 105–113.

Ren, H., Song, Y., Liu, J., Hu, Y., & Lei, J. (2017). A Deep Learning Approach to the Citywide Traffic Accident Risk Prediction. 21st International Conference on Intelligent Transportation Systems (ITSC), 3346–3351.

Schlögl, M., Stütz, R., Laaha, G., & Melcher, M. (2019). A comparison of statistical learning methods for deriving determining factors of accident occurrence from an imbalanced high resolution dataset. Accident Analysis and Prevention, 127(March), 134–149.

Shan, L., Yang, Z., Zhang, H., Shi, R., & Kuang, L. (2019). Predicting duration of traffic accidents based on ensemble learning. Lecture Notes of the Institute for Computer Sciences, Social-Informatics and Telecommunications Engineering, LNICST, 268, 252–266.

Shankar, V. N., Albin, R. B., Milton, J. C., & Mannering, F. L. (1998). Evaluating median crossover likelihoods with clustered accident counts an empirical inquiry using the random effects negative binomial model. Transportation Research Record, (1635), 44–48.

Shankar, V., Milton, J., & Mannering, F. (1997). Modeling accident frequencies as zero-altered probability processes: An empirical inquiry. Accident Analysis and Prevention, 29(6), 829–837.

Singh, S. (2018, March). Critical Reasons for Crashes Investigated in the National Motor Vehicle Crash Causation Survey. (Trafﬁc Safety Facts Crash Stats. Report No. DOT HS 812 506). Washington, DC: National Highway Trafﬁc Safety Administration.

Srinivasan, R., Carter, D., & Bauer, K. (2013). Safety Performance Function Decision Guide: SPF Calibration vs SPF Development Final Report No. FHWA-SA-14-004. Federal Highway Administration Office of Safety.

Wenqi, L., Dongyu, L., & Menghua, Y. (2017). A model of traffic accident prediction based on convolutional neural network. 2017 2nd IEEE International Conference on Intelligent Transportation Engineering, ICITE 2017, 198–202.

Xie, Y., Lord, D., & Zhang, Y. (2007). Predicting motor vehicle collisions using Bayesian neural network models: An empirical analysis. Accident Analysis and Prevention, 39(5), 922–933.

Yu, R., & Abdel-Aty, M. (2013). Utilizing support vector machine in real-time crash risk evaluation. Accident Analysis and Prevention, 51, 252–259.

Yuan, J., Abdel-Aty, M., Gong, Y., & Cai, Q. (2019). Real-Time Crash Risk Prediction using Long Short-Term Memory Recurrent Neural Network. Transportation Research Record, 2673(4), 314–326.

Yuan, Z., Zhou, X., & Yang, T. (2018). Hetero-ConvLSTM: A deep learning approach to traffic accident prediction on heterogeneous spatio-temporal data. Proceedings of the ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, 984–992.
