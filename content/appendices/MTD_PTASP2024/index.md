---
title: "MTD PTASP 2024"
date: 2024-11-14
draft: false
weight: 80
bannerHeading: MTD PTASP 2024
bannerText: >
  Learn more about MTD's Public Transportation Agency Safety Plan (PTASP) and how it relates to the LRTP.

---

The Champaign-Urbana Mass Transit District is required to develop and approve a Public Transportation Agency Safety Plan (PTASP) per 49 CFR Part 673. This Agency Safety Plan (ASP) addresses all applicable requirements and standards as set forth in FTA’s Public Transportation Safety Program and the National Public Transportation Safety Plan.

The CUUATS Long Range Transportation Plan 2050 integrates PTASP in the following ways:
- coordinates with MTD staff to create the LRTP; MTD is a member of the LRTP Steering Committee, [CUUATS Technical Committee](https://ccrpc.org/divisions/planning_and_development/cuuats_technical_committee/index.php), and [CUUATS Policy Committee](https://ccrpc.org/divisions/planning_and_development/cuuats_policy_committee/index.php);
- considers MTD safety objectives and performance measures in the [Goals, Objectives, Performance Measures, and Strategies](https://ccrpc.gitlab.io/lrtp-2050/goals/measuring_progress/) section;
- includes MTD's federally mandated Safety Performance Targets in the [System Performance Report](https://ccrpc.gitlab.io/lrtp-2050/appendices/spr/);
- CUUATS updates its [Federal Performance Measure Report Card](https://ccrpc.org/transportation/federal_performance_measure_report_card.php) annually to reflect MTD's annual revisions to their performance measures outlined in the PTASP.

**<a href="2024_PTASP.pdf">MTD PTASP 2024</a>**