---
title: "System Performance Report"
date: 2024-11-21
draft: false
weight: 40
bannerHeading: System Performance Report
bannerText: >
  An evaluation of system performance with respect to Federal performance targets.

---
## Introduction

The [Moving Ahead for Progress in the 21st Century](https://www.fhwa.dot.gov/map21/legislation.cfm) (MAP-21) enacted in 2012, and the subsequent
[Fixing America's Surface Transportation Act](https://www.fhwa.dot.gov/fastact/legislation.cfm) (FAST Act), enacted in 2015, established a national performance measurement system for the highway and transit programs. In November 2021, President Biden signed the [Infrastructure Investment and Jobs Act (IIJA), also known as the Bipartisan Infrastructure Law (BIL)](https://www.fhwa.dot.gov/bipartisan-infrastructure-law/). The BIL continued the existing performance measures established under MAP21 and FAST Act. The BIL also added a performance measure for greenhouse gases (GHG); however, the reporting requirements for that measure are still being finalized as of November 2024. 

The U.S. Department of Transportation (USDOT) instituted this performance management requirement by establishing performance measures for four categories through rulemakings:
- [Highway Safety](https://www.federalregister.gov/documents/2016/03/15/2016-05202/national-performance-management-measures-highway-safety-improvement-program)  
- [Pavement and Bridge Condition](https://www.federalregister.gov/documents/2017/01/18/2017-00550/national-performance-management-measures-assessing-pavement-condition-for-the-national-highway)
- [System Performance](https://www.federalregister.gov/documents/2017/01/18/2017-00681/national-performance-management-measures-assessing-performance-of-the-national-highway-system)
- [Transit Asset Conditions](https://www.govinfo.gov/content/pkg/FR-2016-07-26/pdf/2016-16883.pdf)

The state departments of transportation (state DOTs) and Metropolitan Planning Organizations (MPOs) are required to establish targets for each highway performance measure, while transit agencies and MPOs set targets for transit asset conditions. MPOs have 180 days after DOTs adopt statewide targets to choose either to set quantitative targets for their metropolitan planning areas or commit to the state’s targets. For the highway measures, at the conclusion of each performance period, USDOT assesses whether “significant progress” has been made toward achieving the highway targets, which is defined differently depending on the measure. If states do not make significant progress, they are required to submit documentation to the Federal Highway Administration (FHWA) on how they will reach the targets; in certain cases, states are also required to program more federal funds toward improving conditions. No penalties are assessed on MPOs or transit agencies. 

Once the targets are established, MPOs are directed to show how the investments in the Transportation Improvement Program (TIP) help achieve the targets. Furthermore, each performance measure’s baseline and targets must be included in a System Performance Report in the MPO’s Metropolitan Transportation Plan (Long Range Transportation Plan) to document the condition and performance of the transportation system with respect to required performance measures.

This System Performance Report starts with an overview of the MPO's process to establish performance measure targets. For each of the federal performance management categories, this report reviews state and MPO baselines and targets and the progress made by the MPO toward achieving the targets. It also includes discussions on how the LRPT 2050 would improve the conditions and performance of the transportation system. This report ends with an overview of the linkage between the system performance measures and infrastructure investment decisions within the Champaign-Urbana Metropolitan Planning Area (MPA). 

## Process

As part of complying with the national performance measurement system established by MAP 21, Illinois Department of Transportation (IDOT), the
Champaign-Urbana MPO ([CUUATS]([CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php))), and the local transit agency, Champaign-Urbana Mass Transit District (MTD), have established a process for data sharing, target setting, and reporting. An Intergovernmental Agreement for Transportation Performance Management was created to comply with 23 CFR 450.314(h) and executed by [CUUATS Policy Committee](https://ccrpc.org/divisions/planning_and_development/cuuats_policy_committee/index.php) in April 2018. This Agreement defines rights and obligations for each agency in terms of cooperatively developing and sharing information related to transportation performance management data and transit asset management data, performance target setting, reporting of performance targets, and tracking progress toward attainment of critical outcomes for the MPO region.

For the Champaign-Urbana MPA, targets for the three sets of National Performance Management Measures and the Transit Asset Management Performance Measures were
adopted by CUUATS Policy Committee upon discussions at a series of CUUATS Technical and Policy Committee meetings, as noted in the “Past and Upcoming Target-Setting Timelines for the Performance Measures” section of the [Federal Performance Measures Report Card](https://www.ccrpc.org/transportation/federal_performance_measure_report_card.php). 

## Highway Safety (PM1)

The safety performance measures require state DOTs and MPOs to establish safety targets as five-year rolling averages on all public roads for:

1. Number of fatalities

2. Rate of fatalities per 100 million vehicle miles traveled (VMT)

3. Number of serious injuries

4. Rate of serious injuries per 100 million VMT

5. Number of non-motorized fatalities and non-motorized serious injuries

### State Targets

Injuries and fatalities from traffic crashes vary considerably from year to year due to numerous factors, and the five-year average is meant to smooth large
changes. IDOT must adopt targets for each safety measure by August 31 on an annual basis. MPOs must establish targets within 180 days after IDOT. MPOs can
either choose to adopt the state’s targets (Table 1) or set their own quantitative targets.

<rpc-table url="pm1_Illinois.csv"
table-title="Table 1: PM1, Illinois State Baseline and Targets"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
text-alignment="c,c"></rpc-table>

### MPO Targets and Performance

CUUATS Policy Committee approved the PM1 targets for 2023 based on goals and targets set for the LRTP 2045, approved in December 2019. The MPO targets did not differ significantly from the state’s targets but are slightly more ambitious than the state’s targets and are consistent with the LRTP. For 2024 targets, however, the Policy Committee decided to go with state targets as they were slightly more ambitious. 2025 targets are pending approval and will be presented in the December 2024 CUUATS Policy Committee meeting. Table 2 presents the PM1 baseline, 2023-2025 targets and 2023 performance assessment for the Champaign-Urbana MPA. 

<rpc-table url="pm1_CU.csv"
table-title="Table 2: PM1, Champaign-Urbana MPA Baseline, Targets, and Performance"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
text-alignment="c,c"></rpc-table>

Champaign-Urbana MPA 2019-2023 five-year rolling average crash data shows that two performance measures, the number and rate of serious injuries, met the 2023 target. Out of the three performance measures that performed worse than the targets set for 2023, two performance measures were maintained or improved from the 2017 baseline (Figures 1-5). 

<rpc-chart
url="figure-1-fatalmpa.csv"
type="line"
y-label="Number of Fatalities"
y-min="5"
y-max="10"
grid-lines="true"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Figure 1: PM1 – Number of fatalities, Champaign-Urbana MPA Baseline, Targets, and Performance">
</rpc-chart>

<rpc-chart
url="figure-2-pm1-rate-of-fatalities-per-100-million-vmt.csv"
type="line"
y-label="Number of Fatalities per VMT"
grid-lines="true"
y-min="0.5"
y-max="1"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Figure 2: PM1 – Rate of fatalities per 100 million Vehicle Miles Traveled (VMT), Champaign-Urbana MPA Baseline, Targets, and Performance">
</rpc-chart>

<rpc-chart
url="figure-3-pm1-number-of-serious-injuries-cu.csv"
type="line"
y-label="Number of Serious Injuries"
grid-lines="true"
y-min="80"
y-max="200"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Figure 3: PM1 – Number of serious injuries, Champaign-Urbana MPA Baseline, Targets, and Performance">
</rpc-chart>

<rpc-chart
url="figure-4-pm1-rate-of-serious-injuries-per-100-million-vmt-cu.csv"
type="line"
y-label="Number of Serious Injuries per VMT"
grid-lines="true"
y-min="5"
y-max="15"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Figure 4: PM1 – Rate of serious injuries per 100 million Vehicle Miles Traveled (VMT), Champaign-Urbana MPA Baseline, Targets, and Performance">
</rpc-chart>

<rpc-chart
url="figure-5-pm1-number-of-non-motorized-fatalities-and-non-motorized-serious-injuries-cu.csv"
type="line"
y-label="Number of Fatalities/Serious Injuries per VMT"
grid-lines="true"
y-min="16"
y-max="30"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
chart-title="Figure 5: PM1 – Number of non-motorized fatalities and non-motorized serious injuries, Champaign-Urbana MPA Baseline, Targets, and Performance">
</rpc-chart>

### Moving Toward the Targets

Since the approval of the LRTP 2045 in December 2019, [CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php), working with the member agencies, has taken a series of actions to implement various safety recommendations to meet the plan’s goals. Below is a list of major studies, plans, and grant applications [CUUATS](https://ccrpc.org/programs/transportation/) carried out to move the Champaign-Urbana MPA toward the LRTP 2045 goals as well as the PM1 safety targets. Note this is not a comprehensive list. See [CUUATS UPWPs](https://www.ccrpc.org/transportation/unified_planning_work_program_(upwp).php) for additional information.

1.	Assessed eight Safety and Security Performance Measures through annual LRTP 2045 [report card update](https://data.ccrpc.org/pages/lrtp-2045-report-card) (2020-2024).
2.	Assessed Federal Performance Measures, as updated annually on the [Federal Report Card](https://ccrpc.org/transportation/federal_performance_measure_report_card.php).
3.	Completed annual updates to the Champaign-Urbana Urban Area [Sidewalk Inventory and Assessment](https://www.ccrpc.org/transportation/sidewalk_network_inventory_and_assessment.php), which provides a database on sidewalk and curb ramp condition and ADA compliance for CUUATS member agencies. 
4.	Continued to facilitate the [Illinois Modeling Users Group (ILMUG)](https://www.ccrpc.org/transportation/illinois_modeling_users_group.php) with quarterly meetings, modeling training and assistance building models for other Illinois MPOs. 
5.	Continued to maintain the [Champaign County Traffic Crash Dashboard](https://crashdashboard.ccrpc.org/). 
6.	Continued to conduct annual traffic counts in the University of Illinois campus area for Multimodal Corridor Enhancement (MCORE) projects.
7.	Continued to update [Safe Routes to Schools (SRTS) Maps](https://ccrpc.org/transportation/safe_walking_route_maps/index.php) for public elementary and middle schools in Champaign-Urbana.
8.	Reviewed, scored and provided comments on all 2020, 2022, and 2024 ITEP applications submitted for funding within the Champaign-Urbana Metropolitan Planning Area.
9.	Prepared and submitted an IDOT State Planning Research program grant application in 2020 to develop a Land Use Inventory for use in transportation modeling. Requested funding was granted and the software application and inventory was developed (2022). 
10.	Prepared and submitted an IDOT State Planning Research program grant application in 2020 to implement certain aspects of the Urban and Rural Safety Plans completed in 2019. Requested funding was granted and activities were completed in 2022. 
11.	Prepared and submitted an IDOT Office of Intermodal Project Implementation grant application in 2020 to develop a Census Data Extraction tool and transportation equity report (Advancing Transportation Equity project). Requested funding was granted and the software application was developed. The report is nearing completion as of November 2024.
12.	Prepared and submitted an IDOT Office of Intermodal Project Implementation grant application in 2020 to analyze fare free transit and job access in the Village of Rantoul. Requested funding was granted and the report is nearing completion as of November 2024.
13.	Prepared and submitted an IDOT Office of Intermodal Project Implementation grant application in 2020 to develop a Sustainable Neighborhoods Bus Route Evaluation for the Champaign-Urbana Mass Transit District. Requested funding was granted and the project is nearing completion as of November 2024.
14.	Prepared and submitted an HSIP grant application for safety improvements on the Philo Slab between US 45 South and IL 130 on behalf of Champaign County (2020). Requested HSIP funding was granted and improvements were implemented in 2024.
15.	Prepared and submitted an IDOT State Planning Research program grant application in 2021 to develop a Systemic Safety Evaluation Tool (SSET). Requested funding was granted and the software application was developed (2022). SSET is a web application that allows state, county, and municipal engineers to evaluate systemic crash risks, explore potential countermeasures, and estimate the benefits and costs of safety improvement projects, using the Champaign County Highway Department roadway network as a demonstration project.
16.	Prepared and submitted an IDOT State Planning Research program grant application in 2021 to complete Phase 2 of the Regional Environmental Framework (REF). Requested funding was granted and the report from Phase 1 was converted into an interactive web application in 2022.
17.	Prepared and submitted an IDOT State Planning Research program grant application in 2022 to develop the CUUATS Modeling Suite. The project began in 2023 and is still underway. The project is upgrading model data management, automating communications between models, and updating/expanding the modeling suite. 
18.	Prepared and submitted an IDOT State Planning Research program grant application in 2022 to complete the [Lincoln Avenue Corridor Study](https://ccrpc.gitlab.io/lincoln-ave/) between Florida Avenue and Green Street. Requested funding was granted and the project is nearing completion as of November 2024.
19.	Prepared and submitted an IDOT State Planning Research program grant application in 2023 to develop a Housing and Transportation Affordability and Accessibility Index. Requested funding was granted and the project began in early 2024. 
20.	Prepared and submitted an application for the federal Safe Streets for All (SS4A) grant program. Requested funding was granted in 2023, and the project started in late 2024. The project includes updating the Champaign County Rural and Urban Safety Plans, and implementing a demonstration project on North Lincoln Avenue in Urbana.
21.	Prepared and submitted an application to the federal Promoting Resilient Operations for Transformative, Efficient, and Cost-Saving Transportation (PROTECT) program in 2023 to complete a Champaign County Regional Transportation System Vulnerability Assessment Under Climate Change analysis and report. Requested funding was granted and the project will begin in January 2025.
22.	Developed the [Urbana Pedestrian Master Plan](https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/2020_urbana_pedestrian_master_plan_(final_report).php) (2020).
23.	Developed [Urbana Bicycle Wayfinding Plan](https://ccrpc.org/transportation/urbana_bicycle_and_pedestrian_plans/urbana_bicycle_wayfinding_master_plan_final_report_2020.php) (2020).
24.	Completed the Safety Forecasting Tool in 2021, which was used to analyze crash risk for the LRTP 2050. 
25.	Completed a speed study on Airport Road in Savoy (2021).
26.	Made recommendations for pedestrian crossing treatments at Lake of the Woods Road and Tin Cup Road in Mahomet (2021).
27.	Completed the [Florida Avenue Corridor Study](https://ccrpc.gitlab.io/florida-ave/), which includes safety analysis and recommendations for safety improvements, mobility improvements, and multimodal connectivity improvements (2022).
28.	Updated the CUUATS [Project Priority Review Guidelines](https://ccrpc.org/transportation/project_priority_review_ppr_guidelines.php) for prioritizing distribution of STPU/STBGP funding (2022).
29.	Conducted a traffic study for Parkland Way between Parkland College and Mattis Avenue (2023).
30.	Reviewed and provided comments on the Safe Routes to School (SRTS) non-infrastructure grant application for the C-U SRTS Project (2023).
31.	Updated the CUUATS [Complete Streets Policy](https://ccrpc.gitlab.io/complete-streets-policy/) (2023), superseding the 2012 Policy.
32.	Updated the CUUATS [Public Participation Plan (PPP)](https://ccrpc.org/transportation/public_participation_plan_(ppp)/2023_public_participation_plan_ppp.php) in 2023.
33.	Updated the CUUATS [Title VI Report](https://ccrpc.gitlab.io/title-vi-2024/) in 2024.
34.	Prepared and submitted a Bicycle Friendly Business (BFB) application for CCRPC to the League of American Bicyclists (LAB) in 2015 and again in 2023. CCRPC was designated a Silver Level Bicycle Friendly Business (BFB) by LAB (2015) and achieved Gold Level BFB in 2023.
35. Identified bicycle and pedestrian facility projects in the Champaign-Urbana MPA that have received state and/or federal funding over the last 15 years, and sent photos of these locations to IDOT at their request (2024).

In the current LRTP 2050, a more detailed list of five-year objectives, additional localized safety performance measures, as well as specific strategies
with responsible parties identified, have been proposed under the [Safety Goal](https://ccrpc.gitlab.io/lrtp-2050/goals/safety/) to continue the efforts of moving the Champaign-Urbana MPA toward the PM1 targets.

The Scenario Modeling section of the [LRTP 2050](https://ccrpc.gitlab.io/lrtp-2050/vision/model/) details how the 2050 preferred scenario, in comparison with the 2050 Business-As-Usual scenario, is projected to result in fewer Vehicle Miles Traveled (VMT), decreased share of automobile travel, and increased pedestrian and bicycle access scores, all of which will likely lead to a positive impact on achieving the PM1 targets.

## Pavement and Bridge Conditions (PM2)

The infrastructure condition performance measures require state DOTs and MPOs to establish targets for:

1. Percentage of pavements of the Interstate System in Good condition
2. Percentage of pavements of the Interstate System in Poor condition
3. Percentage of pavements of the non-Interstate National Highway System (NHS) in Good condition
4. Percentage of pavements of the non-Interstate NHS in Poor condition
5. Percentage of NHS bridges classified as in Good condition
6. Percentage of NHS bridges classified as in Poor condition

### State Targets

IDOT established PM2 targets for 2024 and 2026 in November 2022 (Table 3). On September 30, 2024, IDOT reviewed the 2024 performance (data year 2023) and adjusted the targets for 2026 (Data year 2025). IDOT uses the Condition Rating Survey (CRS) method for rating pavement condition in Illinois and assigns pavement condition ratings based on pavement distress, such as International Roughness Index (IRI), rutting, cracking, and deterioration. IDOT performs bi-yearly safety inspections and condition assessments of bridges. This is the frequency designated by the National Bridge Inspection Standards (NBIS). Through these inspections, condition rating data is collected for the deck, superstructure, and substructure, and an overall rating of Good, Fair, or Poor condition is assigned to each bridge metric per calendar year.

<rpc-table url="pm2_Illinois.csv"
table-title="Table 3: PM2, Illinois State Baseline and Targets"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
text-alignment="c,c"></rpc-table>

### MPO Targets and Performance  

The National Highway System (NHS) within the MPA is the focus of PM2. The NHS is a federal designation for roadways considered important to the nation’s economy. In the Champaign-Urbana MPA, this includes all the interstates and some non-interstate roadways, such as state routes and several local roadways. The NHS in the MPA has changed over time. Figure 6 shows MPA NHS roadway and bridge designations in 2023.

#### Figure 6: NHS Roads and Bridges in the Champaign-Urbana MPA, 2023
{{<image src="Fig-6_NHSroads-bridges2023.png"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  alt="Map of the Champaign-Urbana MPA showing NHS roads and bridges in the area and their jurisdiction. Most bridges are IDOT's responsibility, with a few being Champaign and Urbana's responsibility."
  attr="IDOT"
  position="full">}}

After analyzing the MPA's pavement and bridge baseline condition data, the CUUATS Policy Committee adopted the State’s PM2 targets for the Champaign-Urbana MPA. CUUATS receives pavement and bridge condition data from IDOT. Table 4 presents the PM2 2022 baseline, 2024 and 2026 targets, and 2024 performance assessment (data year 2023) for the Champaign-Urbana MPA. 

<rpc-table url="pm2_CU.csv"
table-title="Table 4: PM2, Champaign-Urbana MPA Baseline, Targets, and Performance"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
text-alignment="c,c"></rpc-table>

Champaign-Urbana MPA PM2 2024 data shows three performance measures performed better than the 2022 baseline and three performed worse than the baseline. All three performance measures that performed worse than the 2022 baseline performed worse than the 2024 target. 

### Moving Toward the Targets

Figure 7 shows 2023 NHS roadway and bridge conditions in the Champaign-Urbana MPA and scheduled improvement projects approved in [TIP FY 2023-2028](https://ccrpc.org/transportation/transportation_improvement_plan_(tip)/transportation_improvement_program_(tip)_fy_2023-2028.php), as of November 20, 2024.

#### Figure 7: NHS Pavement and Bridge Conditions (2023) and Scheduled Improvements (FY 2023-2028)
{{<image src="Fig-7_NHSpavement-bridge-condition2023.png"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CCRPC and IDOT"
  alt="Map of the Champaign-Urbana MPA showing the condition of NHS pavement and bridges, and which pavement and bridges are slated for improvements in the FY 2023-2028 Transportation Improvement Program (TIP)."
  position="full">}}

The [Scenario Modeling](https://ccrpc.gitlab.io/lrtp2045/vision/model/) section of the LRTP 2050 details how the 2050 preferred "Optimistic" scenario will help achieve the PM2 targets by focusing on maintenance of the existing transportation system moreso than construction of new roadways, and also by planning for fewer vehicles on area roadways. 

## System Performance (PM3)

The system performance measures require state DOTs and MPOs to establish targets for:

1. Percent of reliable person-miles traveled on the Interstate
2. Percent of reliable person-miles traveled on the non-Interstate NHS
3. Percentage of Interstate system mileage providing for reliable truck travel time - Truck Travel Time Reliability Index

For MPOs in non-attainment or maintenance status, there are three additional performance measures on traffic congestion assessment that are not applicable
for the Champaign-Urbana MPA.

### State Targets

The PM3 measures require the use of the National Performance Management Research Data Set (NPMRDS). IDOT has procured the Regional Integrated Transportation Information System (RITIS) to analyze the NPMDRS and provided access to RITIS for the MPOs within the state to use. IDOT established PM3 targets for 2024 and 2026 in November 2022. On September 30, 2024, IDOT reviewed the 2024 performance assessment (data year 2023) and adjusted the targets for 2026 (Data year 2025). Table 5 shows the PM3 Illinois state baseline and targets.  

<rpc-table url="pm3_Illinois.csv"
table-title="Table 5: PM3, Illinois State Baseline and Targets"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
text-alignment="c,c"></rpc-table>

### MPO Targets

CUUATS adopted the State’s PM3 targets. Table 6 presents the PM3 2022 baseline, 2024 and 2026 targets, and 2024 performance assessment (data year 2023) using NPMRDS INRIX data for the Champaign-Urbana MPA. 

<rpc-table url="pm3_CU.csv"
table-title="Table 6: PM3, Champaign-Urbana MPA Baseline, Targets, and Performance"
source=" IDOT Roadway Crash Data"
source-url="http://www.idot.illinois.gov/transportation-system/safety/Illinois-Roadway-Crash-Data"
text-alignment="c,c"></rpc-table>

Champaign-Urbana MPA PM3 2023 data show that all PM3 performance measures achieved their 2024 targets, and two of them improved from the 2022 baseline. 

### Moving Toward the Targets

The Champaign-Urbana MPA saw numerous infrastructure projects come to fruition since the last LRTP was approved in 2019 that have made travel more reliable. Examples of this include bridge deck repairs and replacement (I-74 bridges), roadway resurfacing and shoulder improvements (Philo Slab between US45 and IL130), roadway reconstruction (Prospect Avenue between Curtis Road and Windsor Road), corridor studies (Florida Avenue, Lincoln Avenue), and commencement of construction on the long-awaited I-57 interchange at I-74. 

CUUATS also provided data and content for a Rebuilding American Infrastructure with Sustainability and Equity (RAISE) grant application on behalf of the Village of Savoy for the Curtis Road Grade Separation and Complete Streets Project. The Village received a $22 million RAISE grant in 2023.

## Transit Asset Management (TAM)

Transit Asset Management (TAM) is applicable to providers who are recipients or sub-recipients of Federal financial assistance under 49 U.S.C. Chapter 53. TAM is an effort to keep assets and equipment in transit systems in a state of good repair so the systems contribute to the safety of the system as a whole. This supports the idea that a vehicle in good repair will minimize risk and maximize safety. TAM measures performance for the following asset categories:

1. Equipment  
2. Facilities  
3. Infrastructure  
4. Rolling Stock

The TAM Final Rule requires recipients to set one or more performance targets per asset class based on State of Good Repair measures. The Final Rule also requires transit providers to coordinate with MPOs, to the maximum extent practicable, in the selection of MPO performance targets. The coordination amongst transit providers and MPOs should influence transportation funding investment decisions and is intended to increase the likelihood that transit state of good repair needs are programmed, committed to, and funded as part of the planning process.

### MTD Targets and performance

The following tables show the C-U MTD’s TAM targets and performance by categories.  

<rpc-table url="Table-7_TAM-Facilities-MTD.csv"
table-title="Table 7: TAM Facilities, MTD Targets and Performance"
  text-alignment="c,c"></rpc-table>

<rpc-table url="Table-8_TAM-RollingStock-RevenueVehicles-MTD.csv"
table-title="Table 8: TAM Rolling Stock (Revenue Vehicles), MTD Targets and Performance"
  text-alignment="c,c"></rpc-table>

<rpc-table url="Table-9_TAM-Equipment-Service Vehicles-MTD.csv"
table-title="Table 9: TAM Equipment (Service Vehicles), MTD Targets and Performance"
  text-alignment="c,c"></rpc-table>

### Moving Toward the targets

MTD received an FTA Bus & Bus Facilities grant to upgrade and expand Illinois Terminal in FY21; planning is still underway as of late 2024. MTD completed construction of a Vehicle Storage and Maintenance Facility, expansion of its Hydrogen Fueling Station, development of a downtown Urbana off-street transit facility, and upgrading existing facilities. 

MTD's fleet achieved 100% low and no emissions in 2023 when it retired its last four diesel buses. An expanded hydrogen fueling station would allow MTD to support up to 60 additional zero-emission hydrogen buses to replace the existing fleet as the buses reach the end of their useful life.

## Transit Safety

As of April 2024, the FTA has established [14 performance measures](https://www.federalregister.gov/documents/2024/04/11/2024-07514/public-transportation-agency-safety-plans#h-30) in the National Public Transportation Safety Plan. Safety performance measures are calculated as a five-year performance average. MTD establishes safety performance targets as a 5% improvement from the previous year’s safety performance measure.

Safety performance measures and targets are calculated for each mode category:

*	Directly operated Motor Bus/Fixed Route (MB-DO)
*	Directly operated Demand Response/Paratransit (DR-DO)
*	Purchased transit Demand Response/Paratransit (DR-PT)

On an annual basis, MTD will update the Safety Performance Targets based on performance measures from the previous five years and submit transit safety performance targets to CUUATS and IDOT, as shown in the following tables.

<rpc-table url="Table-10_2023-MTD-SafetyPMs.csv"
table-title="Table 10: 2023 MTD Transit Safety Performance Measures"
  text-alignment="c,c"></rpc-table>

<rpc-table url="Table-11_2024-MTD-SafetyTargets.csv"
table-title="Table 11: 2024 MTD Transit Safety Performance Targets"
  text-alignment="c,c"></rpc-table>

## Linking Performance with Investment

### Transportation Improvement Program (TIP)

CUUATS recognizes the importance of linking investment priorities to stated performance objectives, and that establishing this link is critical to the achievement of national transportation goals and statewide and regional performance targets. As such, the [FY 2023-2028 TIP](https://ccrpc.org/transportation/transportation_improvement_plan_(tip)/transportation_improvement_program_(tip)_fy_2023-2028.php) documents the established targets associated with each of the performance measures as well as the local projects utilizing federal funding that address the targets.

The [FY 2023-2028 TIP Database and Map](https://maps.ccrpc.org/tip-2023-2028/) includes a column identifying which performance measures are being addressed. *Note: If the Performance Measure column is not visible, click on "Column Visibility" to add that column to the table.*

### Project Priority Review (PPR)

The CUUATS Policy Committee approved the [Project Priority Review (PPR) Guidelines](https://ccrpc.org/transportation/project_priority_review_ppr_guidelines.php) in 2022, outlining the process by which CUUATS evaluates and documents consistency between the local use of federal Surface Transportation Block Group Program (STBGP), formerly Surface Transportation - Urban (STPU), funds and federal and regional transportation goals. Local agencies seeking to use STBGP funds are required to submit a project application for review by CUUATS staff and the Project Priority Review working group, who use a set of criteria based on federal and regional transportation goals to score each project.

In 2023, [CUUATS Policy Committee](https://ccrpc.org/divisions/planning_and_development/cuuats_policy_committee/index.php) approved allocating funding for four projects in the MPA in fiscal years 2024 through 2026 based on PPR working group and CUUATS staff recommendations:

* Florida Avenue from Wright Street to Hillcrest Drive: Corridor Improvements
* North Lincoln Avenue from Wascher Drive to Killarney Drive: Complete Streets Improvements 
* Mattis Avenue from Curtis Road to Windsor Road: Complete Streets Improvements
* Neil Street from East Washington Street to Edgebrook Drive: Corridor Improvements

Upon approval of the LRTP 2050, the PPR Guidelines will be updated to reflect the federal performance measures as well as the LRTP 2050 goals.

## Conclusion

The emphasis on setting and achieving performance targets represents a major change in the federal transportation program ushered in by MAP-21 and the FAST Act. The required MPO targets are a chance for the region to continue connecting short-term performance measurement to longer-term regional priorities. The MPO target-setting requirements also give the region another avenue to call attention to the large investment and funding needs for different elements of the transportation system. CUUATS will continue tracking and updating these measures as needed. 
