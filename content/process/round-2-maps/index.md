---
title: "Survey: Future Projects Maps"
date: 2024-03-26
draft: true
weight: 10
---

## Proposed Future Projects Survey Maps
In order to complete the survey, please refer to the corresponding maps and project numbers to answer Questions 2-4. Each project number also includes a brief description of the proposed improvements. More details on these project types can be found at the bottom of this page.

<br>
<hr style="height: 5px; background-color: #3498db;">
<br>


## Questions 2 and 3 -  Proposed Future Street Projects Map
 
#### Click on the map below to open the image in a new window.
  <a target="_blank" href="Large Map_Street.png">
                  <img alt="" src="Large Map_Street.png"/>
             </a>



<br>
<hr style="height: 5px; background-color: #3498db;">
<br>



## Question 4 - Proposed Future Bike and Pedestrian Projects Map 

#### Click on the map below to open the image in a new window.

  <a target="_blank" href="Large Map Bike and Ped.png">
                  <img alt="" src="Large Map Bike and Ped.png"/>
             </a>

<br>
<hr style="height: 5px; background-color: #3498db;">
<br>

## Transportation Project Types
Below are descriptions and examples of transportation project types to help with the survey process. Please note that these project examples have been completed and are not the projects included on the maps.

**BIKE LANE** – The portion of a roadway surface that is designated by
pavement markings and signing for the exclusive use of bicyclists.
(IDOT Local Road and Streets Manual)

{{<image src="bike_lane.PNG"
  width="75%" 
     height="75%"
  position="center">}}

**SHARED-USE PATH** – A facility physically separated from the roadway and intended for bicycle or other non-motorized transportation (e.g. pedestrians, disabled persons in wheelchairs, in-line skaters).
(IDOT Local Road and Streets Manual)

{{<image src="shared_use.PNG"
  width="75%" 
     height="75%"
  position="center">}}

**COMPLETE STREET**– Complete Streets are streets designed and operated to enable safe use and support mobility for all users. Those include people of all ages and abilities, regardless of whether they are travelling as drivers, pedestrians, bicyclists, or public transportation riders.
(USDOT 2015)

{{<image src="complete_streets.PNG"
  width="75%" 
     height="75%"
  position="center">}}

**ROAD DIET**– A Road Diet typically involves converting an existing four-lane undivided roadway to a three-lane roadway consisting of two through lanes and a center two-way left-turn lane (FHWA)

{{<image src="road_diet.PNG"
  width="75%" 
     height="75%"
  position="center">}}

**BRIDGE REPLACEMENT**– New local bridges crossing interstates often are designed to include sidewalks and bike lanes in addition to the motorized travel lanes. As separate but related projects, cities extend bicycle and pedestrian facilities approaching the new bridge.

{{<image src="bridge.PNG"
  width="75%" 
     height="75%"
  position="center">}}

**ROUNDABOUT** – An intersection with a circular configuration that safely and efficiently moves traffic. Roundabouts feature channelized, curved approaches that reduce vehicle speed, entry yield control that gives right-of-way to circulating traffic, and counterclockwise flow around a central island that minimizes conflict points. (FHWA)

{{<image src="roundabout.PNG"
  width="75%" 
     height="75%"
  position="center">}}