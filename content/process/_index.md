---
title: Public Involvement
date: 2023
draft: false
menu: main
weight: 50
bannerHeading: Public Involvement
bannerText: >
  Public input is integral to the development of the LRTP because it affects
  every resident, employee, and visitor in our community.
---
{{<image src="20230603_094934.jpg"
  alt="Two survey participants with their small plant gifts in front of the CUUATS outreach table"
  caption="Urbana Farmers Market, 2023"
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

<h2> Overview </h2>

Public involvement was an integral part of the LRTP 2050 planning process. The LRTP vision can affect every resident as a transportation user and every visitor to our community. Collecting input from the public regarding their current experiences and future recommendations for the region’s transportation network helps produce a plan that is grounded in a shared, collective vision. All participation in the planning process is highly encouraged and very much appreciated.
  
The LRTP 2050 planning process provided multiple opportunities for public involvement during the plan’s development. CUUATS staff advertised these opportunities using CUUATS social media and the Champaign County Regional Planning Commission (CCRPC) website. Public outreach strategies included the C-U Transportation Voices online interactive mapping tool, the Champaign-Urbana Transportation Survey 2023, outreach tables at popular community events, a social media presence, and repurposing informational YouTube videos from the LRTP 2045 planning process. These strategies contributed to educating the public about the long-range transportation planning process, raising awareness of existing transportation services, and provided opportunities for the public to inform the direction of planning efforts.

CUUATS staff designed and scheduled the LRTP 2050 public involvement strategies and events to capture a representative sample of the population in the metropolitan planning area, with a special emphasis on engaging historically underrepresented populations in accordance with Title VI of the 1964 Civil Right Act. 

<h3> Social Media Strategy </h3>

CUUATS staff utilized organizational accounts on Facebook and Instagram to encourage people to attend upcoming community events where staff would be present to provide information, utilize the online map and survey, and to review the previous LRTP 2045. The Champaign County Regional Planning Commission (CCRPC) reposted CUUATS content on the main CCRPC Facebook accounts as they were able to reach additional audiences. Staff keep these accounts active and engaging to increase traffic. 

CUUATS staff utilized informational YouTube videos posted on the organizational account that provided explanations of the LRTP planning process in English, Spanish, and Mandarin Chinese. These videos were produced as part of the LRTP 2045 planning process. 

<h3> Partners </h3>
Events attended were hosted by the Urbana Park District, Champaign Park District, City of Urbana, City of Champaign, Champaign County Forest Preserve District, The Land Connection, Village of Savoy, Village of Mahomet, MTD, University of Illinois, and the Christie Clinic Illinois Marathon. 