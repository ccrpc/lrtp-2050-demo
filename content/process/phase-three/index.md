---
title: "Phase III Public Outreach"
date: 2024-12-11
draft: false
weight: 60
bannerHeading: Phase III Public Outreach
bannerText: >
    30-day public comment period and local agency presentations.

---

## Phase Three Public Outreach

The third and last phase of LRTP 2050 public involvement was a 30-day review period for the Draft LRTP 2050 from Monday, September 16 to Tuesday, October 15, 2024. This round did not include a survey. Instead, it offered the public opportunities to provide verbal and/or written open-ended comments pertaining to the LRTP 2050.  In addition to promoting this website for review, printed copies of the website were available at the following locations:

* Champaign Public Library  
* Urbana Free Library  
* Illinois Terminal in Champaign  
* Savoy Municipal Center  
* Tolono Public Library  
* Mahomet Administration Building  
* Champaign County Regional Planning Commission in Urbana

<br>

CUUATS staff also presented the Draft LRTP 2050 to the following local 
agencies for review and feedback:

* October 8: City of Champaign Study Session, 7:00pm
* October 11: Champaign County Highway Committee, 9:00am
* October 15: Champaign City Council, 7:00pm
* October 17: City of Urbana Plan Commission, 7:00pm
* October 22: Village of Mahomet Board, 7:00pm
* October 24: Campus Transportation Advisory Committee, 9:30am
* October 30: MTD Board, 3:00pm
* November 12: Urbana City Council, 7:00pm

### Round 3 - Public Comments Received
The following section summarizes the comments received from this final phase of public involvement.

**City of Champaign - City Council Meeting**
* City Council Members voiced the importance of planning for climate change, connectivity, and accessibility in underserved areas. There was support for physically protected bicycle infrastructure, such as separated bike lanes, and one member shared a personal experience of being hit by a car while biking. Another member encouraged consideration for accessibility for the elderly population and individuals with disabilities.  
* A member of the audience spoke on behalf of the Greater Community AIDS Project of East Central Illinois. They shared that clients rely on paratransit, but there has been decreased reliability and accessibility with the service. This has contributed to isolation, stress and depression amongst clients. Specifically, the MTD text system is inconsistent, as sometimes not showing up. They offered the following solutions: increase frequency, improve staff training, and ensure a reliable communication system. The speaker also noted that they did not see adequate reference to accessibility in the LRTP 2050.
* Other comments voiced support for heath, equity, and sustainability, specifically continuing to commit to green transit infrastructure at MTD and minimizing the community’s carbon footprint. There was concern for driver and pedestrian safety on and around campus when maneuvering traffic. Finally, others expressed an appreciation for the transit system and connectivity in the community.

**Public Comment**
* Requested that crashes and fatalities/injuries be reported per 100,000 so that the number is easily comparable to national statistics  . 
* Requested that data presented dates back 10 years to view an extended trend.
* Noted that the use of VMT is biased.
* Asked if crash rate per VMT included pedestrians.
* Requested that problems surrounding cars be more specified, noting fatalities and air pollution a significantly caused by vehicles over other alternatives.
* Wanted to see more about bike share use as these programs have grown in the region. Specifically, the use of Veo bikes shows tens of thousands of trips taken per month.
* Requested the inclusion of potential connection of the KRT to the Norfolk Southern trail going from Bloomington to Mansfield. The regional significance of an extensive rail trail network in Champaign County may become a reality as ebike use increases as well.
* Confusion around the highest priority project only being a portion of the Kickapoo Rail Trail (KRT). (As a note, this is part of the public feedback on high-priority projects, rated by the public. These ratings are not reflective of thoughts by local practitioners nor public officials).
* Noted that bike projects in the LRTP underestimate the capability of bikers to know what paths are safest for them. The comment requested more projects along less busy streets and projects that make crossing major thoroughfares safer as these are easier routes for many cyclists. They are also worried that projects along major north/south routes are not any safer for cyclists when completed. An emphasis for making the Prospect/I-74 bridge safer for all users was also requested.
* A comment addressing the IDOT East University Avenue resurfacing project from Maple to I-74 called for designing the roadway for reduced speeds to ensure the safety and accessibility of all road users. Additionally, this comment expressed the support from BPAC (the City of Urbana Bicycle and Pedestrian Committee) regarding the recommendations for improvements to East University Ave made by both Urbana Public Works and the MTD. BPAC added the following two items to these recommendations: a pedestrian crossing at Maple Avenue, and on-street bike lanes. 


### Public Outreach Materials
Here you will find the materials used in Round 3 of public outreach.

<a href="Social Media Post.png">LRTP 2050 - Round 3 - Social Media Post</a>

<a href="public_ad.png">LRTP 2050 - Round 3 - Newspaper Ad</a>

<a href="Press_Release.pdf">LRTP 2050 - Round 3 - Media Release</a>

<br>

*Materials distributed with the draft plan at public comment locations*

<a href="LRTP 2050 Infographic.png">LRTP 2050 Infographic Summary</a>

<a href="LRTP 2050 public instructions.pdf">LRTP 2050 - Round 3 - Public Comment Instructions</a>

<a href="Comment Card.pdf">LRTP 2050 - Round 3 - Public Comment Cards</a>

<a href="Note_to_hosts.png">LRTP 2050 - Round 3 - Note to Public Comment Location Hosts</a>

### Changes made to Draft LRTP 2050 based on comments received during Round 3

1.	Updated LRTP 2050 home page with newer process information

2.	Under Overview, updated Updates page with newer process information

3.	Under Overview, added Acknowledgements page

4.	Under Existing Conditions > Land Use, in the 2022 Land Use Map, recolored the maps to align with the Land-Based Classification Standards color scheme (from Dan Saphiere, 9/4/24) 

5.	Under Existing Conditions > Transportation, added the 2022 Illinois Crash Rate to the Crash Rate chart; this was previously listed as zero due to lack of data. 

6.	Under Existing Conditions > Transportation, under Fatal Traffic Crashes, clarified explanation on how crashes are counted (based on comment left at the Tolono library: qualify how fatalities were counted in the fatalities maps in Existing Conditions. For example, the pedestrian fatalities map shows those crashes where a pedestrian died, but the crash may have involved an automobile).

7.	Under Existing Conditions > Transportation > Bicycles, added Veo (e-bike) ridership data above Bicycle Safety section.

8.	Under Goals > Connectivity, added MTD ridership objective and performance measure

9.	Under 2050 Vision > Future Projects, incorporated local plans by reference
 
10.	Under 2050 Vision > Future Projects: Regionally Significant Vision Projects, in the LRTP 2050 Vision graphic, changed the year construction is expected for “Complete Street: Duncan Road from Kirby Ave to Springfield Ave” from “construction expected in 2026” to “construction expected after 2030” 
 
11.	Under 2050 Vision > Future Projects > Project Profiles: Regionally Significant Projects, remove cities as potential lead agencies for the Kickapoo Rail Trail from Urbana to Mahomet.

12.	Under 2050 Vision > Future Projects: Fiscally Constrained, CUUATS TIP Summary Table, added University of Illinois projects that were approved by CUUATS in September 2024 to Fiscally Constrained projects list, and added Lincoln Avenue project that was inadvertently omitted:
    * UI-25-02: Oak Street from Kirby Avenue to St. Mary's Rd - road diet, bike lanes, and crosswalk addition
    * UI-26-01: Pennsylvania Avenue from Urbana city limit east to Lincoln Avenue - reconstruction with bike sharrows, sidewalk, and ramp improvements
    * UR-24-15: Lincoln Avenue from Florida Avenue to Green Street, corridor rehabilitation and safety improvements

13.	Under 2050 Vision > Future Projects, under Future Projects: Local and Unfunded, added the following to the Local and Unfunded table and map:
    * High-Speed Rail project
    * Parkland Way reconstruction in Champaign
    * Bradley Avenue/Canadian National railroad crossing improvements in Champaign
    * University Avenue Corridor Study between State Street and Wright Street in Champaign
    * Mathews Avenue shared-use path from Green to Armory (U. of I.) 
    * North-South Mathews extended shared-use path (U. of I.) 
    * Peabody Drive shared-use path from Fourth St to Sixth St (U. of I.) 
    * Lincoln Avenue shared-use path from Windsor Road to Curtis Road and from Florida Avenue to Hazelwood Drive (U. of I.) 

14.	Under 2050 Vision > Future Projects, under Future Projects: Local and Unfunded, revised the following to the Local and Unfunded table and map:
    * Removed the Mahomet traffic signal installation at US 150 and Lake of the Woods Road (this project is complete)
    * Added the Mahomet traffic signal installation on Prairieview Road at the I-74 ramps

15.	Under 2050 Vision > Scenario Modeling, added a summary of the Safety Forecasting Tool 

16.	Added all public outreach materials to Public Involvement sections 

17.	Added summary comments to Phase II and Phase III Public Involvement 

18.	Updated information in Phase III Public Involvement 

19.	Added appendices:
    * 2020 Census Data Analysis
    * System Performance Report
    * Travel Demand Model 
    * Safety Forecasting Tool
    * MTD Public Transportation Agency Safety Plan (PTASP)

20.	Added the System Performance Report requirements identified in 23 CFR 450.324(f)(4)(i) to the Federal LRTP Requirements appendix

21.	Addressed the Public Transportation Agency Safety Plan (PTASP) targets in the Systems Performance Report Appendix
