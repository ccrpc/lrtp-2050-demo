---
title: "Phase II Public Outreach"
date: 2024-06-20
draft: FALSE
weight: 40
bannerHeading: Phase II Public Outreach
bannerText: >
  Learn more about what area residents had to say during our 2024 outreach efforts. 
---
<style>
 .usa-accordion__heading{
   background-color: #4682B4;
 }
</style>

<h1> Phase Two Public Outreach </h1>
<h4> Survey from April 2024 to July 2024 </h4>

The second round of public outreach took place between April and July of 2024. During this period, public input focused on gathering feedback on project priorities, travel concerns, and community demographics. 

{{<image src="Round 2 Infographic.png" alt="Infographic depicting survey data from the second round of the LRTP survey." attr= "CUUATS" attrlink="https://ccrpc.org/">}}


{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="April 2024 Events"%}}
  25th & 26th - 2024 Christie Clinic Illinois Race Weekend Health & Fitness Expo, Champaign
  {{%/accordion-content%}}
  {{%accordion-content title="May 2024 Events"%}}
  1st - Activities and Recreation Center, UIUC

  4th - Urbana's Market at the Square, Urbana

  11th - U. of I. Extension Day at Martens Center, Champaign

  18th - Champaign Neighborhood Networks Campaign Kickoff, Champaign
  {{%/accordion-content%}}
  {{%accordion-content title="June 2024 Events"%}}
  8th - Mahomet Soda Fest, Mahomet

  12th - UPD Neighborhood Nights at Crestview Park, Urbana

  14th - MTD Juneteenth at Illinois Terminal, Community At-Large

  15th - Juneteenth (Douglass Park), Champaign

  22nd - Jettie Rhodes Day (King Park) Urbana

  {{%/accordion-content%}}
  {{%accordion-content title="July 2024 Events"%}}
  13th - Urbana's Market at the Square, Urbana
  {{%/accordion-content%}}
{{</accordion>}}

## About the Phase Two Surveys

Public outreach surveys for the second phase of the LRTP 2050 wanted to learn more about what residents want to see happen in the coming years. Learning more about this topic came in two survey forms: one about general goals statements and another about planned future projects from municipalities in the Metropolitan Planning Area. The following sections go over the questions and results from both surveys.

### Round 2 Comments

{{<accordion border="true" multiselect="true" level="3">}}
     {{%accordion-content title="Overall"%}}

This section summarizes the comments received from the following questions:

**Question 2**: What transportation improvements would enhance the transportation system where you live, work, shop, or play (e.g. biking, walking, transit, vehicle)?

**Question 8**: Other general comments or suggestions.

Depending on its contents, each comment provided was analyzed by mode and further by subtopic:
-	Automobile
-	Bicycle
-	Bus
-	Pedestrian
-	Train
-	Plane
-	General*

*General category includes comments that pertain to broad improvements not specific to a mode.

<rpc-table 
url="Overall.csv" 
text-alignment="1,r" 
table-title="Percentage of total comments by mode" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 
</rpc-table> 

  {{%/accordion-content%}}
     {{%accordion-content title="Automobile"%}}

The most comments pertaining to automobile improvements involved **improving roadway design** (e.g. improve lighting, parking, signaling, promote greenery, planning for car free development, and placemaking along roadways) (21 comments). This was followed by **improve roadway conditions**, specifically calling to repair pavement (20 comments) and **roadway reconfiguration**, particularly widening and configuring lanes (17 comments). **General support** for automobile improvements (12 comments), addressing **traffic speed and congestion** (7 comments), increasing **EV accessibility** (6 comments), and increasing **driver safety** (1 comment) were amongst the remaining comments.

<rpc-table 
url="Auto.csv" 
text-alignment="1,r" 
table-title="Automobile Comments by Topic (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 

 {{%/accordion-content%}}
     {{%accordion-content title="Bicycle"%}}

The most comments pertaining to bicycle improvements involved **general support** for broad bicycle improvements (52 comments). This was followed by **add new bicycle infrastructure** (e.g. bike lanes, bike paths, bike parking) (49 comments) and **improve existing bicycle infrastructure** (15 comments). **Bicycle safety** (15 comments) and **bicycle connectivity** (8 comments) across existing infrastructure were also mentioned as desired improvements. Finally, one comment mentioned making **bicycle access** free of charge.

<rpc-table 
url="Bicycle.csv" 
text-alignment="1,r" 
table-title="Bicycle Comments by Topics (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 
</rpc-table> 

  {{%/accordion-content%}}
{{%accordion-content title="Pedestrian"%}}
The most comments pertaining to pedestrian improvements involved **adding new pedestrian infrastructure** (e.g. sidewalks, crosswalks, stop signs) (40 comments). This was followed by **general support** for broad pedestrian improvements (39 comments). Other comments involved **improving existing pedestrian infrastructure** (e.g. existing sidewalks, crosswalks) (13 comments), **pedestrian safety** (10 comments), **enhance walkability** across facilities (6 comments), and **improve pedestrian accessibility** (2 comments).

<rpc-table 
url="Pedestrian.csv" 
text-alignment="1,r" 
table-title="Pedestrian Comments by Topic (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 

{{%/accordion-content%}}
{{%accordion-content title="Bus"%}}

The most comments pertaining to bus improvements involved **improving the existing bus services** (46 comments). This included the frequency of buses, the current bus route schedule, improving the accessibility of bus services for those living with disabilities and in underserved areas, and bettering the dissemination of information and resources for middle/high school riders. This was followed by **general support** for broad public transit improvements (40 comments) and **expand the existing bus services** to underserved and surrounding areas (including adding new buses and routes) (32 comments). Other comments involved **adding bus facilities** (e.g. new bus shelters and bus stops) (6 comments), **improving existing bus facilities** (e.g. making bus stops more accessible and improve existing stops/shelters) (3 comments), and **instituting free fare** (3 comments). 

<rpc-table 
url="Bus.csv" 
text-alignment="1,r" 
table-title="Transit Comments by Topic (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 

</rpc-table> 
{{%/accordion-content%}}
{{%accordion-content title="Train"%}}
The most comments pertaining to train improvements involved **adding rail infrastructure** (e.g. light rail, expand tracks). This was followed by **improving existing rail infrastructure** (e.g. Amtrak scheduling, frequency, train car condition) (5 comments), **adding high speed rail** (e.g. Champaign to Chicago) (3 comments), and **reducing traffic congestion** at train crossings (2 comments).

<rpc-table 
url="Train.csv" 
text-alignment="1,r" 
table-title="Train Comments by Topic (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 

{{%/accordion-content%}}
{{%accordion-content title="Plane"%}}
The comments pertaining to plane improvements involved **expanding the existing services** at Willard Airport to include more flights and destinations (5 comments).

<rpc-table 
url="Plane.csv" 
text-alignment="1,r" 
table-title="Plane Comments by Topic (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 

  {{%/accordion-content%}}
{{%accordion-content title="General"%}}
The comments pertaining to general improvements involved **improving accessibility** amongst all transportation modes (6 comments), as well as **enhancing corridor design** (e.g. adding areas to play, traffic gardens, dog park, skateboard park, and more green design) (6 comments). Improved **safety** (2 comments) and **connectivity** (1 comment) were also mentioned.

<rpc-table 
url="General.csv" 
text-alignment="1,r" 
table-title="General Comments by Topic (Count)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis (Round 2)"> 

  {{%/accordion-content%}}
{{</accordion>}}

## Goals Survey Response Trends

### Results of Goals Survey

The LRTP 2050 Goals Survey asked the community what transportation factors were of most importance to them. Topics varied from safety for different modes to expanding multimodal options to electric and autonomous vehicle travel. Options for level of importance were assigned values as follows: High equals 4, Moderate equals 3, Low equals 2, and Not at All equals 1. These values were then averaged among all respondents to determine which factors all respondents found the most or least important. Results are in the table below.

<center><rpc-chart 
url="Survey II Ranks.csv" 
type="horizontalBar" 
colors="#f0d75c"
y-label="Goal Statement"
x-label="Average Response Score" 
x-max="3"
x-min="0" 
legend-position="top" 
legend="false" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Public Opinion on LRTP 2050 Goal Priorities"> 
</rpc-chart></center> 

Far and away, the top-rated priority was improving safety of cyclists and pedestrians (score of 2.57). It then also makes sense that adding more spaces to bike and walk came in second (score of 2.41). From this, it appears residents are keen on improving safety factors before they feel comfortable biking and walking to or from destinations. People also clearly desire more places where this is possible to achieve safely. By creating more of these spaces, people feel this will add to their enjoyment and appreciation of the transportation system.

Other more strongly important factors that had much closer score ranges were in ranks 4-7 with average scores ranging from 2.35 to 2.29. These included better access to jobs, improved safety at intersections and railroad crossings, improved road conditions overall, improving air quality, and establishing high speed rail. A wide array of transportation desires, but all seem equally important to respondents.

The next batch, ranks 8-13 (score range from 2.17 to 1.96), appear to be largely related transit and regional travel. The lower importance of these items correlates with services that serve fewer people or serve people less frequently. Respondents were more likely to be interested in factors that affect their day-to-day travel first.

The final options, ranks 14-16 (score range from 1.77-1.24), related to changes in the vehicle fleet that traverses the area. Part of this is surprising as reducing the number of cars on the road and adding electric vehicle charging stations could be strategies for improving air quality, which came in 7th. However, respondents perhaps see air quality as having a direct effect on their lives while removing vehicles or adding EV stations would not be a direct benefit to their daily travel. 

Support for autonomous vehicle travel came in last. It is clear that respondents believe other issues should be cared for above this one even as the auto industry is making large strides towards autonomous features. It may be that the public has a misunderstanding of autonomous vehicle levels and how small infrastructure changes could improve travel safety. It may also be that the public does not feel we are ready to make these changes or that the technology is trustworthy enough. Time will tell whether the public becomes more comfortable with autonomous vehicles within the Metropolitan Planning Area.

### Goals Survey Demographics
#### Age of Respondents

<center><rpc-chart
url="Survey II Age.csv"
type="horizontalBar"
columns="1,2,3"
colors="#f0d75c, #5e8fd4"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title= "Percentage of Respondents by Age Compared to Census Data"></rpc-chart></center>

The age of respondents showed the largest gap for those ages 18-24. The timing of the survey being during the end of the University calendar year and during the summer meant that it was more difficult to reach out to student-age population. The survey did receive 79 responses from people in this age group overall. Underrepresentation was also seen for those 70 and older. Improvements in response rates from this age groups will be sought in future LRTP surveys as they tend to be consistently underrepresented.

In contrast, overrepresentation was seen in age groups 30-39 and 40-49. These age groups are more likely to engage with the survey material in general and attend summer events in the region. Balancing these groups should be considered for the next LRTP.
It should be noted that feedback from those outside the 18-24 range will also have more experience with the transportation system outside of the University District, where many people 18-24 reside. So, as most events for public outreach for the LRTP 2050 were outside the University District, the underrepresentation from this age groups makes sense.

#### Race of Respondents

<center><rpc-chart
url="Survey II Race.csv"
type="horizontalBar"
columns="1,2,3"
colors="#f0d75c, #5e8fd4"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title= "Percentage of Respondents by Race Compared to Census Data"></rpc-chart></center>

The race of respondents from the LRTP 2050 survey lined up consistently with the racial and ethnic make-up of the area according to the 2022 American Community Survey Estimates. These results are promising in showing that LRTP 2050 outreach was as geographically diverse as the region.

In the past, outreach has had difficulty with ensuring adequate outreach to underserved communities, but it was encouraging through this latest effort that outreach was representative. Goals desired by the communities should be considered generally representative for racial demographics.

#### Respondents by City or Village

<center><rpc-chart
url="Survey II City.csv"
type="horizontalBar"
columns="1,2,3"
colors="#f0d75c, #5e8fd4"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title= "Percentage of Respondents by Municipality Compared to Census Data"></rpc-chart></center>

Representation by municipality showed a small underrepresentation in Champaign, Savoy, and Tolono residents taking the survey and overrepresentation from Urbana and Mahomet. In past surveys, it has been a regular occurrence for Urbana residents to be more likely to take the survey. The overrepresentation from Mahomet was likely due to outreach being done at the Mahomet Soda Fest in June, where we received over 30 responses from Mahomet residents.

The breakdown by municipality is still generally good. Having a variety of events throughout the region provided a wide variety of feedback. As a note, these percentages are compared with 2019 population data due to the undercount of individuals in Urbana during the 2020 Census. More information can be found in the Decennial Census section of the LRTP 2050.

#### Gender of Respondents
<center><rpc-chart
url="Survey II Gender.csv"
type="horizontalBar"
columns="1,2,3"
colors="#f0d75c, #5e8fd4"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title= "Percentage of Respondents by Gender Compared to Census Data"></rpc-chart></center>
As with many surveys, there was underrepresentation from male respondents and overrepresentation from female respondents. This breakdown is better than the response rate from the LRTP 2045, but there appears to be a recurring issue with having men take the survey. Further efforts in the future will be made to continue to close this gap, but this difference is noted throughout survey-taking in general. Additionally, we included categories for a “Gender Non-Conforming” and a “Prefer Not to Answer” option. This totaled 8.9%. Because the Census does not provide these options for answers, the data cannot be directly compared. To better show representation, the 2019 data was normalized to account for the differences with the additional categories for gender provided in the survey.

<br>

#### Languages Spoken at Home by Respondents

<center><rpc-chart
url="Survey II Language.csv"
type="horizontalBar"
columns="1,2,3"
colors="#f0d75c, #5e8fd4"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title= "Percentage of Respondents by Language Spoken Compared to Census Data"></rpc-chart></center>

The LRTP 2050 survey asked about languages spoken at home. Overall, 6.4% of the respondents reported speaking a language other than English in their homes. Contrasting this with the 2019 Census data, and the was vast underrepresentation from this group.

The underrepresentation tends to be common for those who speak English less than very well. Though staff can provide materials in Spanish, Mandarin, and French, and surveys can be translated through Google Forms, other linguistic and cultural barriers exist in regard to outreach for this portion of the community.

Efforts in the future for greater representation will be sought. One portion of this requires greater outreach to the University campus community that houses a significant portion of those who speak a language other than English. Additionally, finding partners within specific cultural communities can assist with getting more outreach in these areas as well. 



<h2> Project Map Response Trends</h2>
The LRTP 2050 Project Map Survey asked the community what future transportation projects were of most importance to them. Projects were divided into two categories: Street Projects and Bike/Pedestrian Projects. Respondents were asked to select their top two street and bike/pedestrian projects from each of the following time frames: 2025-2029, 2030-2040, 2041-2050. The responses were analyzed in two ways: (1) by respondent municipality, in order to evaluate which projects were important to residents by where they live; (2) overall top projects by all residents in all municipalities. These results are shown in the sections and tables below.

{{<image src="Top Projects Map_FINAL.png" alt="Map showing the top projects selected from the second round of the LRTP survey." attr= "CUUATS" attrlink="https://ccrpc.org/">}}

<br>

### Top Street Projects
In the second round of the survey, we asked respondents to select their top two projects from each time frame (i.e. 2025-2029, 2030-2040, 2041-2050). The table below shows the top selected street projects by respondent residence for each time frame. The results show that most of the projects selected were within the same municipality as the resident or geographically close. For example, Mahomet respondents selected projects including Staley Rd. Despite Staley Rd being located in Champaign, it serves as a corridor from Mahomet to Champaign.

<rpc-table 
url="Top_street_2025_2029.csv" 
text-alignment="1,r" 
table-title="Top Street Projects by Resident Municipality (2025-2029)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Top_street_2030_2040.csv" 
text-alignment="1,r" 
table-title="Top Street Projects by Resident Municipality (2030-2040)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Top_street_2041_2050.csv" 
text-alignment="1,r" 
table-title="Top Street Projects by Resident Municipality (2041-2050)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 


<br>

### Regional Review (Street Projects)
We also analyzed the survey results to see which projects that respondents favored outside of their respective municipality of residence. The table below depicts the projects selected by residence. For each time frame (i.e. 2025-2029, 2030-2040, 2041-2050), most projects resembled the overall top-rated projects. More specifically, in 2025-2029, Project #1, 7, 8, and 23 remained highly rated; in 2030-2040, Project # 12, 13, 15, and 24 all remained highly rates; in 2041-2050, Project #22 and 26 remained highly rated. The results show that these projects are also regionally significant, as respondents from municipalities outside of the respective project location favored the project.

<rpc-table 
url="Regional_Street_2025_2029.csv" 
text-alignment="1,r" 
table-title="Top Street Projects Outside Municipality of Residence (2025-2029)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Regional_Street_2030_2040.csv" 
text-alignment="1,r" 
table-title="Top Street Projects Outside Municipality of Residence (2030-2040)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Regional_Street_2041_2050.csv" 
text-alignment="1,r" 
table-title="Top Street Projects Outside Municipality of Residence (2041-2050)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 


<br>

### Top Bike/Pedestrian Projects
In addition to street projects, the survey asked respondents to select their top two bike/pedestrian projects from each time frame (i.e. 2025-2029, 2030-2040, 2041-2050). The table below shows the top selected bike/pedestrian projects by respondent residence for each time frame. Again, we found that the projects selected by respondents were geographically related to their municipality of residence.


<rpc-table 
url="Top_bike_2025_2029.csv" 
text-alignment="1,r" 
table-title="Top Bike/Pedestrian Projects by Resident Municipality (2025-2029)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Top_bike_2030_2040.csv" 
text-alignment="1,r" 
table-title="Top Bike/Pedestrian Projects by Resident Municipality (2030-2040)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Top_bike_2041_2050.csv" 
text-alignment="1,r" 
table-title="Top Bike/Pedestrian Projects by Resident Municipality (2041-2050)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<br>

### Regional Review (Bike/Ped Projects)
Following the same procedure as street projects, we analyzed the bike/ped projects to see which projects that respondents favored outside of their respective municipality of residence. The table below depicts the bike/ped projects selected by residence. Similarly, for each time frame (i.e. 2025-2029, 2030-2040, 2041-2050), the projects resembled the overall top rated projects. More specifically, in 2025-2029, Project # 19 and 24 remained highly rated; in 2030-2040, Project # 20 and 37 remained highly rated; in 2041-2050, Project #22 and 55 remained highly rated. The results show that these projects are also regionally significant, as respondents from municipalities outside of the respective project location favored the project. Notedly, the project mentioned the most times amongst all municipalities was the Kickapoo Rail Trail.

<rpc-table 
url="Regional_Bike_2025_2029.csv" 
text-alignment="1,r" 
table-title="Top Bike/Pedestrian Projects Outside Municipality of Residence (2025-2029)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Regional_Bike_2030_2040.csv" 
text-alignment="1,r" 
table-title="Top Bike/Pedestrian Projects Outside Municipality of Residence (2030-2040)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 

<rpc-table 
url="Regional_Bike_2041-2050.csv" 
text-alignment="1,r" 
table-title="Top Bike/Pedestrian Projects Outside Municipality of Residence (2041-2050)" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Project Map Analysis"> 
</rpc-table> 


<br>

### High Speed Rail
Respondents were asked to rate their level of support for high-speed rail that would connect Champaign’s Illinois Terminal to Chicago in a 45 minute to 1-hour long train trip. Over 90 respondents strongly support, with an additional 20 who support. While 12 respondents had no opinion and 6 total respondents opposed high speed rail.

<rpc-chart 
url="High_Speed_Rail.csv" 
type="horizontalBar" 
colors="#00caaf"
y-label=""
x-label="Number of Respondents" 
x-max="100"
legend-position="top" 
legend="false" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Respondent Opinions on High Speed Rail"> 
</rpc-chart>

## Public Outreach Materials
Here you will find the materials used in Round 2 of public outreach.

*Surveys*

<a href="Round_2_Survey (one page).pdf">LRTP 2050 - Round 2 Survey - English</a>

<a href="Round_2_Survey (one page) french.pdf">LRTP 2050 - Round 2 Survey - French</a>

<a href="Round_2_Survey (one page) spanish.pdf">LRTP 2050 - Round 2 Survey - Spanish</a>

<a href="Project_Activity_Sheet FINAL.pdf">Project Map Activity Sheet - English</a>

<a href="Project_Activity_Sheet french.pdf">Project Map Activity Sheet - French</a>

<a href="Project_Activity_Sheet spanish.pdf">Project Map Activity Sheet - Spanish</a>

<a href="BIKE_PED MAP.png">Project Map - Future Project Priorities - Bike/Pedestrian Projects</a>

<a href="Large Map_Street.png">Project Map - Future Project Priorities - Street Projects</a>

<br>

*Paper Materials*

<a href="Table QR Codes.pdf">LRTP 2050 Tabling Event Fliers</a>

<a href="LRTP 2050 Sandwich Board_Round2.pdf">LRTP 2050 - Round 2 - Sandwich Board</a>

<a href="Soda Fest Comment Card.pdf">Mahomet Soda Fest Comment Card</a>


<br>

*Media and Press*

<a href="LRTP2050 social media post.png">LRTP 2050 - Round 2 - Social Media Post</a>

<a href="NEWS RELEASE 20240412 FINAL.pdf">LRTP 2050 - Round 2 - Media Release</a>