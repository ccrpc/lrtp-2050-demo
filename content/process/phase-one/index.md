---
title: "Phase I Public Outreach"
date: 2018-11-02
draft: FALSE
weight: 20
bannerHeading: Phase I Public Outreach
bannerText: >
  Learn more about what area residents had to say during our 2023 outreach efforts. 
---

<style>
 .usa-accordion__heading{
   background-color: #4682B4;
 }
</style>


## Phase One Public Outreach

### Travel Survey from April 2023 to October 2023

The first round of public outreach took place between April and October of 2023. During this period, public input focused on gathering sample information about transportation patterns, travel obstacles, and community demographics. 

CUUATS staff prepared a table for the public at 13 community events during the initial outreach period. The online map and the survey were both distributed at these events as the primary data collection methods. Paper surveys with pencils, laptops and tablets, and business cards with QR codes were available for participants to choose from. Those who completed the survey on-site were given a choice of incentives to encourage participation, which was highly successful. 


{{<image src="LRTP Round One- Infographic.png" alt="Infographic depicting survey data from the first round of the LRTP survey." attr= "CUUATS" attrlink="https://ccrpc.org/">}}




{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="April 2023 Events"%}}
  27th & 28th - 2023 Christie Clinic Illinois Race Weekend Health & Fitness Expo, Champaign
  {{%/accordion-content%}}
  {{%accordion-content title="May 2023 Events"%}}
  16th - Champaign Farmer’s Market, Champaign
  {{%/accordion-content%}}
  {{%accordion-content title="June 2023 Events"%}}
  3rd - Urbana’s Market at the Square, Urbana

  17th - Juneteenth Celebration at Douglass Park

  24th - MLK Jettie Rhodes Day Celebration at King Park
  {{%/accordion-content%}}
  {{%accordion-content title="July 2023 Events"%}}
  9th - Music by the Lake, Mahomet

  15th - Saturdays in the Park at Colbert Park, Savoy

  25th - Garden Hills Community Meeting at Hedge POP Park, Champaign
  {{%/accordion-content%}}
  {{%accordion-content title="August 2023 Events"%}}
  5th - Hessel Park Concert, Champaign

  10th - Hedge POP Park End of Summer Celebration, Champaign

  26th - Urbana’s Market at the Square, Urbana
  {{%/accordion-content%}}
  {{%accordion-content title="September 2023 Events"%}}
  14th - C-U Bike to Work Day, Urbana

  15th - Jazz Walk in Meadowbrook Park, Urbana

  19th - Light the Night, Urbana
  {{%/accordion-content%}}
  {{%accordion-content title="October 2023 Events"%}}
  7th - Climate Action Celebration, Urbana
  {{%/accordion-content%}}
{{</accordion>}}


<rpc-table 
url="Public_Outreach_Numbers.csv" 
text-alignment="1,r" 
table-title="Public Outreach Numbers" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis"> 
</rpc-table> 

## Online Input Map ##

[CUUATS](https://ccrpc.org/programs/transportation/) staff developed an online
map to collect input on strengths and weaknesses in the local transportation
system. This input contributed to the plan's goals and informed the agencies
that own and maintain local transportation facilities. The online map, while no
longer interactive, will serve as a public repository of input as long as
possible.

<iframe src="https://ccrpc.gitlab.io/transportation-voices-2050/"
  width="100%" height="600" allowfullscreen="true">
  Map displaying current land-use in the metropolitan planning area by
  parcel-level tax data.
</iframe>

### Online Input Map Input Trends ###
  Below is a summary of comments received from the online input map, categorized by transportation mode: pedestrian, bicycle, bus, automobile, train, plane, and all modes.

{{<accordion border="true" multiselect="true" level="3">}}
     {{%accordion-content title="Automobile Input"%}}
<b>Signalization:</b>

*Add a turn signal (2 comments)*
<ul>
<li>Kirby Avenue/State Street, Champaign</li>
<li>Washington Street/Market Street, Champaign</li>
</ul>

<br>

*Signalize intersection (3 comments)*
<ul>
<li>Prospect Avenue/Devonshire Drive, Champaign </li> 
<li>Kirby Avenue/Crescent Drive, Champaign</li>
<li>South Prairieview Road/I-74 Westbound Exit, Mahomet</li>
</ul>

<hr>

<b>Road Configuration Suggestions:</b>

*Implement a road diet (3 comments)*
<ul>
<li>Prospect Avenue (Windsor to Springfield), Champaign</li> 
<li>Kirby Avenue (Duncan to Prospect), Champaign</li>
<li>Mattis Avenue (Windsor to Kirby), Champaign</li>
</ul>

<br>

*Add a roundabout (3 comments)*
<ul>
<li>Florida Avenue/Vine Street, Urbana</li>
<li>Florida Avenue/Orchard Street, Urbana</li>
<li>Washington Street/Philo Road, Urbana</li>
</ul>

<br>

*Widen ramp so that drivers do not use shoulder to exit interstate (1 comment)*
<ul><li>Country Road 400 E/I-74 Westbound Exit, Mahomet</li>
</ul>

<br>

*Reconfigure intersection to provide E/W turn lanes and realign the N/S (1 comment)*
<ul><li>South Prospect Avenue/Ayrshire Circle, Champaign</li></ul>

<br>

*Keep parking downtown instead of decreasing it (2 comments)*

<ul><li>Downtown Urbana (2 comments)</li></ul>

<hr>

<b>Safety:</b>

*Drivers do not comply with street signs (1 comment)*
<ul><li>Green Street/Glover Avenue, Urbana</li></ul>

<br>

*Frequent auto crashes at this intersection (1 comment)*
<ul><li>Kirby Avenue/State Street, Champaign</li></ul>

<br>

*Drivers fail to merge before intersection and cause issues crossing intersection to one lane roadway (2 comments)*

<ul><li>Florida Avenue/Lincoln Avenue, Urbana</li>
<li>Windsor Road/Mattis Avenue, Champaign</li></ul>

<br>

*Reduce speed limit (1 comment)*
<ul><li>Perkins Road/Brownfield Road, Urbana</li></ul>

<br>

*Road is full of glass and needs swept (1 comment)*
<ul><li>N Market Street/Anthony Drive, Champaign</li></ul>

<br>

*Make medians/islands more visible (1 comment)*
<ul><li>Mattis Avenue/University Avenue, Champaign</li></ul>

<br>

*Make pavement markings more visible (2 comments)*
<ul><li>High Cross Road (between Windsor Road and Washington Street), Urbana</li>
<li>Country Club Road, Urbana</li></ul>

<hr>

<b>Traffic Flow:</b>

*Traffic Congestion (3 comments)*
<ul><li>N Prospect Avenue/I-74 Eastbound Exit, Champaign</li>
<li>South Prairieview Road/I-74 Westbound Exit, Mahomet</li>
<li>Northbound Lincoln Avenue/Westbound I-74 Entry, Urbana</li></ul>

<br>

*Westbound lights are too short and feel rushed (1 comment)*

<ul><li>N Prospect Avenue/Bloomington Road, Champaign</li></ul>

<br>

*Traffic light timing needs to be reconfigured (especially during peak hours) (4 comments)*

<ul><li>N Prospect Avenue/Marketview Drive, Champaign</li>
<li>Cunningham Avenue/Perkins Road, Urbana</li>
<li>N Prospect Avenue/I-74 Eastbound Exit, Champaign</li>
<li>Windsor Road/First Street, Champaign</li></ul>

<br>

*Increase speed limit (1 comment)*
<ul><li>South Lincoln Avenue (from Windsor Road to Hazelwood Drive), Urbana</li></ul>

<br>

*Add signage indicating two turn lanes to avoid traffic back up in one turn lane (1 comment)*

<ul><li>Northbound Lincoln Avenue/Westbound I-74 Entry, Urbana</li></ul>

<br>

*Add a turn lane to reduce congestion/back up (4 comments)*
<ul><li>Windsor Road/Morrissey Park Drive, Champaign</li>
<li>Prospect Avenue/Devonshire Drive, Champaign</li>
<li>Kirby Avenue/Crescent Drive, Champaign</li>
<li>Mattis Avenue/Broadmoor (southbound; TWLTL), Champaign</li></ul>
  {{%/accordion-content%}}
{{%accordion-content title="Bicycle Input"%}}

<b>Safety (speed):</b>

*High speed of motorized vehicles makes it feel unsafe to bike (8 comments)*
<ul><li>Washington Street/Philo Road, Urbana</li>
<li>Bradley Avenue/Bluegrass Lane, Champaign</li>
<li>Curtis Road near Embarras River, Urbana Township</li>
<li>Mattis Avenue/Gentry Square Lane, Champaign</li>
<li>Dunlap Avenue (US Route 45)/Curtis Road, Savoy</li>
<li>Cunningham Avenue (US Route 45)/Perkins Road, Urbana</li>
<li>Mattis Avenue/Church Street, Champaign</li>
<li>Broadway Avenue/University Avenue, Urbana</li></ul>

<br>

*Roadway width is too wide and encourages people to speed (4 comments)*
<ul><li>Dunlap Avenue (US Route 45)/Curtis Road, Savoy</li>
<li>Lincoln Avenue/Oregon Street, Urbana</li>
<li>Springfield Avenue at Grainger Engineering Library, Urbana</li>
<li>Illinois Street at Urbana’s Market at the Square, Urbana</li></ul>

<hr>

<b>Safety (separation): </b>

*Add barriers instead of rumble strips to on-street bike lanes (1 comment)*
<ul><li>Windsor Road/Morrissey Park Drive, Champaign</li></ul>

<br>

*Too many drivers park in the bike lane (1 comment)*
<ul><li>Green Street/Sixth Street, Champaign</li></ul>

<br>

*Improve space distribution between cyclists and pedestrians (1 comment)*
<ul><li>Wright Street/Armory Avenue, Champaign</li></ul>

<br>

*Improve safety of intersection for bicyclists (2 comments)*
<ul><li>Kirby Avenue/State Street, Champaign</li>	
<li>Broadway Avenue/University Avenue, Urbana</li></ul>

<hr>

<b>New Infrastructure:</b>

*Build new bike infrastructure (14 comments)*
<ul><li>Kirby Avenue/Valley Road, Champaign (2 comments)</li>
<li>Dunlap Avenue (US Route 45)/Tomaras Avenue, Savoy</li>
<li>Country Club Road/Division Avenue, Urbana</li>
<li>Country Club Road/Willow Road, Urbana</li>
<li>Broadway Avenue/Thompson St (Anita Purves Nature Center), Urbana</li>
<li>Main Street/Dodson Drive, Urbana</li>
<li>Neil Street (US Route 45)/Windsor Road, Champaign</li>
<li>Race Street/Sherwin Drive, Urbana Township</li>
<li>Sixth Street/John Street, Champaign</li>
<li>Illinois Street at Urbana’s Market at the Square, Urbana</li>
<li>W Florida Avenue between Busey Avenue and Orchard Street, Urbana</li>
<li>W Bloomington Road/N Mattis Avenue, Champaign	</li>
<li>Marketview Drive (between Prospect Avenue and Neil Street), Champaign</li></ul>

<br>

*Add trees along bike paths/roads for shade and aesthetic appearance (2 comments)*
<ul><li>First Street/Curtis Road (new First St. Bike Path), Savoy</li>
<li>Curtis Road/Parkview Lane, Savoy</li></ul>

<hr>

<b>Connectivity:</b>

*Bike lane/path suddenly ends, please fix/extend it to increase connectivity (7 comments)*
<ul><li>Main Street/Dodson Drive, Urbana</li>
<li>Green Street/Fifth Street, Champaign</li>
<li>Green Street/Wright Street, Champaign</li>
<li>Market Street (southbound under I-74), Urbana</li>
<li>W Bloomington Road/N Mattis Avenue, Champaign	</li>
<li>North Mattis Avenue, Champaign</li>
<li>Broadway Avenue/University Avenue, Urbana</li></ul>

<hr>

<b>Signalization:</b>

*Add bike intervals/signals (1 comment)*
<ul><li>Armory Avenue/Sixth Street, Champaign</li></ul>

<hr>

<b>Accessibility:</b>

*Inaccessible by bicycle or wheelchair (3 comments)*
<ul><li>Prospect Avenue/I-74 (bridge), Champaign</li> 
<li>W Bloomington Road/N Mattis Avenue, Champaign	</li>
<li>Marketview Drive (between Prospect Avenue and Neil Street), Champaign</li></ul>

  {{%/accordion-content%}}
{{%accordion-content title="Bus Input"%}}


<b>Route Service:</b>

*Issues with C-CARTS service availability (2 comments)*

<ul><li>Franklin Street/East Street (Mahomet Walgreens), Mahomet</li>
<li>Roberto Road/Halo Drive, Mahomet</li></ul>

<br>

*Expand MTD fixed-route bus service into new areas (2 comments)*

<ul><li>Byrnebruk Drive/Trout Valley Road (Lincolnshire Fields), Champaign</li>
<li>Kearns Drive/Ashford Court (Champaign Humane Society), Champaign</li></ul>

<br>

*Add MTD fixed-route bus service after 6 PM on Sundays (3 comments)*

<ul><li>Prospect Avenue/Meijer Drive (Champaign Walmart), Champaign</li>
<li>University Avenue/Chester Street (Illinois Terminal), Champaign</li>
<li>University Avenue/Market Street (Illinois Terminal), Champaign</li></ul>

<br>

*This is an important bus stop; please keep it (1 comment)*

<ul><li>Marketplace Mall, Champaign</li></ul> 

<hr>

<b>Stops:</b>

*Add additional MTD fixed-route bus service to existing areas/bus stops (6 comments)*

<ul><li>High Cross Road/Tatman Drive (Urbana Walmart), Urbana</li>
<li>Crestwood Drive/Clayton Road, Champaign</li>
<li>Broadway Avenue/Thompson Street (Anita Purves Nature Center), Urbana</li>
<li>Cunningham Avenue (US Route 45)/Perkins Road, Urbana</li>
<li>Neil Street/Kenyon Road (C-U Public Health District), Champaign</li></ul>

<br>

*Build more bus shelters (1 comment)*
<ul><li>Broadway Avenue/Thompson Street, Urbana</li></ul>

<br>

*Parkland Student Building is a great place to wait for an upcoming bus (1 comment)*

  {{%/accordion-content%}}
{{%accordion-content title="Pedestrian Input"%}}

<hr>

<b>Safety (speed):</b>

*High speed of motorized vehicles makes it feel unsafe to walk (11 comments)*
<ul><li>Cunningham Avenue (US Route 45)/Perkins Road, Urbana (2 comments)</li>
<li>Country Club Road/County Road 1700 N (Anita Purves Nature Center), Urbana
<li>Lincoln Avenue/Oregon Street, Urbana</li>
<li>Springfield Avenue at Grainger Engineering Library, Urbana</li>
<li>Grove Street/Oregon Street, Urbana</li>
<li>Prospect Avenue (US Route 150)/Vine Street, Champaign</li>
<li>Mattis Avenue/Church Street, Champaign</li>
<li>Mattis Avenue/Bradley Avenue, Champaign</li>
<li>North Prospect Avenue/I-74 (bridge), Champaign </li>
<li>Broadway Avenue/University Avenue, Urbana</li></ul>

<br>

*Roadway width is too wide, encouraging people to speed (3 comments)*

<ul><li>Lincoln Avenue/Oregon Street, Urbana</li>
<li>Springfield Avenue at Grainger Engineering Library, Urbana</li>
<li>Illinois Street at Urbana’s Market at the Square, Urbana</li></ul>

<hr>

<b>Safety (infrastructure)</b>

*Crosswalks and sidewalks feel unsafe and dangerous, please improve them (10 comments)*

<ul><li>Cunningham Avenue (US Route 45)/Perkins Road, Urbana (2 comments)</li>
<li>Curtis Road/Savoy Plaza Lane, Savoy</li>
<li>Lincoln Avenue/Oregon Street, Urbana</li>
<li>Sixth Street/Healey Street, Champaign</li>
<li>Vine Street/Pennsylvania Avenue (Blair Park), Urbana</li>
<li>University Avenue (US Route 150)/Cottage Grove Avenue (MTD Offices), Urbana</li>
<li>Prospect Avenue (US Route 150)/Vine Street, Champaign</li>
<li>University Avenue/State Street, Champaign</li>
<li>Mattis Avenue/Bradley Avenue, Champaign</li></ul>

<br>

*Add lighting for safer walking experience (1 comment)*
<ul><li>Lincoln Avenue (Nevada to Florida), Urbana</li></ul>

<hr>

<b>Accessibility:</b>

*Existing infrastructure makes it difficult to maneuver as a pedestrian (4 comments)*
<ul><li>North Prospect Avenue/I-74 (bridge), Champaign (2 comments)</li>
<li>Taylor Street to Fremont Street, Champaign</li>
<li>University Avenue/U.S. 150, Urbana</li>
<li>Make sidewalk to ADA accessible (1 comment)</li>
<li>Garden Hills Drive (North and South Railroad Crossing), Champaign</li></ul>

<hr>

<b>Signalization:</b>

*Increase crossing time at crosswalks (3 comments)*

<ul><li>Springfield Avenue (US Route 45)/Wright Street, Champaign</li>
<li>Bloomington Road (US Route 150)/Prospect Avenue, Champaign</li>
<li>Mattis Avenue/Bradley Avenue, Champaign</li></ul>

<br>

*Signalize crosswalk with pedestrian flashing light (5 comments)*
<ul><li>Florida Avenue/Orchard Street, Urbana</li>
<li>Florida Avenue (behind FAR), Urbana</li>
<li>Lincoln Avenue/Iowa Street (crosswalk), Urbana</li>
<li>Lincoln Avenue/Ohio Street (crosswalk), Urbana</li>
<li>Lincoln Avenue, Indiana (crosswalk), Urbana</li></ul>

<br>

*Increase frequency of pedestrian crossings (1 comment)*
<ul><li>Garden Hills Drive (North/South Railroad Crossing), Champaign</li></ul>

<hr>

<b>Safety (separation):</b>

*Improve space distribution between cyclists and pedestrians (2 comments)*
<ul><li>Wright Street/Armory Avenue, Champaign</li>
<li>Smith Road, Urbana</li></ul>

<br>

*Improve space between automobile traffic and pedestrians/cyclists (2 comments)*
<ul><li>Staley Road/I-72 Bridge, Champaign</li>
<li>University Avenue/U.S. 150, Urbana</li></ul>

<br>

*Dogs are loose and not on leashes (1 comment)*
<ul><li>Brady Lane/High Street, Urbana</li></ul>

<hr>

<b>Connectivity:</b>

*Add sidewalk infrastructure to increase connectivity and safety (4 comments)*
<ul><li>Country Road 350 East (between Thornewood Drive and Providence Road), Mahomet</li>
<li>Country Road 350 East (until 2400 N), Mahomet</li>
<li>Country Road 350 East (2400 N to 2425 N), Mahomet</li>
<li>University Avenue/U.S. 150, Urbana</li></ul>

  {{%/accordion-content%}}
{{%accordion-content title="Plane Input"%}}

None
  {{%/accordion-content%}}
{{%accordion-content title="Train Input"%}}
*Train service has deteriorated badly (1 comment)*
<ul><li> University Avenue/Market Street (Illinois Terminal), Champaign</li></ul>
  {{%/accordion-content%}}
{{</accordion>}}


### Champaign-Urbana Transportation Survey 2023

CUUATS staff used paper and digital surveys to collect transportation and demographic information from participants who live and/or work in Champaign County. The survey was available in English, Spanish, French, and Mandarin Chinese. The survey had a total of 21 questions divided into three sections (Section 1: Your Transportation Patterns, Section 2: Obstacles to Travel, and Section 3: Demographics). Section 1 was required for the survey response to count and be analyzed. Sections 2 and 3 were optional, but highly encouraged.
 
At outreach events, paper copies in all available languages and pencils were available for those who wished to fill it out on-site. Laptops and tablets were also available for those who preferred a digital method. Business cards with QR codes to the survey were also developed and distributed at outreach events, allowing participants to take the survey on their mobile device and/or take it on their own time, allowing table visitors to share the survey easily with family and friends. This input contributed to the plan’s goals and informed the agencies that own and maintain local transportation facilities. 

The below section highlights findings from the survey. A full analysis of survey findings will be developed as an Appendix to this document.

#### Survey Response Trends

<h3><b>SECTION 1: YOUR TRANSPORTATION PATTERNS (REQUIRED SECTION OF SURVEY)</b></h3>

Section 1 of the survey aims provide an idea of the current travel patterns of residents in the metropolitan planning area (MPA). Since 2018, which is when the previous LRTP 2045 public outreach was conducted, there have been changes in the factors that influence travel behavior in the MPA. The COVID-19 pandemic, the increasing shift to remote work, increased financial challenges of businesses and individuals, increasing student enrollment at the University of Illinois campus, new housing developments, the introduction of bike-share, and the completion of the Multimodal Corridor Enhancement Project (MCORE) are just some of the many factors that may have influenced changes in travel behavior since 2018. Data collected in this section also helps inform the development of the Champaign County Travel Demand Model (TDM) and CUUATS Land Use Model (Urban SIM). This section was required for respondents to complete for the survey response to be counted and analyzed.

<h4><b> Question 3: Please provide the name of the primary destination that you most commonly visit on a weekly basis.</b></h4>

By getting an idea of where respondents travel to within in the MPA on a weekly basis, these responses help to determine trip generation and trip distribution and provides an idea of major employers, educational institutions, commercial centers, and recreational attractions.  

<rpc-table 
url="Top_Primary_Destination_All.csv" 
text-alignment="1,r" 
table-title="Figure 3: Top 10 Primary Destination Categories" 
switch="false" 
source="CUUATS LRTP 2050 Survey and Online Map Analysis"> 
</rpc-table> 

<h4><b> Question 4: How do you travel to your primary destination in a typical week? </b></h4>

Identifying primary mode share distribution among respondents helps determine what modes are used the most at peak travel times in the MPA. Respondents were able to choose multiple modes as some residents may choose different modes throughout the week.

<rpc-chart
url="Q4_PM_Typical.csv"
type="horizontalBar"
columns="1,2"
colors="#f0d75c"
rows="1,2,3,4,5,6,7,8"
x-min="0"
x-label="Percent"
legend-position="top"
legend="true"
grid-lines="true"
chart-title= "Percentage of Respondents' Chosen Primary Mode(s)"></rpc-chart>

<h4><b> Question 5: How often do you work from home in a typical week?</b></h4>

The increase of remote work as a result of the COVID-19 pandemic had an impact on the travel methods and travel frequencies of the MPA's residents, so the LRTP 2050 aimed to learn more about how many respondents work remotely from home as opposed to commuting to a physical destination, either fully or on a hybrid-basis. This information is also very useful as there is currently not an accurate way to measure remote workers in travel demand modeling techniques, and modeling must rely upon estimating generated trips to convey work-from-home travel behaviors.

<rpc-chart url="Q5_WFH.csv"
  columns="-7"
  colors="#f0d75c, #a9da67, #5ed588,#00caaf,#00b9cc, #00a6db, #5e8fd4"
  chart-title="Percent of Survey Respondents Reporting Number of Days Working from Home"
  type="pie"></rpc-chart>


<h4><b>Question 8: How often do you use the following mode for long-distance travel when leaving Champaign County?</b></h4>

Examining how and when respondents leave the MPA for long-distance travel conveys the frequency and overall usage of the MPA’s currently available long-distance travel modes.  

<center><rpc-chart
url="Q8_Bus_All.csv"
type="bar"
colors="#5e8fd4,#f0d75c,#8db474"
columns="1,2,3,4"
rows="1,2,3,4,5,6,7,8"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title="Regional Bus Use Frequency"></rpc-chart></center>
<hr>
<center><rpc-chart
url="Q8_Train_All.csv"
type="bar"
colors="#5e8fd4,#f0d75c,#8db474"
columns="1,2,3,4"
rows="1,2,3,4,5,6,7,8"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title="Regional Train Use Frequency"></rpc-chart></center>
<hr>
<center><rpc-chart
url="Q8_Plane_All.csv"
type="bar"
colors="#5e8fd4,#f0d75c,#8db474"
columns="1,2,3,4"
rows="1,2,3,4,5,6,7,8"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title="Regional Airplane (Willard) Use Frequency"></rpc-chart></center>
<hr>
<center><rpc-chart
url="Q8_Car_All.csv"
type="bar"
colors="#5e8fd4,#f0d75c,#8db474"
columns="1,2,3,4"
rows="1,2,3,4,5,6,7,8"
x-min="0"
x-label="Percent"
width="60%"
aspect-ratio="1.5" 
legend-position="top"
legend="true"
grid-lines="true"
chart-title="Regional Personal Vehicle Use Frequency"></rpc-chart></center>

<h4><b>Question 9: How many motor vehicles are in your household?</b></h4>
Asking this question through a localized survey offers insight on the validity of other data sources CUUATS uses for other work (i.e. data estimates from the US Census Bureau).

The 18-24 year-old population had the lowest average number of vehicles per household, followed by 25-29 year-olds. Car ownership among respondents is generally lower among the university population. As a result, the reported number of cars owned increased with age. Students on the University of Illinois campus are also discouraged from owning vehicles due to the compact nature of the campus. From 30 and beyond, most respondents had 1 or 2 cars in their households, with significantly fewer people having 3 or more cars at home.

<rpc-chart 
url="Q9_Car_Owners.csv" 
type="horizontalBar" 
colors="#f0d75c, #a9da67, #5ed588,#00caaf,#00b9cc, #00a6db, #5e8fd4"
stacked="true" 
switch="true" 
columns="1,2,3,4,5,6,7,8" 
y-label="Age (Years)"
x-label="Number of Respondents" 
y-max="100" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Reported Number of Vehicles per Household"> 
</rpc-chart> 
<!--"#538fd4, #4e77b2, #3e6092,#2e4a73,#1f3555, #112139, #000c1f"-->

<h4><b>Questions 10 & 11: COVID-19 Pandemic Peak (2020-2022) Mode Use and Planned Mode Use for 2023 and Beyond. </b></h4>

The COVID-19 pandemic initially had a tremendous impact on transportation. Question 10 asked how residents initially reacted to restricted travel measures and how behaviors were beginning to change as these requirements were removed in 2023.

Vehicle, bike, transit, and walking modes are presented below. During COVID, people reported using bikes and walking more often than when COVID restrictions were relaxed, 40% and 60%, respectively. Post-COVID, even more respondents (55%) reported planning to use bikes more often. This number dipped slightly for walking, but was still significant at 47% for post-COVID plans. At the same time, more total respondents reported planning to use bikes or walk post-COVID. This shows great interest by the public in biking and walking in the region. Providing safe and connected networks for these modes appears to be important as more people seek to use alternative modes for everyday life.

As expected, vehicle use (42%) and transit ridership (63.1%) were reported to have gone down among the general population during the peak of COVID restrictions. At this time, many people were working at home more often and were discouraged from being around others to prevent the spread of disease. 

Respondents reported planning to use transit more often (46.7%) as restrictions relaxed. An additional 46 respondents reported plans to use transit again overall. This indicates that transit use was on the rebound in 2023, and this is confirmed by ridership numbers reported in the existing conditions section of this document. 

For vehicle use, people transitioned to reporting that they were planning to use their cars the same amount as they were during the peak of COVID in 2023. About the same number of people reported using this mode overall. This indicates that vehicle use may be changing considerably, at least in the short-term, and perhaps in the long-term as well. However, it appears there may not be a transition away from vehicle use overall. However, 2hile the same number of people have and use vehicles, fewer trips are being made. Higher rates of working-from-home and less desire for vehicle use may both be contributing factors to this. 

<center><rpc-chart 
url="Q10-Q11_Bike.csv" 
type="bar" 
colors="#f0d75c,#5ed588,#5e8fd4"
stacked="true" 
switch="true" 
y-label="Percent of Respondents" 
y-max="100"
width="60%"
aspect-ratio="1.5"  
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Frequency of Bike Use During and Post-COVID Restrictions"> 
</rpc-chart></center>
<hr>
<center><rpc-chart 
url="Q10-Q11_Walk.csv" 
type="bar" 
colors="#f0d75c,#5ed588,#5e8fd4"
stacked="true" 
switch="true" 
y-label="Percent of Respondents" 
y-max="100"
width="60%"
aspect-ratio="1.5"  
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Frequency of Walking During and Post-COVID Restrictions"> 
</rpc-chart></center>
<hr>
<center><rpc-chart 
url="Q10-Q11_Vehicle.csv" 
type="bar" 
colors="#f0d75c,#5ed588,#5e8fd4"
stacked="true" 
switch="true" 
y-label="Percent of Respondents" 
y-max="100"
width="60%"
aspect-ratio="1.5"  
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Frequency of Vehicle Use During and Post-COVID Restrictions"> 
</rpc-chart> </center>
<hr>
<center><rpc-chart 
url="Q10-Q11_Transit.csv" 
type="bar" 
colors="#f0d75c,#5ed588,#5e8fd4"
stacked="true" 
switch="true" 
y-label="Percent of Respondents" 
y-max="100"
width="60%"
aspect-ratio="1.5" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Frequency of Transit Use During and Post-COVID Restrictions"> 
</rpc-chart> </center>

<h4><b>Question 12: Rate the level of stress you feel when taking different modes to your primary destination.</b></h4>

CUUATS asked survey takers to rate the stress level of their trips to their primary destination. The "Level of Stress" is a metric used by transportation experts to estimate the difficulty of travel for users in the current system. CUUATS also uses a tool called Access Score to quantify travel stress on the roads for biking, walking, transit, or personal vehicle use. This question wanted to learn more about how individual users rate their own travel for comparison.

For the Access Score tool, average travel access by mode in the MPA goes from vehicles being the most accessible, then bikes, then transit, then walking; but, does accessibility equate to less stress by users? The reported answers to this question showed that cyclists report the highest stress in their travel, then vehicles, then transit and walking, which had similar response rates for each level of stress.

This indicates that availability does not always equate to easier or equitable travel. First, some people may opt out of using transit or walking because of time or other hazards (see Questions 13 and 14). Hence, it may be that only people that have the fewest barriers in using these modes will. Additionally, driving may be more stressful than walking or using transit because the transportation system is primarily built for vehicles. If people have to use a vehicle to get where they need to go, it is a given that they will have to encounter more stressful interactions on their commute. Cyclists report the highest stress of travel, and this may come from commonly having to share the road with vehicles, which are much larger and faster (see Question 13).

<rpc-chart 
url="Q12_LOS.csv" 
type="horizontalBar" 
colors="#f0d75c, #5ed588,#00b9cc, #5e8fd4"
stacked="true"
switch="true" 
y-label="Mode of Travel"
x-label="Percent" 
x-max="100" 
legend-position="top" 
legend="true" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Level of Stress by Mode to Primary Destination"> 
</rpc-chart> 

<h4><b>Question 13: What TRAVEL factors listed might PREVENT you from using different transportation modes around the Champaign-Urbana area? </b></h4>

Survey takers were asked about five <u>travel</u> factors that might prevent them from using other transportation modes for their trips: amount of traffic along the route, cost, distance to destinations, speed of traffic along the route, and time. The chart below shows the most common responses, where at least one in five respondents reported each factor listed for a specific mode.

The most common factor mentioned was time for biking, then transit use, then walking. Amount of traffic for biking came in fourth, then distance for walking and/or biking. When residents consider using alternative modes, it appears that they want to be closer to destinations overall to make using other modes worth it. While cost and traffic concerns are lower than time and distance for most modes, they were still reported somewhat often and should be taken into consideration.
<hr>
<rpc-chart 
url="Travel_Factors.csv" 
type="horizontalBar" 
colors="#5e8fd4"
y-label="Travel Factor"
x-label="Number of Respondents" 
x-max="240" 
legend-position="top" 
legend="false" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Travel Factors Preventing Mode Use"> 
</rpc-chart> 

<h4><b>Question 14: What SAFETY factors listed might PREVENT you from using different transportation modes around the Champaign-Urbana area? </b></h4>

Survey takers were asked about five <u>safety</u> factors that might prevent them from using other transportation modes for their trips: access to sidewalks, bike paths, or other pathways; crime or harassment concerns; quality of roads/sidewalks/paths; safety of intersections/crossings, and weather. The chart below shows the most common responses, where at least one in five respondents reported each factor listed.

Like question 13, a factor regarding bikes took the first spot. Issues with safety regarding bike access to pathways was the number one reason respondents chose not to use that travel mode. Weather, a largely uncontrollable factor, was reported second and third for biking and walking. The quality of the pathway for biking was forth, then access to sidewalks for walking was fifth. Intersection safety for biking and walking was also of major concern for respondents. While concerns regarding crime or harassment were less reported, it was still a risk factor mentioned for walking, transit, and biking by more than one in five survey takers.
<hr>
<rpc-chart 
url="Safety_Factors.csv" 
type="horizontalBar" 
colors="#00caaf"
y-label="Safety Factor"
x-label="Number of Respondents" 
x-max="230"
legend-position="top" 
legend="false" 
grid-lines="true" 
tooltip-intersect="true" 
chart-title="Safety Factors Preventing Mode Use"> 
</rpc-chart>

#### Demographics of Survey Takers

More information regarding the demographics of survey takers will be provided in the Appendices as they become available. Information was gathered on age, race, gender, employment status, and housing type for this survey to help CUUATS better understand various travel behaviors among different demographic categories.

(1) Larger findings include the increase of personal vehicle use and decrease of alternative mode use with increasing age until respondents reached 40-years-old and above. After 40, alternative mode use dwindled and personal vehicle use was much more common. This makes sense with the typical age range of University students in the area. University students a much more likely to use alternative modes as they are discouraged from vehicle use through the compact infrastructure of the University area.

(2) Women were more likely to use personal vehicles than men or gender non-conforming individuals.

(3) Full-time workers and retirees were the most likely to use a personal vehicle for travel and post-secondary students were the least likely. Part-time workers were equally likely to bike as to use a personal vehicle. <!--Unemployed individuals used all modes at approximately equivalent rates.--> Other types of employment status were reported but did not have enough respondents to make additional conclusions.

(4) White, Black/African American, and those of Two or More Races were more likely to use a personal vehicle than other modes. Hispanic/Latine and Asian survey takers used various modes at similar rates (vehicle, transit, bike, and walking).

(5) Bikeshare and rideshare are still not common, but approximately 1-3% of travelers used this mode at the time of this survey. Conclusions were not made on these modes regarding demographic trends.

## Public Outreach Materials
Here you will find the materials used in Round 1 of public outreach.

*Surveys*

<a href="LRTP 2050 Paper Survey English.pdf">LRTP 2050 - Round 1 Survey - English </a>

<a href="LRTP 2050 Paper Survey French.pdf">LRTP 2050 - Round 1 Survey  - French </a>

<a href="LRTP 2050 Paper Survey Mandarin Chinese.pdf">LRTP 2050 - Round 1 Survey  - Mandarian Chinese </a>

<a href="LRTP 2050 Paper Survey Spanish.pdf">LRTP 2050 - Round 1 Survey - Spanish</a>

<br>

*Paper Materials*

<a href="LRTP 2050 Sandwich Board.pdf">LRTP 2050 Sandwich Board (Version 1)</a>

<a href="LRTP 2050 Sandwich Board_v2.pdf">LRTP 2050 Sandwich Board (Version 2)</a>

<a href="LRTP 2050 Survey and Online Map Flier.pdf">LRTP 2050 Event Tabling Flier</a>



{{<image src="Sign_Event_Photo.jpg" alt="Photo showing the RPC/CUUATS table set up at Jettie Rhodes Day 2023, depicting the sandwich board and outreach material setup." attr= "CUUATS" attrlink="https://ccrpc.org/">}}