---
title: "2020 Decennial Census"
date: 2018-09-18T13:50:42-06:00
draft: false
weight: 10
bannerHeading: 2020 Decennial Census
bannerText: >
  Every 10 years, as stipulated by the US Constitution, the federal government is required to count all the people residing in the United States. CUUATS and other transportation agencies rely on Census data to make educated determinations about transportation needs.
---
Every 10 years, as stipulated by the US Constitution, the federal government is required to count all people residing in the United States. This count requires an immense amount of effort. Doing this helps to determine representation at the local, state, and federal levels. The data collected assists CUUATS and other local transportation agencies in making determinations about transportation modeling, projects, and priorities. The population counts also factor into the funding received by CUUATS from federal allotments.

## The Long Range Transportation Plan and the 2020 Decennial Census ##

The 2020 Census demographic data presented in the Long Range Transportation Plan 2050 is a snapshot in time when COVID restrictions were at their peak. This contrasts with the 2022 American Community Survey (ACS) data, which is informed by the Decennial Census as well as other sample surveys that are taken by the US Census Bureau in non-Census years. The Decennial Census is more comprehensive because it attempts to count every citizen, whereas the ACS takes a smaller sample and applies it broadly to make estimations about population and demographic changes over 1-year and 5-year periods.

For the Champaign-Urbana area, the 2020 Decennial Census snapshot appears to have some unusual population counts that would not be considered normal for the area, i.e., population undercounts. This undercounting is undoubtedly due to many students and faculty from the University of Illinois Urbana-Champaign not being in the area during 2020 because of online class use and other travel restrictions at the time. Therefore, CUUATS and other local agencies do not consider the 2020 Decennial Census numbers to be a reasonable representation of normal yearly population levels. This section details an explanation of these population deviations in the 2020 Decennial Census compared to ACS data trends over the last 10 years (2012-2022). It will also cover changes to the Census that will impact future analyses by CUUATS staff.

This section and the Demographics section present the census data and contextualize it to show that the student-age population normally present in Champaign-Urbana during the school year was underrepresented in US Census Bureau’s population counts. This can and will have effects on future grant opportunities and funding until the next census and may also influence the future projects chosen for the LRTP 2050. Conclusions are provided at the end of this section to explain how CUUATS will attempt to rectify issues with the data for the modeling scenarios completed as part of this document.
 
## Population Changes 

Typically, comparing single-year data to five-year data is not advised when presenting comparative statistics; however, this section will present the 2020 Decennial Census data (1-year) against the 5-year American Community Survey (ACS) data to examine potential issues with how the data represents reality. This comparison is done as an exception because the COVID-19 pandemic was an unprecedented event that affected the population of the Champaign-Urbana area, impacting the 2020 Decennial Census population counts. This ultimately has downstream effects to subsequent 5-year ACS data years. These data issues could continue until the next census count is completed in 2030. This section emphasizes significant changes that were unexpected when compared with the LRTP 2045 and with long-term ACS data.

<rpc-table 
url="Population_Statistics.csv" 
text-alignment="1,r" 
table-title="Champaign-Urbana Population 2020 Census and 2017 ACS Data Comparison" 
switch="false"
source-url="https://data.census.gov/table?g=160XX00US1707211,1712385,1746136,1767860,1775614,1777005&d=DEC%20Demographic%20Profile" 
source= "Decennial Census DEC Demographic Profile Data; LRTP 2045 Demographic Table Data">
</rpc-table> 

Calculated population changes from the 2017 data to the 2020 data are shown above. The numbers suggest that Champaign and Savoy gained a modest 3% and 3.6% in population, respectively. Contrastingly, Mahomet and Tolono made bigger gains at 15.5% and 24.3%, respectively. Lastly, Bondville and Urbana appear to have lost a significant amount of population, 18.0% and 9.0%, respectively. Some of these changes would be considered reasonable (Champaign, Mahomet, Bondville, and Savoy) while others were not expected (Tolono and Urbana).

In year-over-year comparisons (see 2020 Census Data Appendix), it was expected that Bondville would lose population. Thus, the loss of population is not surprising. Champaign and Savoy had a modest increase in their populations similar to previously estimated increases. Though Mahomet’s population increased significantly, the 15.5% population increase would be within the reasonable means of increase based on year-over-year projections and the increase in housing built there in recent years.

For the Village of Tolono, the population increased 24.3% increase compared to a more modest increase that had been projected after 2017. While a surprise, there is no concern, as Tolono has capacity for this population change. As seen in the Appendix 2020 Census Data section, the population increases came overwhelmingly from youth and working adult-aged persons. This indicates families moving to these areas, likely for schooling. This can be seen by the Village of Mahomet’s current request for school expansions. 

Contrastingly, Urbana’s population shows a 9.0% decrease, despite there having been a small population increase expected over previous yearly estimates (see the graph below). From this data, Urbana’s total population count appears astoundingly out of place with the other area changes. Urbana itself had not shown any mass population exodus before the COVID-19 pandemic, and what makes the area different is the significant contribution of University of Illinois students and faculty to its population.

Urbana and CUUATS suspect this drop in population is almost solely due to the lack of students being on campus during the census count. Proximate population estimates derived for updated ACS data before the next census will likely not consider this error until a recount of the population is completed. Unfortunately, a census recount is unlikely to be finished and approved until at least 2025, after the finalization of the LRTP 2050 document.

<rpc-chart url="Urb_ACS_Census_Pop.csv"
  chart-title="Urbana Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,orange"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="2020 Census Population Count"
data="-,-,-,-,-,-,-,-,38336,-,-">
</rpc-dataset></rpc-chart>

Other notable changes to the area’s counts that will have long-term effects include Bondville’s population decrease, and Tolono and Mahomet’s significant population increases. Some of the increases look to have outpaced the expected increases by the American Community Survey estimates. This will have an effect on area planning and growth expectations over the next 25 years.

On a final note, it was noted that Champaign and Savoy both appear to have had modest population increases over this period. However, looking more closely at specific data demographics in the proximate subsections, this reasonable population increase may have been far more substantial if it had not been for COVID-19’s changes to the area population in 2020. Further details follow in the subsections below.


## Age

Reviewing 2020 Census counts by age versus the 5-year ACS data estimates provides more insight on what demographic counts may be underrepresented in the region. The charts below show the total population counts taken during the 2020 Decennial Census by age and how they compare to ACS estimates before and after. Urbana’s graph is shown below, but other comparisons can be found in the 2020 Census Data Appendix. Hovering over the lines will show the specific population measurements for each age range. You can also deselect options by clicking on them in the legend as needed.

<hr>
<rpc-chart url="Urb_ACS_Census_Pop_Age.csv"
  chart-title="Urbana Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo, orange, green"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
type="line"
fill="false"
border-color="green"
background-color="green"
border-width="0"
point-radius="8"
label="65+ years old, 2020 Census, Point"
data="-,-,-,-,-,-,-,-,4519,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="orange"
background-color="orange"
border-width="0"
point-radius="8"
label="25-64 years old, 2020 Census, Point"
data="-,-,-,-,-,-,-,-,16903,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="indigo"
background-color="indigo"
border-width="0"
point-radius="8"
label="15-24 years old, 2020 Census, Point"
data="-,-,-,-,-,-,-,-,12543,-,-">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="blue"
background-color="blue"
border-width="0"
point-radius="8"
label="0-14 years old, 2020 Census, Point"
data="-,-,-,-,-,-,-,-,4371,-,-">
</rpc-dataset>
</rpc-chart>
<hr>

Upon inspecting these counts, Urbana’s population not only decreased, but the decrease significantly comes from persons aged 15-24 years, shown by the purple dot. This is a nearly 4,600-estimated shortfall when comparing the 2020 Census count to the ACS estimate for 2020. Comparatively, the 15–24-year-old population in Champaign grew for the same age range, approximately 877 people above the predicted population. This could suggest some students that would have previously lived in Urbana left for Champaign as new housing was built. However, it does not account for all of the population change. Additionally, the University has observed student counts growing year-over-year for some time. It would be highly unexpected for fewer people of this age range to be living in the area without an extraordinary change.

<hr>
<rpc-chart url="Ch_ACS_Census_Pop_Age.csv"
  chart-title="Champaign Estimated Population 2012-2022 and 2020 Decennial Census Population Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo,orange, green"
  legend-position= "right"
  source-url="https://data.census.gov/table/ACSDT5Y2012.B01001?q=B01001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT5Y2013.B01001" 
source= "2012-2022 ACS 5-year estimates, Table B01001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
    type="line"
    fill="false"
    border-color="green"
    background-color="green"
    border-width="0"
    point-radius="8"
    label="65+ years old, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,8958,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="orange"
    background-color="orange"
    border-width="0"
    point-radius="8"
    label="25-64 years old, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,38187,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="8"
    label="15-24 years old, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,28535,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="blue"
    background-color="blue"
    border-width="0"
    point-radius="8"
    label="0-14 years old, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,12622,-,-">
    </rpc-dataset>
</rpc-chart>
<hr>

Similarly, Savoy’s population saw growth. However, persons aged 15-24 still decreased, though by much less (327 fewer comparing 2020 data points, see 2020 Census Data Appendix. Altogether, these numbers indicate decline where the area was planning for growth as seen by housing unit increases. The difference, clearly, being the onset of COVID-19 pandemic and campus restrictions, reducing the student-age population in the area during the decennial count.

These anomalies in the population count could mean that local municipalities will request special census counts to get a more accurate picture of the current populations. These counts would not be completed until after the LRTP 2050 is published. This will also mean that population projections used in this LRTP will likely need to adjust from pre-pandemic data, i.e. 2019 ACS demographic and housing data, as these will likely be more accurate about the projections for area growth.


## Race and Ethnicity

The racial and ethnic makeup of the MPA population remains more diverse than the nation overall. The 2020 Census count showed some consistency in that the percentage of minority racial groups in Champaign and Urbana have the highest rates of non-white populations with fairly similar percentages for all races in both cities. Black or African American populations constitute the largest minority, followed closely by the Asian populations of both cities. Each of these groups are anywhere from 16.5% to 19.0% of the total population. The Asian population makes up the largest minority in Savoy at around 18.4%.

<rpc-chart
  url="Percent_Race_Pop.csv"
  type="bar"
  stacked="true"
  y-label="Percent Population"
  y-min="0"
  y-max="100"
  colors="#c4c4c4,blue,indigo,orange,green,red,lime"
  legend-position="top"
  legend="true"
  tooltip-intersect="true"
  source=" U.S. Census Bureau, Decennial Census DEC Demographic Profile Data, Table DP01"
    source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
  chart-title="2020 Decennial Census Population Proportion by Race"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data">
</rpc-chart>

Looking more closely at the 2020 Census numbers, the percentage of total minority populations increased in all MPA municipalities. This was heavily due a change in methodology by the US Census Bureau in how it reports racial designations. In previous decennial census surveys, up to two racial groups were identified per person. Additionally, official race categories were prioritized before adding Hispanic origin identities to the coded options. This meant that fewer racial identity combinations were calculated. However, in the data on race for the 2020 Census, up to six were allowed, i.e. all categorized responses. This caused significant increases in persons being reported as “Some Other Race” or as “Two or More Races”, and those results have readily changed the ACS data estimates from 2020 and beyond.

It becomes quite clear that the change to the coding of the results from the 2020 Decennial Census has caused increased reporting of persons as Some Other Race or as Two or More Races. Looking at the total differences from the ACS data and the 2020 Decennial Census data for Champaign and Urbana, there was a drastic increase in the number of people reported as Two or More Races and Some Other Race. The graphs for the other municipalities’ changes can be found in the 2020 Census Data Appendix. The change to the Census reporting ultimately changed the proportion of different races. This will also be the case throughout the country.

Additionally, it is critical to discuss the change in the White population for the area. First, a large proportion of the increases to the Some Other Race and Two or More Races categories likely came proportionately from the White population. This means that in the previous LRTP, many of these people would have been coded as White only. This would come directly from the change in the Census’ coding of race, many of whom would also be reporting Hispanic heritage as part of their race. This has been the case in previous decennial censuses, but the coding change clearly caused a drastic increase in the reporting.

The decrease in the White population for Champaign and Urbana are shown below. As suspected, the White population decreased significantly for both cities, but was this solely due to the change in the US Census Bureau’s updated methodology?

<hr>

<rpc-chart url="Race_CU_White_Pop_ACS.csv"
  chart-title="Champaign-Urbana Estimated White Population 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  switch="true"
  type="line"
  colors="indigo,#c4c4c4"
  legend-position= "right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data"><rpc-dataset
    type="line"
    fill="false"
    border-color="#c4c4c4"
    background-color="#c4c4c4"
    border-width="0"
    point-radius="8"
    label="Champaign White Population, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,47201,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="8"
    label="Urbana White Population, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,19784,-,-">
    </rpc-dataset>
</rpc-chart>

<rpc-chart url="Race_CH_Min_Pop_ACS.csv"
  chart-title="Champaign Estimated Non-White Populations 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo,orange,green,red,lime"
  legend-position= "right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data" ><rpc-dataset
    type="line"
    fill="false"
    border-color="lime"
    background-color="lime"
    border-width="0"
    point-radius="8"
    label="Two or More Races: 2020 Census"
    data="-,-,-,-,-,-,-,-,6638,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="red"
    background-color="red"
    border-width="0"
    point-radius="8"
    label="Some Other Race alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,3494,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="green"
    background-color="green"
    border-width="0"
    point-radius="8"
    label="Native Hawaiian and Other Pacific Islander alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,40,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="orange"
    background-color="orange"
    border-width="0"
    point-radius="8"
    label="Asian, 2020 Census"
    data="-,-,-,-,-,-,-,-,14737,-,-">
    </rpc-dataset>
    <rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="8"
    label="American Indian and Alaska Native alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,326,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="blue"
    background-color="blue"
    border-width="0"
    point-radius="8"
    label="Black or African American, 2020 Census"
    data="-,-,-,-,-,-,-,-,15866,-,-">
    </rpc-dataset>
</rpc-chart>

<rpc-chart url="Race_UR_Min_Pop_ACS.csv"
  chart-title="Urbana Estimated Non-White Populations 2012-2022 and 2020 Decennial Census Count"
  x-label="Year"
  y-label="Total Population"
  type="line"
  colors="blue,indigo,orange,green,red,lime"
  legend-position= "right"
  source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data" ><rpc-dataset
    type="line"
    fill="false"
    border-color="lime"
    background-color="lime"
    border-width="0"
    point-radius="8"
    label="Two or More Races: 2020 Census"
    data="-,-,-,-,-,-,-,-,2825,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="red"
    background-color="red"
    border-width="0"
    point-radius="8"
    label="Some Other Race alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,1367,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="green"
    background-color="green"
    border-width="0"
    point-radius="8"
    label="Native Hawaiian and Other Pacific Islander alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,13,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="orange"
    background-color="orange"
    border-width="0"
    point-radius="8"
    label="Asian, 2020 Census"
    data="-,-,-,-,-,-,-,-,7002,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="indigo"
    background-color="indigo"
    border-width="0"
    point-radius="8"
    label="American Indian and Alaska Native alone, 2020 Census"
    data="-,-,-,-,-,-,-,-,116,-,-">
    </rpc-dataset><rpc-dataset
    type="line"
    fill="false"
    border-color="blue"
    background-color="blue"
    border-width="0"
    point-radius="8"
    label="Black or African American, 2020 Census Count"
    data="-,-,-,-,-,-,-,-,7229,-,-">
    </rpc-dataset>
</rpc-chart>
<hr>

The answer is no, not all of the changes to these proportions were related to the 2020 Decennial Census coding change. The proportional percentage decreases in White population still outpaced the increases in the Some Other Race and Two or More Races categories. This means the absolute number of people from the White population decreased more than other racial categories increased (see table below). Comparing the 2019 ACS 5-year estimates* to the 2020 Decennial Census data, Champaign and Urbana show a much greater negative total population count for White people. Yet, we know that the COVID restrictions left many students learning from home, many not living in the area during that time. These unique changes to the population would have been reasonable to see as the University normally hosts tens-of-thousands of students every academic year. The same may also be true for the Asian and Black/African American populations to a lesser extent, but it is not clear as Champaign showed an increase in its Asian population and only minor decreases to the Black/African American populations.

<rpc-table 
url="Total_Pop_Diff_Race.csv" 
text-alignment="1,r" 
table-title="Change in Total Population by Municipality Estimated from 2019 ACS Data to 2020 Census Count" 
switch="false"
legend-position= "right" 
 source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data"> 
</rpc-table> 
<hr>

<rpc-chart
url="CH_Total_Pop_Diff_Race.csv"
type="bar"
colors="gray,blue,indigo,orange,green,red,lime"
y-label="Champaign 2020 Census Count Minus 2019 ACS Population Estimate"
tooltip-intersect="true"
 source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data"
chart-title="Difference between 2019 ACS Estimates and 2020 Decennial Census Counts by Race in Champaign"></rpc-chart>
<hr>

<rpc-chart
url="UR_Total_Pop_Diff_Race.csv"
type="bar"
colors="gray,blue,indigo,orange,green,red,lime"
y-label="Urbana 2020 Census Count Minus 2019 ACS Population Estimate"
tooltip-intersect="true"
 source-url= "https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
  source= "2012-2022 ACS 5-year estimates, Table B02001; Decennial Census DEC Demographic Profile Data"
chart-title="Difference between 2019 ACS Estimates and 2020 Decennial Census Counts by Race in Urbana"></rpc-chart>
<hr>

Finally, with the knowledge that the population of students had been increasing over the last decade, the data may also be hiding what would have been a higher population increase for the area in the last decade. Without the normal population of students on campus, it is impossible to guess how high the population counts may have been. It certainly is possible that Champaign and even Savoy would have had greater population increases had it not been for the necessary public health measures taken during the beginning of the COVID-19 pandemic.

*2019 ACS data was used to compare racial categorical changes because 2020 ACS 5-year data would include the updated methodology for the year 2020, diluting the comparative change.

### Conclusions

The takeaways from reviewing the 2020 Decennial Census data against the last 10 years of ACS data are that (1) White and student-aged populations were likely to have been undercounted in the urban area, (2) that the change to the Census questions will mean all areas of the country will have increases to the Some Other Race and Two or More Races population categories, and (3) the total population was likely undercounted for the City of Urbana, for the urban area, and for the MPA. These effects will probably not be fully corrected until the next census in 2030.

The numbers reported by the decennial census will have impacts on future grant work and for CUUATS in assessing and addressing underserved populations. This may also have other impacts on how transportation agencies in the region make planning decisions. Ultimately, because CUUATS suspects the count produced misleading population numbers, the LRTP will use population projections based on 2019 ACS data with modest population increases for future scenario modeling purposes.
