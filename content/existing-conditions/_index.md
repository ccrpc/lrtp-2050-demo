---
title: Existing Conditions
date: 2024-08-30
draft: false
menu: main
weight: 20
bannerHeading: Existing Conditions
bannerText: >
  Understanding the current context of our communities helps us build a better future.
---

Using a base year of 2020, existing conditions data establish a baseline from
which planning visions can be grounded. This section includes demographic, land
use, environment, and information specific to each transportation mode.

