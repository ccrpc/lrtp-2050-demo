---
title: "Demographics"
date: 2018-09-18T13:50:42-06:00
draft: false
weight: 10
bannerHeading: Demographics
bannerText: >
  The Champaign-Urbana Urban Area is a diverse region spanning 47 square
  miles and containing a population of more than 150,000 people in east-central
  Illinois.
---

The Champaign-Urbana urban area, updated in 2020, spans approximately 47 square miles and contains a population of more than <a href="https://data.census.gov/table/ACSDT1Y2022.B01001?q=B01001&g=400XX00US15211"> 150,000 people </a> in east-central Illinois. The region is located 135 miles south of Chicago, Illinois, 120 miles west of Indianapolis, Indiana, and 180 miles northeast of Saint Louis, Missouri. 

Urban area boundaries are updated as part of the U.S. Census Bureau's Decennial Census, last completed in 2020. The most recently updated boundaries for the geography meant that the size of the urban area decreased slightly and removed the Village of Tolono. However, the urban area also expanded outward, mostly to the north of Champaign-Urbana and also in west Champaign. Three municipalities are partially or wholly within the Champaign-Urbana urban area: the City of Champaign, the City of Urbana, and the Village of Savoy. 

In contrast, the Metropolitan Planning Area (MPA) is the 25-year planning area for the long range transportation plan. The MPA spans 179 square miles and includes the Champaign-Urbana Urban Area, the Village of Mahomet, and the Village of Tolono. The function of the MPA boundary is to consider areas that could be developed and included in the urban area over the next 25 years. Therefore, the federally determined urban area is different from the Metropolitan Planning Area, which is determined by local agencies. The difference between the 2010 urbanized area, the 2020 urban area, and the MPA is shown below.

## A Note on 2020 Decennial Census Results v. 2022 American Community Survey Results

The demographic information presented in this section is either from the American Community Survey's 5-year averaged data for 2022 or the 2020 Decennial Census <i>(referred to as the Census throughout)</i> data. There is an important limitation to mention before presenting this data. Through analysis completed by CUUATS, the 2020 census counts for the area show discrepancies in the area's regular demographic patterns. The obvious factor contributing to the discrepancy is that COVID restrictions were in full enforcement when the Census count was taken. As a result, CUUATS concludes that the area's population experienced an undercount of its normal population due to the closure of in-person classes at the University of Illinois while the count was being taken. The suspected undercount has led some municipalities to consider special recounts of their population. These counts will not be completed until after the LRTP 2050 has concluded. As a result, the information presented from the U.S. Census Bureau, dated 2020 and beyond, should be understood to show potential population undercounts, particularly for persons aged 18-24 that are White/Caucasian or Asian. The full discussion of this conclusion is in the Census count portion of the LRTP 2050. 

## Total Population and Land Changes

### Population

General characteristics of each municipality within the MPA are provided in the table below. From 2017 to 2022, the American Community Survey (ACS) 5-year estimates that Tolono and Mahomet experienced the greatest population growth with increases of 23.4 and 13.8%, respectively. Champaign and Savoy also experienced modest increases in population.

Bondville and Urbana's populations decreased significantly according to the data. However, the City of Urbana intends to do a census recount. It is suspected that the 7.0% decrease is a misrepresentation of the population because the Census could not account for students normally living in the area with the University of Illinois' on-campus COVID restrictions at the time of the count.
<hr>

<rpc-table 
url="Total_Pop_Land.csv" 
text-alignment="1,r" 
table-title="Population and Land Area of MPA Municipalities" 
switch="false" 
source-url="https://www.census.gov/geographies/reference-files/time-series/geo/gazetteer-files.2022.html#list-tab-264479560" 
source="U.S. Census Bureau, 2022 Gazetteer -- Places, Accessed 22 December 2023">
</rpc-table><rpc-table
source-url="https://data.census.gov/table/ACSDT5Y2021.B01003?q=Population+Total&g=160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
source="U.S. Census Bureau, 2018-2022 ACS 5-year estimates, Table B01003, Accessed 22 December 2023">
</rpc-table>
<hr>

### Land

Total land area generally remained the same or increased modestly for municipalities within the MPA. The greatest change in land area occurred in Mahomet at around a two percent increase. These increases were all proportionately less than those recorded for the LRTP  2045, indicating that population increases in Champaign and Mahomet were infill increases, a goal of the LRTP 2045. The chart below shows a comparison of land area changes compared to recorded Census population changes. Note that the changes to the Urbana population may be different once their special census takes place.
<hr>

<rpc-chart
url="Pop_Land_Chart.csv"
type="bar"
colors="blue,green"
y-label="Percent Change"
legend-position="top"
legend="true"
tooltip-intersect="true"
source="U.S. Census Bureau, 2018-2022 ACS 5-year estimates, Table B01003, Accessed 22 December 2023"
source-url="https://data.census.gov/table/ACSDT5Y2021.B01003?q=Population+Total&g=160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
chart-title="Percent Change in Population and Land Area, 2017-2022"></rpc-chart>
<hr>

## Age and Gender 
Comparing national and local demographics can be useful for identifying how the area is unique. When comparing age ranges (school-age, university-age, working age, and retirement age), the most significant difference between the local and national age distribution is the larger share of adults between the ages of 18 and 24 in Champaign and Urbana overall, which can be attributed to the University of Illinois campus population. Another significant difference is the proportion of adults 65 years and over living in Savoy compared with the U.S. and other local municipalities. Lastly, more suburban areas like Tolono, Mahomet, and Savoy also have a higher proportion of school-age children when compared to the overall national statistics.
<hr>
<rpc-chart
url="Age_Percent.csv"
type="bar"
switch="true"
stacked="true"
y-label="Percent of Population"
y-min="0"
y-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2018-2022 American Community Survey 5-Year Estimates, Table B01001"
source-url="https://data.census.gov/table?q=B01001:%20Sex%20by%20Age&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
chart-title="Percent Distribution by Age in the MPA, 2022"></rpc-chart>
<hr>

## People with Disabilities

Within the MPA, people with disabilities range from 5-18% of each municipality’s total population. Bondville’s percentage of residents with disabilities is the highest of all the MPA municipalities and higher than the national percentage.

Looking at the rest of the municipalities, there is a trend in the increased percent of population with a disability from <a href="https://ccrpc.gitlab.io/lrtp2045/existing-conditions/demographics/">2017</a> to 2022. It is possible that the lack of University students in the area at the time would artificially inflate this population between the ages of 18 and 64 most prominently. The University of Illinois, while exemplary in its accessibility status, normally lowers the proportion of persons with disabilities in the area. That being said, the estimated absolute population of people with disabilities is included here to provide a clearer indication of the population that may need additional accommodations along the transportation network. Additionally, it is assumed that the 0- to 17-year-old population with disabilities for the Villages of Bondville and Tolono are non-zero for planning and project purposes and within the margin of error provided by the U.S. Census Bureau (+/- 48 persons for both).

<rpc-chart
url="Disab_Perc_MPA.csv"
type="bar"
switch="true"
stacked="true"
columns="1,2,3,4"
y-label="Percentage of Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2018-2022 American Community Survey 5-Year Estimates, Table B18101, Accessed 22 December 2023"
source-url="https://data.census.gov/table?q=B18101&g=010XX00US_160XX00US1707211,1777005,1746136,1767860,1712385,1775614&d=ACS%205-Year%20Estimates%20Detailed%20Tables"
chart-title="Percentage of the Population with a Disability, 2022"><rpc-dataset
type="line"
label="National Percentage of People with Disabilities"
data="12.9,12.9,12.9,12.9,12.9,12.9"
fill="false"
order="7"
border-color="gray"
point-radius="0"></rpc-dataset></rpc-chart>

<rpc-table 
url="Disability_Total.csv" 
text-alignment="1,r" 
table-title="Total Estimated Population of Persons with Disabilities in the MPA by Age" 
switch="false" 
source-url="https://data.census.gov/table?q=B18101&g=010XX00US_160XX00US1707211,1777005,1746136,1767860,1712385,1775614&d=ACS%205-Year%20Estimates%20Detailed%20Tables0" 
source="U.S. Census Bureau, 2018-2022 American Community Survey 5-Year Estimates, Table B18101, Accessed 22 December 2023">
</rpc-table>

## Race and Ethnicity 

### Populations by Race
The racial demographics of the MPA appear to have changed significantly since 2017 when the previous LRTP 2045 was written. This is seen through the noticeable increase in persons identifying as "Some Other Race" or as "Two or More Races" in the data.  This change is likely to be significantly due to the change in the coding of this question by the U.S. Census Bureau (see 2020 Decennial Census section for details). 

Overall, the racial makeup of the Champaign and Urbana populations remains more diverse than the nation overall. Locally, the percentage of minority racial groups in Champaign, Urbana, and Savoy tends to be 2-to-3 times higher than Bondville, Tolono, or Mahomet.  

Also, as a result of the census question change, the percentage of total minority populations increased in all MPA municipalities from 2017. From CUUATS analysis of the population numbers, the most significant drop in the population was in White/Caucasian persons, specifically those college-aged. Again, it is suspected that on-campus COVID restrictions caused this change in more commonly seen population demographics. This may explain the significant difference in total minority percentages in Champaign when compared to previous LRTPs. 

It is also seen in the data presented that there was a significant reduction in the Asian population in Urbana and Savoy, proportionately, potentially contributing to the decrease in total minority populations for both municipalities. The reduction in college-age students during the 2020 period may have also contributed to the drop in the Asian population, as the collegiate population in the area normally carries a proportionately higher number of Asian residents as compared to other age groups.

<hr>

<rpc-chart
url="Race_Percent.csv"
type="bar"
switch="true"
colors="blue,orange,red,green,indigo,lime,#c4c4c4"
stacked="true"
y-label="Percentage of Population"
y-min="0"
y-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
grid-lines="true"
source=" U.S. Census Bureau, 2018-2022 American Community Survey 5-Year Estimates, Table B02001"
source-url="https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001:%20Race&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
chart-title="Race in MPA Municipalities, 2022"></rpc-chart>

<hr>

<rpc-chart
url="Minority_Percent.csv"
type="bar"
colors="indigo,#24ABE2"
stacked="false"
grid-lines="false"
xangle="-0"
columns="1,2,3"
y-label="Percentage of Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
grid-lines="false"
source=" U.S. Census Bureau, 2013-2017 and 2018-2022 American Community Survey 5-Year Estimates, Table B02001"
source-url="https://data.census.gov/table/ACSDT5Y2022.B02001?q=B02001:%20Race&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
chart-title="Race and Ethnicity in MPA Municipalities, 2018-2022">
<rpc-dataset
type="line"
fill="false"
border-color="indigo"
border-width="1"
point-radius="0"
order="3"
label="National Percentage of Minority Groups in 2017"
data="27,27,27,27,27,27">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="#24ABE2"
border-width="1"
point-radius="0"
order="4"
label="National Percentage of Minority Groups in 2022"
data="34.1,34.1,34.1,34.1,34.1,34.1">
</rpc-dataset></rpc-chart>
<hr>

### Populations by Ethnicity

Lastly, the Hispanic/Latino population ranges from 0.42% of the population in Tolono to 8.42% in Urbana. The proportion of the population from the 2022 ACS data varies significantly from the 2017 estimates. The ACS 5-year data for Tolono is suspected to be underrepresenting the actual Hispanic/Latino population. Based on the Census Count, <a href="https://data.census.gov/table/DECENNIALDHC2020.P9?q=Hispanic%20or%20Latino&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"> the population should be roughly 2.6%</a>. The other population percentages are within the margin of error supplied by the U.S. Census Bureau's tables.

<rpc-table 
url="Ethnicity_Percent.csv" 
text-alignment="1,r" 
table-title="Percent of Municipal Populations Identifying as Hispanic/Latino, 2022" 
switch="false" 
source-url="https://data.census.gov/table/ACSDT5Y2022.B03003?q=Hispanic%20or%20Latino&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT1Y2022.B03003" 
source="U.S. Census Bureau, 2018-2022 American Community Survey 5-Year Estimates, Table B03003, Accessed 5 January 2023">
</rpc-table>

## Income and Poverty

### Population Living Below the Poverty Level
The population living in poverty generally decreased within the MPA municipalities between the 2017 and 2022 according to the ACS 5-year data, ranging from 2.6% in Mahomet to 27.7% in Urbana. Champaign and Urbana have the highest percentages of people living in poverty, at 1.8 and 2.2 times the national average, respectively. Overall, every municipality had a decrease in poverty rates, from a 12.3% reduction in Champaign to a recorded 80.0% reduction for Bondville. However, these decreases may also be deceiving due to the Census.

The higher rates of poverty seen in Champaign, Urbana, and Savoy can be partially attributed to the university student population that largely subsists on student loans and other outside funding sources while studying at the University of Illinois. Due to the decline in this younger population during the time of the Census count, the new data reflected this population decrease. As an effect, this could have artificially deflated the poverty rate because this loss of population was only temporary due to COVID restrictions. In the Fall of 2022, a total of 56,644 undergraduate, professional, and graduate students were enrolled at the Urbana-Champaign campus. Since the last LRTP, the student enrollment grew around 18% though it is unclear how many of these students live in the area now as COVID has also increased enrollment in distance education. 

Overall, based on the data shown here and in the 2020 Decennial Census section, it is reasonable to believe that the reduction in population present during Fall 2020 could have contributed to the decrease in the overall population estimated to be below the poverty level. However, it is also possible that poverty rates have decreased, considering the national average decreased from 14.6 percent to 12.5 percent over the same time period. The uncertainty means that no specific conclusions on local poverty rates can be provided until the census recounts are completed in the future.

### Population Living at or Below 200% of the Poverty Level
Compared to the overal poverty levels, when reviewing the income-to-poverty ratio numbers provided by the U.S. Census Bureau, the stratification of income between the municipalities shrinks to some extent. Those that live at or below 200% of the poverty level ranges from 11.6% in Mahomet to 48.1% in Urbana. The urban area still experiences higher ratios than the outlying communities in the MPA.

<hr>

<rpc-chart
url="Poverty_Level_Percent.csv"
type="bar"
stacked="false"
columns="1,2,3"
rows="1,3,4,5,6,7,8"
colors="indigo, #24ABE2"
legend-position="top"
legend="true"
y-label="Percentage of Population"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2013-2017 and 2018-2022 American Community Survey 5-Year Estimates, Table B17001"
source-url="https://data.census.gov/table/ACSDT5Y2022.B17001?q=Poverty%20status&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005&tid=ACSDT1Y2022.B17001"
chart-title="Percentage of Population Living below the Poverty Level, 2017-2022">
<rpc-dataset
type="line"
fill="false"
border-color="indigo"
border-width="1"
point-radius="0"
order="3"
label="National Poverty Rate in 2013"
data="14.6,14.6,14.6,14.6,14.6,14.6">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="#24ABE2"
border-width="1"
point-radius="0"
order="4"
label="National Poverty Rate in 2017"
data="12.5,12.5,12.5,12.5,12.5,12.5">
</rpc-dataset></rpc-chart>
<hr>

<rpc-chart
url="Income_Poverty_Ratio.csv"
type="bar"
switch="true"
rows="1,3,4,5,6,7,8"
colors="red,#c77f87,gray,#c4c4c4,#DDDDDD"
grid-lines="true"
stacked="true"
legend-position="top"
legend="true"
y-label="Percentage of Population"
y-min="0"
y-max="100"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2018-2022 American Community Survey 5-Year Estimates, Table C17002"
source-url="https://data.census.gov/table?q=C17002:%20Ratio%20of%20Income%20to%20Poverty%20Level%20in%20the%20Past%2012%20Months&g=010XX00US_160XX00US1707211,1712385,1746136,1767860,1775614,1777005"
chart-title="Income to Poverty Ratio in the MPA, 2022">
<rpc-dataset
type="line"
label="National Rate of Poverty"
data="12.5,12.5,12.5,12.5,12.5,12.5"
fill="false"
order="7"
border-color="#470b11"
point-radius="0"></rpc-dataset>
</rpc-chart>

## Population Distribution and Density 

Changes in the urban area boundaries occur every ten years in conjunction with the decennial census. Since the last LRTP update, there were significant updates to the urban area definitions. This also caused significant changes to the urban area boundaries. This is discussed in greater detail in the 2020 Decennial Census section of this LRTP.

The urban area (formerly called the urbanized area) grew roughly 1.4% from 145,361 people in 2010 to 147,452 people in 2020. While the new urban area removed the Village of Tolono from the boundaries, the overall area of the new boundaries only shrunk by about 0.2 square miles because population density grew on the outskirts of Champaign and Urbana. Previously, the urbanized area trended around a three-percent growth rate from rural-to-urban land designation per decade in Champaign County from 1980 to 2010. Currently, the ratio of those living within the Champaign urban area to those outside the new boundary remains roughly the same as in 2010 at 72% urban to 28% rural. CUUATS suspects that the special census for Urbana will raise the urban area population proportion slightly when it is completed. 

It should also be noted that the U.S. Census Bureau previously had distinctions between urban clusters (smaller populations) and urbanized areas (larger populations). The new Decennial Census removed the distinctions and now only defines "urban areas". As a result, there are now three urban areas in Champaign County: the Champaign urban area (Champaign-Urbana-Savoy), the Mahomet urban area (Mahomet), and the Rantoul urban area (Rantoul-Thomasboro). A comparative summary of the urban area populations to the rest of the county can be found below. Important aspects to note are that 77% of the County population is urban within in the MPA, increasing to 84% for the whole County and all urban areas. 

<rpc-table url="Urban_Rural_Percent.csv" 
table-title="Champaign County to Champaign Urban Area Population Ratio (1980-2010)" 
text-alignment="1,r" 
switch="false" 
source=" U.S. Census Bureau, 1980-2010 Decennial Census, Summary File 1 Table P2 & DEC: Demographic and Housing Characteristics Tables, Accessed 10 January 2024"></rpc-table> 

<rpc-table url="Urban_Areas_Percent.csv" 
table-title="Urban and Rural Area Total and Percent Population, Champaign County, 2020 Decennial Census" 
text-alignment="1,r" 
switch="false" 
source-url="https://data.census.gov/table?q=P2&g=050XX00US17019_400XX00US15211,53416,73369&d=DEC%20Demographic%20and%20Housing%20Characteristics"
source="U.S. Census Bureau, 1980-2010 Decennial Census DEC: Demographic and Housing Characteristics Tables, Accessed 10 January 2024"></rpc-table> 

## Employment 

Champaign County is a regional employment center because of the presence of 
education, health care, and manufacturing employers in the area. The Census Bureau 2022 ACS 5-Year Estimates identified the following top 5 employment sectors for Champaign County in 2022. 

<rpc-table url="employment_2022.csv" 
table-title="Champaign County Top 5 Employment Sectors, 2022" 
text-alignment="1,r" 
switch="false" 
source=" U.S. Census Bureau, ACS 2022 5-Year Estimates, Table DP03, Accessed 01 February 2024"></rpc-table> 


### Unemployment Rate

According to the Illinois Department of Employment Security (IDES), the annual average unemployment rate increased from 4.3% in 2017 to 6.6% in 2020 with the onset of the COVID-19 pandemic. Since then, unemployment rates are similar to those pre-pandemic. The following table shows Champaign County's unemployment rate from 2017 to 2022.  

 
 <rpc-chart url="Unemployment 2017-2022.csv"
  chart-title="Champaign County Unemployment, 2017-2022"
  x-label="Year"
  y-label="Unemployment Rate"
  type="line"
  source-url="https://ides.illinois.gov/resources/labor-market-information/laus.html" 
  source=" IDES, Accessed September 5, 2023"></rpc-chart>

 

### Top Employers in Champaign County

Champaign County's list of top employers has remained relatively consistent over the years. With the most current imformation available, this section utilizes the Champaign County Regional Workforce Profile (2018) to determine the top employers and their total employees. The University of Illinois Urbana-Champaign is the county's largest employer, with nearly 15,000 employees. This is followed by Carle Foundation Hospital with an estimated 6,500 employees. The following table lists Champaign County’s top employers since 2018.
 
<rpc-table 
url="Top_Employers_2018.csv" 
text-alignment="1,r" 
table-title="Champaign County Top Employers (2018)" 
switch="false" 
source-url="http://www.champaigncountyedc.org/area-facts/directories-reports" 
source=" Champaign County Economic Development Corporation, 2018 Top Employers - Champaign County, Illinois, Accessed 7 September 2023"> 
</rpc-table> 
