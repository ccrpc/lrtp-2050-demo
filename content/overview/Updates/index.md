---
title: "Updates"
date: 2024-12-11
weight: 10
layout: "future_vision"
bannerHeading: Learn More about the LRTP 2050
bannerText: >
  
---
<style>
 .usa-accordion__heading{
   background-color: #4682B4;
 }
</style>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Survey Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
            .timeline {
                display: flex;
                flex-direction: column;
            }
            .arrow-component {
                display: flex;
            }
            .arrow {
                min-width: 50px; /* Adjust as needed */
                display: flex;
                align-items: center;
                justify-content: center;
                background-color: #007bff;
                color: white;
                padding: 5px;
                position: relative; /* Needed for arrowhead */
            }
            .arrow h3 {
                margin: 0;
                text-transform: uppercase;
                color: white;
                writing-mode: vertical-lr;
            }
            .text-section {
                flex: 1;
                padding: 10px;
                border: 1px solid black;
                position: relative; /* Needed for arrowhead */
            }
            .text-section h3 {
                margin-top: 0;
                text-decoration: underline;
            }
            /* Styles for main content */
            p {
                margin-bottom: 10px !important;
            }
            .arrow-blue {
                background-color: #007bff;
            }
            .arrow-blue::after {
                border-top-color:: #4169E1;
            }
            .arrow-green {
                background-color: #006f22;
            }
            .arrow-green::after {
                border-top-color: #006f22;
            }
            .arrow-yellow {
                background-color: #f0d659;
            }
            .arrow-yellow::after {
                border-top-color: #f0d659;
            }
            .arrow-red {
                background-color: #c05642;
            }
            .arrow-red::after {
                border-top-color: #c05642;
            }
            .title {
                font-weight: bold;
                background-color: #1e4ca0;
                padding: 5px 12px;
                color: #fff;
                letter-spacing: 0.05em;
                margin: 0;
                margin-bottom: 1em;
                line-height: 1.3;
                border: 1px solid #1284c8;
                font-family: "urw-din", sans-serif;
            }
            .notes-head {
                font-size: 1.4em;
                font-weight: bold;
                background-color: #1e4ca0;
                padding: 5px 12px;
                color: #fff;
                letter-spacing: 0.05em;
                margin: 0;
                line-height: 1.3;
                border: 1px solid #1284c8;
                font-family: "urw-din", sans-serif;
            }
            .notes {
                background-color: #e8f4fa;
                font-size: 1.2em;
                padding: 1em;
                border: 1px solid #1284c8;
                margin: 0;
                font-weight: normal;
            }
            /* Styles for participation section */
            .participate {
                background-color: #1e4ca0;
                padding: 8px 16px 4px 16px;
                color: #fff;
                font-size: 1.2em;
                font-family: "urw-din", sans-serif;
            }
            .participate p {
                color: white;
            }
            /* Styles for links within participation section */
            .participate a {
                text-decoration: none;
                color: #fff;
                border-bottom: 1px dotted #808284;
            }
            /* Styles for links within participation section on hover */
            .participate a:hover {
                border-bottom: none;
            }
             /* Arrow shape for larger screens */
            @media screen and (min-width: 768px) {
                .arrow::after {
                    content: "";
                    position: absolute;
                    bottom: -44px; /* Adjust to move the arrowhead up */
                    left: 50%;
                    transform: translateX(-50%);
                    border-left: 45px solid transparent;
                    border-right: 45px solid transparent;
                    z-index: 1;
                }
            }
            .arrow-blue::after {
                border-top: 45px solid #007bff;
            }
            .arrow-green::after {
                border-top: 45px solid #006f22;
            }
            .arrow-red::after {
                border-top: 45px solid #c05642;
            }
            .arrow-yellow::after {
                border-top: 45px solid #f0d659;
            }
            /* Media queries for responsiveness */
            @media screen and (max-width: 767px) {
                p {
                   margin-bottom: 0px !important;
                  }
                .notes-head {
                    font-size: 1.4em;
                }
                .arrow-component {
                	flex-direction: column;
              	}
                .arrow,
                .text-section {
                    width: 100%;
                    margin-bottom: 0;
                }
                .arrow h3 {
                    writing-mode: horizontal-tb; 
                }
                .text-section {
                position: relative;
                }
                .text-section::after {
                    content: "";
                    position: absolute;
                    top: 100%;
                    left: 50%;
                    transform: translateX(-50%);
                    border-left: 20px solid transparent;
                    border-right: 20px solid transparent;
                    border-top: 20px solid #007bff; /* You can change the color here */
                    z-index: 1;
                }
            }
        </style>
  </head>
  <body>
    <h2 class="section-divide welcome">Welcome</h2>
    <h3 class="welcome-flavour-text"> Learn more about the Long Range Transportation Plan 2050 Process </h3>
    <hr class="future-vision-hr">
    <section class="anchor-link-buttons">
      <div class="row">
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#timeline" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="fas fa-calendar-check icon-size mr-2 mb-3 mb-md-0"></i> Timeline and Events for the LRTP </a>
        </div>
        <!--<section><div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#general-comment" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="far fa-comment icon-size mr-2 mb-3 mb-md-0"></i> Public Comment Period and Surveys </a>
        </div>
        </section>-->
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#contact" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="fa fa-mobile icon-size mr-2 mb-3 mb-md-0"></i> Contact Us </a>
        </div>
        <div class="col-md-6 col-lg-3 mb-1 mb-md-0">
          <a href="#faq" class="d-flex align-items-center btn-block btn-anchor mb-3 mb-lg-0 p-2 p-md-4">
            <i class="fa fa-question-circle icon-size mr-2 mb-3 mb-md-0"></i> FAQ </a>
        </div>
      </div>
    </section>
    <section class="welcome">
      <p> The Long Range Transportation Plan is a visioning document about the regional transportation system and covers short- and long-term goals for the Champaign-Urbana Metropolitan Planning Area. Through the document, we receive input from residents and local agencies about what we expect transportation to look like in the next 25 years.
            
  This website serves as the final LRTP 2050 document. The following section details the timeline for the LRTP 2050. 
      
  As part of the previous LRTP 2045 process in 2019, CUUATS staff developed a [short video](https://youtu.be/9S0LQETxDAI) for people to learn about the LRTP Planning Process.
 
  You can also view [a video from the previous LRTP 2045](https://www.youtube.com/watch?v=MwJMs9c31dc) that provides illustrations of the future of transportation in the C-U region.
      

 </p>
    </section>
    <section class="alert">
      <article class="exclaim">
        <p class="section-intro">The CUUATS Long Range Transportation Plan (LRTP) is a vision for the future of transportation in the Champaign-Urbana metropolitan planning area, which also includes Savoy, Mahomet, Tolono, Bondville, the University of Illinois campus, and the Champaign-Urbana Mass Transit District service area.</p>
      </article>
    </section>
    <hr class="future-vision-hr">
    <section class="phase-1">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fas fa-calendar-check icon-size text-danger mr-3" aria-hidden="true"></i>
        <h2 id="timeline">Timeline for the LRTP</h2>
      </div>
      <div class="timeline mt-3">            
            <div class="arrow-component pb-3 mb-3">
                <div class="arrow arrow-blue">
                    <h3>DONE</h3>
                </div>
                <div class="text-section">
                    <h3>Determining Existing Conditions</h3>
                    <p>These items include reviewing the statistics for population demographics, environmental characteristics of the area, transportation habits of residents, and any land use changes that occurred from the last update of the LRTP to now. Learn more in the Existing Conditions tab.</p>
                </div>
            </div>
            <div class="arrow-component pb-3 mb-3">
                <div class="arrow arrow-blue">
                    <h3>DONE</h3>
                </div>
                <div class="text-section">
                    <h3>Phase I of Public Outreach</h3>
                    <p>The LRTP requires public input, and so CUUATS staff solicited responses to a survey about travel behaviors and potential obstacles residents have to their use of transport methods. This period also allowed for the public to provide feedback on the current system through its online map. Feedback received before the end of October was taken into consideration for the goals and for modeling future scenarios for the LRTP. The map is still available should residents like to provide feedback to CUUATS or to local agencies about their transportation experience.</p>
                </div>
            </div>
            <div class="arrow-component pb-3 mb-3">
                <div class="arrow arrow-blue">
                    <h3>DONE</h3>
                </div>
                <div class="text-section">
                    <h3>Scenario Planning</h3>
                    <p>CUUATS worked with local agencies to review their current work and determine their insights for the LRTP’s vision. From this feedback, CUUATS developed scenarios for its transportation models. Additionally, staff worked on developing principles related to the goals of the LRTP, and from this, staff worked with local agencies and the public to define feasible performance metrics for the next five years for the regional transportation system.</p>
                </div>
            </div>
            <div class="arrow-component pb-3 mb-3">
                 <div class="arrow arrow-blue">
                    <h3>DONE</h3>
                </div>
                <div class="text-section">
                    <h3>Performance Measures & Future Projects</h3>
                    <p>CUUATS staff worked with local agencies to develop performance measures that reflect the LRTP goals. Staff coordinated with local agencies on future project visioning. Starting in September, CUUATS staff will conduct another round of public feedback on this vision, the future projects, and the conclusions developed. Feedback will be collected both online and in-person.</p>
                </div>
            </div>
            <div class="arrow-component pb-3 mb-3">
                 <div class="arrow arrow-blue">
                    <h3>DONE</h3>
                </div>
                <div class="text-section">
                    <h3>Ensuring Fiscal Constraint and Finalizing Draft Document</h3>
                    <p>One of the requirements of any LRTP is that projects that are included need to be "fiscally constrained"; in other words, there needs to be funding set aside to complete the local, state, and federal projects that are in the plan. As the region looks to its future projects, it makes sense to require sound budgeting.</p>
                </div>
            </div>
                <div class="arrow-component pb-3 mb-3">
                 <div class="arrow arrow-blue">
                    <h3>DONE</h3>
                </div>
                <div class="text-section">
                    <h3>Draft LRTP 2050 Public Review</h3>
                    <p>The Draft LRTP 2050 was approved by CUUATS Technical and Policy Committees in early September 2024. It was followed by a 30-day public review period from September 16 through October 15, 2024. During that time, residents and other interested parties could read the Draft LRTP 2050 on this website or find paper copies to review at selected locations in the Metropolitan Planning Area. CUUATS staff also presented the LRTP to several area agencies from September through early November.</p>
                </div>
            </div>        </div>
    </section>
     <br>
    <!--<section>
    <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fas fa-calendar-check icon-size text-danger mr-3" aria-hidden="true"></i>
        <h2 id="timeline">Round Three Public Participation</h2>
      </div>
      <h5>Round Three public participation will involve a 30-day review period and presentations to local agency boards. Paper copies of the Draft LRTP 2050 can be found at the locations listed below:
      </h5>
    </section>-->
    <hr class="future-vision-hr">
    <!--<section>
    <section class="comment-form">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block far fa-comment icon-size text-danger mr-3" aria-hidden="true"></i>
        <h2 id="general-comment"> Phase III Public Outreach: Open Public Comment Period </h2>
      </div>
       <h5>Phase III of public feedback on the LRTP 2050 comes from the public comment period for the document. Public comments should pertain to material throughout this website in order to be considered relevant. The public comment period is 30 days, from September 16th to October 15th, 2024. All relevant comments will be reviewed and submitted to the CUUATS Technical and Policy Committees for consideration. We look forward to hearing from you about your thoughts on where transportation is going regionally for the Champaign-Urbana Metropolitan Planning Area.</h5>
       <br>
       </section>-->
      <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSclWl3MxY8S0yPsNoDkh1hmaOLXUGKubpNNaHDG3zQzd69IIA/viewform" width="100%" height="900" frameborder="0" marginheight="0" marginwidth="0"> Loading…</iframe>
    </section>
    <!--<section>
    <section class="alert">
      <article class="exclaim">
        <p class="section-intro">If you have any issues with viewing the above form, please click <a target="blank" href="https://docs.google.com/forms/d/e/1FAIpQLSclWl3MxY8S0yPsNoDkh1hmaOLXUGKubpNNaHDG3zQzd69IIA/viewform"> here </a>.</p>
      </article></section>
      </section>-->
          <hr class="future-vision-hr">
        <section class="event-form">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fa fa-mobile icon-size text-danger icon-size mr-3" aria-hidden="true"></i>
        <h2 id="contact"> Contact Us </h2>
      </div>
      <div id="equity">
            <!-- <h2 class="title" style="text-decoration: none;">
                <i class="fas fa-envelope"></i> Contact Us
            </h2> -->
            <h4 class="notes-head">How to Reach Us</h4>
            <div class="notes">
                <p>
                    If you have any questions, feel free to email us at<a href=mailto:LRTP2050@co.champaign.il.us> LRTP2050@co.champaign.il.us.</a>
                </p>
            </div>
            <article class="participate">
                <p>
                    Thank you for your interest in LRTP 2050!
                </p>
            </article>
        </div>
    </section>
    <hr class="future-vision-hr">
    <section class="faq">
      <div class="d-flex align-items-center mb-0">
        <i class="d-none d-sm-block fas fa-question-circle icon-size text-danger icon-size mr-3" aria-hidden="true"></i>
        <h2 id="faq"> FAQ </h2>
      </div>
      <p class="mb-3"> Welcome to our FAQ section! Below, you'll find answers to some of the most commonly asked questions about our products and services. If you can't find the information you're looking for, feel free to reach out to us. We're here to help you! </p>
        {{<accordion border="true" multiselect="false" level="5">}}
         {{%accordion-content title="Where do I return paper surveys?"%}}
If you took a survey on paper and need to return it to us, then you can do either of the following: 

  * Email a copy of the completed survey to LRTP2050@co.champaign.il.us 
  * Mail the hardcopy to the following address: CUUATS, 1776 East Washington Street, Urbana, IL 61802
  
  Thank you for your feedback!
         {{%/accordion-content%}}
         {{%accordion-content title="What geographical area does the LRTP 2050 cover?"%}}
 The LRTP 2050 includes the Champaign-Urbana Metropolitan Planning Area, which is made up of the following municipalities: City of Champaign, City of Urbana, Village of Savoy, Village of Mahomet, Village of Tolono, and Village of Bondville. 
        {{%/accordion-content%}}
        {{%accordion-content title="What is a Metropolitan Planning Area (MPA)?"%}}
 A Metropolitan Planning Area (MPA) is a geographical boundary determined in agreement between the state government and the local Metropolitan Planning Organization (MPO), within which the metropolitan transportation planning process is carried out. The MPA includes the census-defined Urban Area, as well as surrounding areas that could be developed and included in the urban area over the next 20 to 25 years. More specifically, the Champaign-Urbana Urban Area includes the City of Champaign, City of Urbana, Village of Savoy, and the Village of Bondville. However, the MPA takes additional areas into consideration for long range planning and includes the following: City of Champaign, City of Urbana, Village of Savoy, Village of Mahomet, Village of Tolono, and Village of Bondville.  
 
 The area MPO is the Champaign Urbana Urban Area Transportation Study (CUUATS) within the Champaign County Regional Planning Commission (CCRPC). 
 {{%/accordion-content%}}
       {{%accordion-content title="Which organizations are involved in the LRTP 2050?"%}}
Members of the steering committee meet on a quarterly basis with CCRPC staff to discuss project updates and provide input to guide the project along. The LRTP 2050 Steering Committee consists of representatives from various regional agencies and organizations. Those currently taking a role in active involvement include the following: 
  * [Champaign County](http://co.champaign.il.us/HeaderMenu/Home.php)
  * [Champaign County Bikes](http://www.champaigncountybikes.org/)
  * [Champaign County Chamber of Commerce](https://www.champaigncounty.org/)
  * [Champaign County Economic Development Corporation](https://www.champaigncountyedc.org/)
  * [Champaign County Emergency Management Agency](http://www.champaigncountyema.org/)
  * [Champaign County Forest Preserve District](https://www.champaignforests.org/)
  * [Champaign County Regional Planning Commission](https://ccrpc.org/)
  * [Champaign Park District](https://champaignparks.com/)
  * [Champaign Township](https://www.toi.org/township/Champaign-county-Champaign-township/)
  * [Champaign-Urbana Mass Transit District](https://mtd.org/)
  * [Champaign-Urbana Public Health District](https://www.c-uphd.org/)
  * [City of Champaign](https://champaignil.gov/)
  * [City of Urbana](https://www.urbanaillinois.us/)
  * [Experience Champaign-Urbana](https://experiencecu.org/)
  * [Family Service of Champaign County](https://www.famservcc.org/)
  * [Federal Highway Administration](https://highways.dot.gov/)
  * [Illinois Department of Transportation](http://idot.illinois.gov/)
  * [University of Illinois: Center on Health, Aging, and Disability](https://ahs.illinois.edu/center-on-health-aging-&-disability)
  * [University of Illinois: Facilities and Services](https://fs.illinois.edu/)
  * [Urbana Park District](https://www.urbanaparks.org/)
  * [Village of Mahomet](https://www.mahomet-il.gov/)
  * [Village of Savoy](https://www.savoy.illinois.gov/)
  * [Village of Tolono](https://www.tolonoil.us/)
  * [Willard Airport](https://www.ifly.com/champaign-airport)
      {{%/accordion-content%}}
       {{%accordion-content title="Which travel modes does the LRTP 2050 take into consideration?"%}}
Through LRTP 2050 public outreach efforts and surveys, CCRPC staff is interested in learning about how people move through the MPA using various travel modes, including the following:
  * Motor Vehicle or Rideshare
  * Transit/Bus
  * Train
  * Airplane
  * Regional Bus 
  * Bicycle (Scooter, E-Bike, E-Scooter, Bikeshare)
  * Pedestrian Travel (Walk/Run)
  * Wheelchair/Mobility Device
 {{%/accordion-content%}}
       {{%accordion-content title="How long does the process take?"%}}
The Long Range Transportation Plan is a visioning document about the regional transportation system and covers short- and long-term goals for the Champaign-Urbana Metropolitan Planning Area. Through the document, we receive input from residents and local agencies about what we expect transportation to look like in the next 25 years. The LRTP 2050 process itself is a two-year planning process. To learn more about the timeline and what is currently being done, refer to the <a href="#timeline">Timeline for the LRTP</a>.
 
 {{%/accordion-content%}}
        {{%accordion-content title="National Transportation Goals"%}}
  
  **Safety:** To achieve a significant reduction in traffic fatalities and serious injuries on all public roads.
  
  **Infrastructure Condition:** To maintain the highway infrastructure asset system in a state of good repair.
  
  **Congestion Reduction:** To achieve a significant reduction in congestion on the National Highway System.
  
  **System Reliability:** To improve the efficiency of the surface transportation system.
  
  **Freight Movement and Economic Vitality:** To improve the National Highway Freight Network, strengthen the ability of rural communities to access national and international trade markets, and support regional economic development.
  
  **Environmental Sustainability:** To enhance the performance of the transportation system while protecting and enhancing the natural environment.
  
  **Reduced Project Delivery Delays:** To reduce project costs, promote jobs and the economy, and expedite the movement of people and goods by accelerating project completion through eliminating delays in the project development and delivery process, including reducing regulatory burdens and improving agencies' work practices.
  
  _Source: United States Code, 23 U.S.C. 150(b)_
  {{%/accordion-content%}}
  {{%accordion-content title="State Transportation Goals"%}}
  
  **Economy:** Improve Illinois’ economy by providing transportation infrastructure that supports the efficient movement of people and goods.
  
  **Access:** Support all modes of transportation to improve accessibility and safety by improving connections between all modes of transportation.
  
  **Livability:** Enhance quality of life across the state by ensuring that transportation investments advance local goals, provide multimodal options and preserve the environment.
  
  **Stewardship:** Safeguard existing funding the increase revenues to support system maintenance, modernization and strategic growth of Illinois’ transportation system.
  
  **Resilience:** Proactively assess, plan, and invest in the state’s transportation system to ensure that our infrastructure is prepared to sustain and recover from extreme weather events and other disruptions.
  
  _Source: Illinois Department of Transportation, Long Range Transportation Plan, 2019_
  {{%/accordion-content%}}
{{</accordion>}}
    </section>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
      function submitForm() {
        event.preventDefault();
        //Fields that get mapped
        const safety = document.getElementById('safety').value;
        const connectivity = document.getElementById('connectivity').value;
        const quality_of_life = document.getElementById('quality_of_life').value;
        const sustainability = document.getElementById('sustainability').value;
        const reliability = document.getElementById('reliability').value;
        // Define the URL of the API endpoint
        const apiUrl = 'https://forms.ccrpc.org/wioa/lrtp-2050-survey/submission';
        // Define the request headers and body
        const requestOptions = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: {
              safety: safety,
              connectivity: connectivity,
              quality_of_life: quality_of_life,
              sustainability:sustainability,
              reliability:reliability
            }
          }),
        };
        // Make the HTTP request
        fetch(apiUrl, requestOptions).then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        }).then(data => {
          // Acknowledge response and reload page to reset sliders
          alert("Thank you for your submisssion!");
          location.reload();
        }).catch(error => {
          // Handle errors, such as network issues or server errors
          console.error('Error:', error);
        });
      }
    </script>
    <style>
      .slider {
        -webkit-appearance: none;
        width: 100%;
        height: 10px;
        background: #3186b4;
        outline: none;
        -webkit-transition: .2s;
        transition: opacity .2s;
      }
      .slider-car::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("car.png");
        width: 70px;
        height: 70px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-car::-moz-range-thumb {
        width: 70px;
        height: 70px;
        background: url("car.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bus::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("transit.png");
        width: 70px;
        height: 38px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bus::-moz-range-thumb {
        width: 70px;
        height: 38px;
        background: url("transit.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-plane::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("airplane.png");
        width: 70px;
        height: 70px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-plane::-moz-range-thumb {
        width: 70px;
        height: 70px;
        background: url("airplane.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-running::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("walk.png");
        width: 36px;
        height: 53px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-running::-moz-range-thumb {
        width: 36px;
        height: 53px;
        background: url("walk.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bike::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("bike.png");
        width: 70px;
        height: 45px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bike::-moz-range-thumb {
        width: 70px;
        height: 45px;
        background: url("bike.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      /* Style the labels */
      .range-labels {
        display: flex;
        justify-content: space-between;
        margin-top: 10px;
      }
      .range-labels>span,
      p {
        font-size: 1em;
      }
      .custom-heading {
        font-weight: 700;
      }
      @media (max-width: 767.98px) {
        .slider-car::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("car.png");
        width: 70px;
        height: 70px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-car::-moz-range-thumb {
        width: 70px;
        height: 70px;
        background: url("car.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bus::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("transit.png");
        width: 70px;
        height: 38px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bus::-moz-range-thumb {
        width: 70px;
        height: 38px;
        background: url("transit.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-plane::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("airplane.png");
        width: 70px;
        height: 70px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-plane::-moz-range-thumb {
        width: 70px;
        height: 70px;
        background: url("airplane.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-running::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("walk.png");
        width: 36px;
        height: 53px;
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-running::-moz-range-thumb {
        width: 36px;
        height: 53px;
        background: url("walk.png");
        background-size: 100% auto;
        cursor: pointer;
      }
      .slider-bike::-webkit-slider-thumb {
        -webkit-appearance: none;
        appearance: none;
        background: url("bike.png");
        width: 70px;
        height: 45px;
        background-size: 100%;
        cursor: pointer;
      }
      .slider-bike::-moz-range-thumb {
        width: 70px;
        height: 45px;
        background: url("bike.png");
        background-size: 100%;
        cursor: pointer;
      }
      }
    </style>
  </body>
</html>