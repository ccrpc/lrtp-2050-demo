---
title: "Acknowledgements"
date: 2024-11-14
draft: false
weight: 40
bannerHeading: Acknowledgements
bannerText: >
  CCRPC/CUUATS thanks those who collaborated to create the LRTP 2050.

---

#### CCRPC staff would like to extend our sincere thanks to all the residents, agencies, and community groups who participated in our public outreach efforts. The LRTP 2050 is a better plan because of your unique perspectives, ideas and input!

## LRTP 2050 Steering Committee

Representatives from the following agencies, organizations, and municipalities helped guide the LRTP 2050 planning process:

* [Champaign County](http://co.champaign.il.us/HeaderMenu/Home.php)*
* [Champaign County Bikes](http://www.champaigncountybikes.org/)
* [Champaign County Chamber of Commerce](https://www.champaigncounty.org/)
* [Champaign County Economic Development Corporation](https://www.champaigncountyedc.org/)
* [Champaign County Emergency Management Agency](http://www.champaigncountyema.org/)
* [Champaign County Forest Preserve District](https://www.champaignforests.org/)
* [Champaign County Regional Planning Commission](https://ccrpc.org/)
* [Champaign Park District](https://champaignparks.com/)
* [Champaign Township](https://www.toi.org/township/Champaign-county-Champaign-township/)
* [Champaign-Urbana Mass Transit District](https://mtd.org/)*
* [Champaign-Urbana Public Health District](https://www.c-uphd.org/)
* [City of Champaign](https://champaignil.gov/)*
* [City of Urbana](https://www.urbanaillinois.us/)*
* [Experience Champaign-Urbana](https://experiencecu.org/)
* [Family Service of Champaign County](https://www.famservcc.org/)
* [Federal Highway Administration](https://highways.dot.gov/)
* [Illinois Department of Transportation](http://idot.illinois.gov/)*
* [University of Illinois: Center on Health, Aging, and Disability](https://ahs.illinois.edu/center-on-health-aging-&-disability)
* [University of Illinois: Facilities and Services](https://fs.illinois.edu/)*
* [Urbana Park District](https://www.urbanaparks.org/)
* [Village of Mahomet](https://www.mahomet-il.gov/)
* [Village of Savoy](https://www.savoy.illinois.gov/)*
* [Village of Tolono](https://www.tolonoil.us/)
* [Willard Airport](https://www.ifly.com/champaign-airport)<br>

**CUUATS Member Agency*

## CUUATS Policy Committee (2023/2024)

* Deborah Frank Feinen, Chair - City of Champaign*
* John Brown, Vice-Chair - Village of Savoy*
* Diane Marlin - City of Urbana*
* Samantha Carter - Champaign County*
* Dick Barnes - MTD
* Scott Neihart - IDOT District 5 (retired)
* Jeff Angiel - University of Illinois*
* Morgan White - University of Illinois

**current representative in December 2024*

## CUUATS Technical Committee (2023/2024)
* Chris Sokolowski, Chair - City of Champaign*
* Stacey DeLorenzo, Vice-Chair - University of Illinois*
* Jeff Blue - County Highway Department*
* Jennifer Marner - County Highway Department*
* John Cooper - County Highway Department (retired)
* Karl Gnadt - MTD*
* Amy Snyder - MTD*
* Dalitso Sulamoyo - Regional Planning Commission*
* David Clark - City of Champaign
* Rob Kowalski - City of Champaign
* Dan Saphiere - City of Champaign*
* Kevin Garcia - City of Urbana*
* John Zeman - City of Urbana*
* Derek Bridges - IDOT District 5*
* Daniel Magee - IDOT District 5*
* Mark Moreschi - IDOT District 5
* Morgan White - University of Illinois
* Sarthak Prasad - University of Illinois*
* Christopher Walton - Village of Savoy*
* Roland White - Village of Savoy*

**current representative in December 2024*

## CCRPC Staff

### Planning & Community Development Division
* Rita Morocoima-Black, Planning & Community Developement Director
* Susan Burgstrom, Planning Manager
* Min Jiang, Research Analyst
* Rafsun Mashraky, Planner III
* Gabe Lewis, Planner III 
* Allison Gwinup, Planner II
* Gaby Harpel, Planner II
* J.D. McClanahan, Planner II
* Evan Alvarez, Planner II
* Dasom Han, Planner II
* Hasibul Islam, Transportation Engineer I 
* Emma Woods, HSTP Coordinator/PCOM 
* Leela Bhairy, Intern

### Data & Technology Division
* Tyler Rainey, Data & Technology Director
* Fabian Junge, Software Developer II
* Niranjan Abhyankar, Software Developer II
* Amer Islam, Software Developer I