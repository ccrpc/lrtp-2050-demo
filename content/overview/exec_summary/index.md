---
title: "Executive Summary"
date: 2024-08-26
draft: false
weight: 20
bannerHeading: Executive Summary
bannerText: >
  A snapshot of our two-year planning process and where to find information in the LRTP 2050 web plan.

---

{{<image src="CU_Urban_map4.png"
  position="right"
  caption="LRTP 2050 MPA and Champaign-Urbana Urban Area"
  attr="CUUATS"
  attrlink="https://ccrpc.org/" >}}

The Long Range Transportation Plan (LRTP), officially known as a Metropolitan Transportation Plan (MTP), is a federally mandated document that is updated every five years, and looks at the projected evolution of pedestrian, bicycle, transit, automobile, rail, and air travel over the next 25 years. The LRTP covers a 25-year Metropolitan Planning Area (MPA), which encompasses the Champaign-Urbana Urban Area as delineated by the 2020 U.S. Census as well as land outside the urban area that could be included in the urban area between the years 2025 and 2050.

Carried out by [Champaign Urbana Urban Area Transportation Study (CUUATS)](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) staff, in conjunction with the LRTP Steering Committee, this planning process has a regional scope, and is not meant to take the place of local transportation plans or comprehensive land use plans. Its main purpose is to identify major regionally beneficial transportation projects that can be targeted for federal funding. While smaller, localized transportation projects are reviewed and taken into consideration during the planning process, the LRTP lends itself to a broader regional focus and attempts to bring multiple jurisdictions together under one common vision. The Long Range Transportation Plan 2050 focuses on increasing the mobility of the metropolitan planning area's residents and improving the connectivity of the entire transportation system in order to provide residents with greater access to services and to create a more efficient travel network.

[Input from the public](https://ccrpc.gitlab.io/lrtp-2050/process/) and local agencies was combined with the [existing conditions data analysis](https://ccrpc.gitlab.io/lrtp-2050/existing-conditions/) to develop goals, objectives, and performance measures that reflect the transportation vision for 2050 and provide direction for the plan’s implementation. The five overarching long-term goals are **[safety](https://ccrpc.gitlab.io/lrtp-2050/goals/safety/), [reliability](https://ccrpc.gitlab.io/lrtp-2050/goals/reliability/), [sustainability](https://ccrpc.gitlab.io/lrtp-2050/goals/sustainability/), [equity and quality of life](https://ccrpc.gitlab.io/lrtp-2050/goals/equity_quality-of-life/), and [connectivity](https://ccrpc.gitlab.io/lrtp-2050/goals/connectivity/)**. These goals and their objectives are based on a combination of the Federal transportation goals, State of Illinois transportation goals, local knowledge, current local planning efforts, and input received from the public. Additionally, short-term objectives and performance measures translate the long range vision for 2050 into short term action. 

By federal law, the LRTP must include a financial plan that demonstrates how the adopted transportation plan can be implemented. In order to address this requirement and provide transparency about project implementation, the future transportation projects listed in this plan are labeled according to whether or not they have secured funding prior to this plan being approved. Lists and maps of both funded and unfunded [future projects](https://ccrpc.gitlab.io/lrtp-2050/vision/futureprojects/) provide additional detail about transportation improvement priorities in the region. More information on funding sources and how CUUATS staff calculated future funding levels can be found in the [Funding](https://ccrpc.gitlab.io/lrtp-2050/vision/funding/) section.

CUUATS staff and member agencies have spent considerable time and resources to develop the LRTP 2050. Carrying out the plan is essential to its achievement. The [Implementation](https://ccrpc.gitlab.io/lrtp-2050/vision/implementation/) section includes tasks related to both construction projects and conceptual tasks we can undertake toward improving the area transportation system. The section also includes obstacles to overcome as we carry out the plan.

As mentioned above, we sought ideas from area residents in the development of the LRTP 2050. The results of our public outreach efforts can be found in the [Public Involvement](https://ccrpc.gitlab.io/lrtp-2050/process/) section. 

* In [Phase I](https://ccrpc.gitlab.io/lrtp-2050/process/phase-one/) (2023), we asked residents to identify how and where they travel, and what issues they encounter when traveling. 

* In [Phase II](https://ccrpc.gitlab.io/lrtp-2050/process/phase-two/) (April-July 2024), residents completed surveys to help prioritize future transportation projects and establish the LRTP 2050 goals.

* In [Phase III](https://ccrpc.gitlab.io/lrtp-2050/process/phase-three/) (August-November 2024), we publicized the draft LRTP 2050 for public review and comment, and presented the plan to area agencies.

We received approximately 800 surveys over the two years, in addition to numerous individual comments that were crucial to developing a plan that is representative of our community's residents, needs, and ideas.

If you would like more information on LRTP federal requirements, the tools we used to forecast potential future scenarios, and other inputs, please visit the [Appendices](https://ccrpc.gitlab.io/lrtp-2050/appendices/) section.
