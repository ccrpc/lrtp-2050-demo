---
title: "Overview"
date: 2024-08-19
draft: false
menu: main
Weight: 10
bannerHeading: LRTP 2050 Overview
bannerText: >
  Learn more about CUUATS transportation planning history and the current LRTP process.
---

This section contains an executive summary of the LRTP 2050, and also provides
a history of local transportation infrastructure, local long range transportation
plans, and federal transportation planning legislation.
