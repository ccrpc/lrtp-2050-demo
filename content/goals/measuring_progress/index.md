---
title: "Measuring Progress"
date: 2024-08-01
draft: false
weight: 20
bannerHeading: Measuring Progress
bannerText: >
  The formulation of goals and objectives determines what direction planning efforts and implementation should take.
---

<style>
 .usa-accordion__heading{
   background-color: #4682B4;
 }
</style>

The [Champaign Urbana Urban Area Transportation Study (CUUATS)](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) staff, in conjunction with
the Long Range Transportation Plan (LRTP) Steering Committee, developed goals
with related objectives and performance measures to reflect the long range
transportation vision for 2050 and provide direction for the plan’s
implementation. The five overarching long-term goals are **safety, reliability, sustainability, equity and quality of life, and connectivity**. These goals and their
objectives are based on a combination of the Federal transportation goals, State
of Illinois transportation goals, local knowledge, current local planning
efforts, and input received from the public. Additionally, short-term objectives
and performance measures translate the long range vision for 2050 into short
term action. [CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) staff uses the
SMART (specific, measurable, agreed, realistic, and timebound) acronym to guide
the objective development process.

•	***Goals*** are defined as an end state that will be brought about by implementing
the plan.

•	***Objectives*** are sub-goals that help organize the plan implementation into
manageable parts.

•	***Performance Measures*** are metrics used to help agencies track the progress toward
each objective over time.

•	***Strategies*** are ideas or guiding principles to help agencies reach the stated goals and objectives.

## Annual LRTP Report Card

Each year, [CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) staff updates the
Annual LRTP Report Card to measure and document the progress made toward the
current LRTP’s short-term objectives using the performance measures. The Annual
LRTP Report Card helps provide transparency and continuity between five-year
LRTP updates by identifying opportunities and barriers to achieving LRTP goals
and objectives. Each objective’s performance measure (or measures) are assigned
a good, neutral, or negative rating depending on the circumstances and data
trend of each measure. The Report Card is reviewed by the
[CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php)
[Technical](https://ccrpc.org/divisions/planning_and_development/cuuats_technical_committee/index.php)
[Policy](https://ccrpc.org/divisions/planning_and_development/cuuats_policy_committee/index.php) Committees each year and
is available online for local agencies and members of the public.

The Annual LRTP Report Card tracks the performance measures in the LRTP for the active years of the plan, 2025 to 2029, with 2025 as the base year. The LRTP 2050 Report Card will become available in 2025.  The [Annual LRTP 2045 Report Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) tracks the performance measures in the LRTP 2045 for the years 2020 through 2024, with 2020 as the base year.


## Recent Additions to Federal Requirements

In addition to helping the metropolitan planning area refine its vision, the
LRTP 2050 has been updated in accordance with federal requirements. 
- In May 2016, the Federal Highway Administration (FHWA) and Federal Transit 
Administration (FTA) jointly issued the final rule on Statewide and Nonmetropolitan 
Transportation Planning and Metropolitan Transportation Planning, implementing
changes to the planning processes that had been established by the Moving Ahead
for Progress in the 21st Century Act (MAP21) and the Fixing America’s Surface
Transportation Act (FAST Act). These changes established 23 performance measures
intended to ensure effective investment of federal transportation funds. The
federal measures include five safety measures which are reflected in the LRTP
2050, as well as six infrastructure, three system performance, and nine transit asset
conditions measures. These additional measures are reflected in the CUUATS Annual 
Transportation Improvement Program (TIP) and are also documented in this plan in 
the [System Performance Report](https://ccrpc.gitlab.io/lrtp-2050/appendices/spr/). 
The System Performance Report includes an evaluation of system performance with 
respect to each performance target.

- In November 2021, President Biden signed the Infrastructure Investment and Jobs 
Act (IIJA), also known as the Bipartisan Infrastructure Law (BIL). The BIL continued 
the existing performance measures established under MAP21 and FAST Act. The BIL also 
added a performance measure for greenhouse gases (GHG); however, the reporting 
requirements for that measure are still being finalized as of 2024.

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="National Transportation Goals"%}}
  **Safety:** To achieve a significant reduction in traffic fatalities and serious injuries on all public roads.    

  **Infrastructure Condition:** To maintain the highway infrastructure asset system in a state of good repair.

  **Congestion Reduction:** To achieve a significant reduction in congestion on the National Highway System.

  **System Reliability:** To improve the efficiency of the surface transportation system.

  **Freight Movement and Economic Vitality:** To improve the National Highway Freight Network, strengthen the ability of rural communities to access national and international trade markets, and support regional economic development.

  **Environmental Sustainability:** To enhance the performance of the transportation system while protecting and enhancing the natural environment.

  **Reduced Project Delivery Delays:** To reduce project costs, promote jobs and the economy, and expedite the movement of people and goods by accelerating project completion through eliminating delays in the project development and delivery process, including reducing regulatory burdens and improving agencies' work practices.

  *Source: United States Code, 23 U.S.C. 150(b)*
  {{%/accordion-content%}}
  {{</accordion>}}

  {{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="State Transportation Goals"%}}
  **Economy:** Improve Illinois’ economy by providing transportation infrastructure that supports the efficient movement of people and goods.

  **Access:** Support all modes of transportation to improve accessibility and safety by improving connections between all modes of transportation.

  **Livability:** Enhance quality of life across the state by ensuring that transportation investments advance local goals, provide multimodal options and preserve the environment.

  **Stewardship:** Safeguard existing funding the increase revenues to support system maintenance, modernization and strategic growth of Illinois’ transportation system.

  **Resilience:** Proactively assess, plan, and invest in the state’s transportation system to ensure that our infrastructure is prepared to sustain and recover from extreme weather events and other disruptions.

  *Source: [Illinois Department of Transportation, Long Range Transportation Plan, 2019](http://idot.illinois.gov/transportation-system/transportation-management/planning/lrtp/index)*
  {{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="LRTP 2050 Goals"%}}
  **Safety:** Keep Everyone Protected: The metropolitan planning area transportation system will be maintained, preserved, and enhanced to safely move people and goods in the short term and to design and implement improvements to achieve the Illinois and CUUATS goal of zero deaths and disabling injuries in the long term.    

  **Reliability:** Make Every Trip Dependable: Residents and visitors of the metropolitan planning area will be provided with timely and dependable travel options to and from communities, healthcare facilities, amenities, employment, shopping, and educational centers that inspire confidence in the transportation system.

  **Sustainability:** Make Every Mile Clean: Active transportation modes and new transportation technologies will be maintained and expanded upon in the metropolitan planning area to reduce the transportation system’s contribution to greenhouse gas emissions and environmental degradation and to improve the quality of human and environmental health in the region.

  **Equity and Quality of Life:**  Make the Community Vibrant: Everyone in the metropolitan planning area will be provided with access to safe, affordable, and efficient transportation choices to support a vibrant economy and improved quality of life in every part of the community.

  **Connectivity:** Make the System Comprehensive: A cost-effective multimodal transportation system will provide users choices in how they travel and will increase accessibility, connectivity, and mobility of people and freight to all parts of the metropolitan planning area.

  {{%/accordion-content%}}
{{</accordion>}}


{{<image src="IDOT_Fed_Goals.png"
  alt="Graph showing the level of alignment between LRTP goals and Federal and IDOT Goals."
  caption="LRTP 2050 Goals level of alignment with IDOT Goals and Federal Planning Factors"
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}