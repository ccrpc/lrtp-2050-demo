---
title: "Sustainability"
date: 2024-08-07
draft: false
weight: 50
bannerHeading: Sustainability
bannerText: >
  Make every mile clean
bannerTextIcon : sustainability
---
### Sustainability Goal
#### Active transportation modes and new transportation technologies will be maintained and expanded upon in the metropolitan planning area to reduce the transportation system’s contribution to greenhouse gas emissions and environmental degradation and to improve the quality of human and environmental health in the region.

### Objectives and Performance Measures

<rpc-table
url="sustainability_table.csv"
rpc-table>

### Strategies

<rpc-table
url="Sustainability_Strategies.csv"
rpc-table>

