---
title: "Connectivity"
date: 2024-08-07
draft: false
weight: 70
bannerHeading: Connectivity
bannerText: >
  Make the system comprehensive 
bannerTextIcon: connectivity
---
### Connectivity Goal 
#### A cost-effective multimodal transportation system will provide users choices in how they travel and will increase accessibility, connectivity, and mobility of people and freight to all parts of the metropolitan planning area.


### Objectives and Performance Measures

<rpc-table
url="connectivity_table.csv"
rpc-table>

### Strategies

<rpc-table
url="Connectivity_Strategies.csv"
rpc-table>
