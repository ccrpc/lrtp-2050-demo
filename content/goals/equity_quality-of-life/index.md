---
title: "Equity & Quality of Life"
date: 2024-08-07  
draft: false
weight: 60
bannerHeading: Equity & Quality of Life
bannerText: >
  Make the community vibrant 
bannerTextIcon: equity-and-quality-of-life
---
### Equity & Quality of Life Goal
#### Everyone in the metropolitan planning area will be provided with access to safe, affordable, and efficient transportation choices to support a vibrant economy and improved quality of life in every part of the community.


### Objectives and Performance Measures

<rpc-table 
url="equity_table.csv"
rpc-table>

### Strategies

<rpc-table 
url="Equity_Strategies.csv"
rpc-table>
