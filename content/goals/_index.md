---
title: Goals
date: 2024-08-07
draft: false
menu: main
weight: 30
bannerHeading: Goals, Objectives, and Strategies
bannerText: >
  Setting goals help guide us toward our preferred future.
---

The five LRTP 2050 goals are safety, reliability, sustainability, equity and quality 
of life, and connectivity. These goals and their objectives are based on a combination 
of the Federal transportation goals, State of Illinois transportation goals, local 
knowledge, current local planning efforts, and input received from the public.