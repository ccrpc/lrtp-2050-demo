---
title: "Safety"
date: 2024-08-01
draft: false
weight: 30
bannerHeading: Safety
bannerText: >
  Keep everyone protected
bannerTextIcon: safety
---
### Safety Goal 
#### The metropolitan planning area transportation system will be maintained, preserved, and enhanced to safely move people and goods in the short term and to design and implement improvements to achieve the Illinois and CUUATS goal of zero deaths and disabling injuries in the long term.


### Objectives and Performance Measures

<rpc-table
url="safety_table.csv"
rpc-table>

### Strategies

<rpc-table
url="Safety_Strategies.csv"
rpc-table>

