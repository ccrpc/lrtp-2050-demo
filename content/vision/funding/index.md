---
title: "Funding"
date: 2024-08-06
draft: false  
Weight: 50
bannerHeading: Funding
bannerText: >
  The 25-year vision for the urban area transportation system is defined
  in part by how much money there is to fund the infrastructure projects
  needed to realize it.
---
[CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) 
staff establishes the 25-year vision for the Metropolitan Planning Area (MPA) transportation 
system to correspond with available funding for future infrastructure projects. The
proposed future projects in the LRTP 2050 align with collective [transportation
goals](https://ccrpc.gitlab.io/lrtp-2050/goals/) for the region, such as
reducing emissions, crashes, and the number of roads and bridges in poor
condition. This chapter will outline existing revenue sources and expenditures
for the transportation system as well as document how staff and local agencies
project revenues for the 25-year planning horizon.

Predicting future revenues is a somewhat elusive exercise, yet there are
guidelines to be followed. By federal law, the LRTP must include a financial
plan that demonstrates how the adopted transportation plan can be implemented.
In order to address this requirement and provide transparency about project
implementation, the [future transportation
projects](https://ccrpc.gitlab.io/lrtp-2050/vision/futureprojects/) listed in this plan
are labeled according to whether or not they have secured funding prior to this
plan being approved. In addition, this chapter will include information on the 
agencies and federal funding sources that directly impact the
transportation system in the Metropolitan Planning Area (MPA).

There are two basic categories for transportation spending:

1. Operations and Maintenance – keeping the existing transportation
infrastructure in good operational condition through resurfacing,
reconstruction, and repairing as periodically needed.

2. Expansion – creating new transportation infrastructure to increase access for
additional transportation modes or for all modes to additional locations.

Securing funding for expansion projects requires
increasing the operations and maintenance budget to accommodate the ongoing
upkeep of additional transportation infrastructure. To effectively choose where
and how roadways are resurfaced, upgraded, or expanded, transportation officials
must use asset management practices, such as life cycle cost analyses, to
maximize transportation funding resources, including taxpayer dollars.

## Revenues

Much of the transportation funding at the local, state, and federal level relies
on the Motor Fuel Tax (MFT) revenue system. The MFT is a user-based fee where an
excise tax is imposed on the sale of motor fuel to fund transportation
system maintenance and improvements for motor vehicles. MFT can be collected at
the federal, state, and local levels. Unfortunately, MFT is not currently
sufficient to support the operations, maintenance, and expansion of today’s
transportation system.

The 2024 federal Motor Fuel Tax (MFT) rate is 18.4 cents per gallon on gasoline 
and 24.4 cents per gallon on diesel fuel. The federal MFT hasn’t been increased 
to keep up with inflation or increasing construction costs since 1993. In the 
past two decades, motor vehicles have increased their fuel efficiency, meaning 
drivers can operate their vehicles on increasingly smaller amounts of fuel. To 
begin to address these funding shortfalls, in 2012 both the Cities of Champaign 
and Urbana imposed an additional $0.04/gallon Motor Fuel Tax at the local level. 
The City of Urbana increased theirs to $0.05/gallon in 2015. The Village of Savoy 
imposed a $0.05/gallon local Motor Fuel Tax in 2021.

Fortunately, a new state bill passed in 2019, Rebuild Illinois, which doubled the 
state MFT from 19 cents to 38 cents. The bill also introduced annual increases tied 
to inflation. As of July 2024, the state MFT rate is 47 cents for gasoline and 54.5 
cents for diesel. In 2019, the State of Illinois established a second allocation, a 
Motor Fuel Use Tax. As of January 2024, the rate is 19.6 cents per gallon of gas and 
21.2 cents per gallon of diesel. 

In addition to MFT, there are many different strategies being tested around the
country to address improve transportation infrastructure funding, including
mileage-based user fees, local sales taxes, public-private partnerships,
infrastructure banks, short-term federal discretionary grants, and additional
toll roads. It is unknown at this time if any of these revenue sources will
eventually replace MFT revenues at the local, state, and/or federal levels.

### Federal ###

On November 15, 2021, President Biden signed into law the Infrastructure Investment and Jobs Act (IIJA), also known as the [Bipartisan Infrastructure Law (BIL)](https://www.fhwa.dot.gov/bipartisan-infrastructure-law/). BIL funded $350 billion in highway programs and $108 billion for public transportation over fiscal years 2022 through 2026. The BIL expanded on the [Fixing America’s Surface Transportation Act (FAST Act)](https://www.fhwa.dot.gov/fastact/) passed in 2015 and the [Moving Ahead for Progress in the 21st Century (MAP-21)](https://www.fhwa.dot.gov/map21/) bill passed in 2012. The Federal Highway Administration (FHWA) and Federal Transit Administration (FTA) are working with local stakeholders to ensure that individual communities can both maintain and construct safe, multimodal, sustainable transportation projects ranging from passenger rail and public transportation infrastructure to bicycle and pedestrian facilities.

Federal transportation grants require different levels of local matching funds,
ranging from 10 percent to 50 percent. The BIL authorized a single amount each year for
all the apportioned highway programs combined, as dictated by the BIL.  This
included the [National Highway Performance Program (NHPP)](https://www.fhwa.dot.gov/specialfunding/nhpp/), the [Surface Transportation Block Grant Program (STBG)](https://www.fhwa.dot.gov/specialfunding/stp/) (formerly Surface Transportation Program), the [Highway Safety Improvement Program (HSIP)](https://safety.fhwa.dot.gov/hsip/), the [Railway-Highway Grade Crossings Program](https://highways.dot.gov/safety/hsip/xings/railway-highway-crossing-program-overview), the  [National Highway Freight Program (NHFP)](https://www.fhwa.dot.gov/bipartisan-infrastructure-law/nhfp.cfm), the [Congestion Mitigation and Air Quality Improvement Program (CMAQ)](https://www.fhwa.dot.gov/fastact/factsheets/cmaqfs.cfm), the  [Carbon Reductions Program](https://www.transportation.gov/priorities/climate-and-sustainability/carbon-reduction-program), the [Promoting Resilient Operations for Transformative, Efficient, and Cost-Saving Transportation (PROTECT)](https://www.transportation.gov/rural/grant-toolkit/promoting-resilient-operations-transformative-efficient-and-cost-saving) program, and the [Metropolitan Planning Program](https://www.fhwa.dot.gov/bipartisan-infrastructure-law/metro_planning.cfm). FTA apportionments include the following programs: the [Statewide and Metropolitan Planning](https://www.transit.dot.gov/regulations-and-guidance/transportation-planning/metropolitan-statewide-non-metropolitan-planning) program, the [Urbanized Area Formula](https://www.transit.dot.gov/funding/grants/urbanized-area-formula-grants-5307) program, the [Rural Area Formula](https://www.transit.dot.gov/rural-formula-grants-5311) program, and the [Rural Transportation Assistance Program](https://www.transit.dot.gov/funding/grants/rural-transportation-assistance-program-5311b3).

### State ###

On June 1, 2019, Illinois legislators passed [Rebuild
Illinois](https://www2.illinois.gov/IISNews/20266-Rebuild_Illinois_Capital_Plan.pdf),
a six-year infrastructure bill that includes $44.8 billion invested over six
years, of which $33.2 billion are dedicated to transportation across all modes.
The bill includes $10.44 billion in new state level funding that will maintain,
enhance, and upgrade Illinois’ highway network. It also includes both a transit
bonding component and a new state source recurring revenue for ongoing capital
funding totaling $4.5 billion over the life of the six-year program: $3 billion
in new bond funding and $1.5 billion in new recurring revenue for mass transit
throughout the state, of which downstate transit districts will receive $300
million in bond proceeds and $150 million in recurring revenue to maintain and
improve their systems. Rebuild Illinois also includes a $3.9 billion increase in
direct funding for roads and bridges to municipalities through state bonding
($1.5 billion) and additional MFT revenues ($2.4 billion). A portion of state
MFT revenues are distributed by the Illinois Department of Transportation (IDOT) 
to counties, townships, and municipalities for use in transportation-related 
expenditures. The MFT revenue increase from $0.19 to $0.38 in 2019 will continue 
as an ongoing source of income for the local roads system into the future.

IDOT receives funding from the state to maintain its highways and is also charged 
with distributing federal funding from the Bipartisan Infrastructure Law programs 
to the state Metropolitan Planning Organizations (MPO). The allocation for each MPO 
is based on urban area population. Most state funding allocated to projects in the 
urban area requires at least a 20 percent local match from an MPO member agency or 
outside source such as a private party. The Illinois Department of Natural Resources 
(IDNR) also provides funds for greenways and trails projects with a required local
match. Many local bike paths and trails have been partially funded by IDNR.

### Local ###

Transportation projects receive local funding through
municipal and county budgets, local MFT, public transit fares, local park
district budgets (for greenways and trails projects), the University of
Illinois, and private donations.

## Forecasting Revenues, 2025-2050

What follows is a summary of
[CUUATS’](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) 
methodology for estimating revenues for the 25-year planning horizon on a source-by-source 
basis. As with most projections about the future, uncertainty increases as the planning horizon 
moves farther out. Many factors affect transportation projects, which make the determination 
for funding complex at the federal, state, and local levels. The funding projections shown in 
this section are based on projects documented in past and current versions of the local 
Transportation Improvement Program (TIP) as well as future growth rates provided by the different 
agencies involved.

### Federal ###

Federal funding fluctuates annually by millions of dollars. In order to provide
a reasonable estimate, five different calculations were completed based on
varying program funds:

- All federal program funds, not including STBGP/STPU Local dollars and transit funding, 
were calculated for the FY 2023-28 Transportation Improvement Program (TIP). The estimated 
funding for the four years included in the LRTP 2050 (2025-2028) was $61,105,022.

- To estimate the remaining 21 years of federal funding, we typically use the average of the 
previous five years’ (FY 2019 - 2023) of federal funding allocations, not including the STBGP/STP-U 
Local and transit apportionments, as documented in the annual listing of Federally Obligated Projects. 
This year, we adjusted the methodology to use the FY 2018 – 2022 five-year average in order to exclude 
the FY 2023 funding for the I-57/I-74 interchange in the forecasts because we cannot reasonably 
anticipate such a large funding allocation again before 2050. A 4 percent annual growth rate was applied 
to the FY 2018 – 2022 average and projected to 2050, which produced an estimate for federal funding for 
2029 - 2050 of $370,558,303.

- STP-U Local funding was estimated by applying a 4 percent annual growth rate to the FY2025 allocation 
of ($2,424,666 for Champaign-Urbana and $152,592 for Mahomet) through 2050 for a total of $114,202,794.

- The Champaign-Urbana Mass Transit District estimates there will be $165,592,466 available in the next 
25 years through FTA Formula Capital funds, which are used for fleet expansion and replacement.

- The Champaign-Urbana Mass Transit District was awarded a $17,275,000 grant from the U.S. Department 
of Transportation’s Buses and Bus Facilities Grant Program for the Illinois Terminal Expansion Project 
scheduled to begin in Fiscal Year 2025.

**2025-2050 total for all Federal programs: $728,733,585**  

### State ###

The forecast for future state funding is composed of five steps: total annual state-only funds allocated 
for projects in the FY 2025-2028 TIP minus state-only transit funds, projected state-only funds available 
for FY 2029 - 2050 minus state transit funds based on the FY 2020-2024 average, total annual non state-only 
funds allocated for projects in the FY 2025-2028 TIP minus state transit funds, projected non state-only 
funds available for FY 2029 - 2050 minus state transit funds based on the FY 2020-2024 average, and total 
annual funds allocated for projects in the TIP FY 2025 – 2028 by the Illinois Department of Natural Resources. 

- Total of state-only funds allocated for projects in TIP FY 2025-2028 is $2,900,000.

- To estimate the remaining 21 years of state-only funding, the average was taken of the previous five years’
 (FY 2020 - 2024) of state-only funding allocations, not including transit funding, as documented in the annual 
 TIP documents, with an attempt to eliminate funding redundancies of projects that carryover year to year. A 4 
 percent annual growth rate was applied to the annual average and projected to 2050, which produced an estimate 
 for state funding for 2029 - 2050 of $218,106,413.

- Total non state-only funds, not including state transit funding, allocated for projects in TIP FY 2025-2028 is $13,180,000.

- To estimate the remaining 21 years of non state-only funding, the average was taken of the previous five years’ 
(FY 2020 - 2024) of non state-only funding allocations, not including transit funding, as documented in the annual 
TIP documents, with an attempt to eliminate funding redundancies of projects that carryover year to year. A 4 percent 
annual growth rate was applied to the annual average and projected to 2050, which produced an estimate for state funding 
for 2029 - 2050 of $446,240,654.

- Funding from the Illinois Department of Natural Resources is not a fixed part of the annual budget; rather, it is based 
on the approval of intermittent grant applications. The only reasonably guaranteed funding from this source is that which 
would be listed in the current TIP; however, no IDNR funding is listed in the current TIP.

**2025-2050 total for all State programs: $680,427,067**  

### Local ###

To estimate local transportation revenues from 2025-2050, each local agency provided local budget projections including 
motor fuel tax revenues estimates and future growth rates, when possible. Local budget estimates are limited to funding 
spent for transportation improvements, operations, and maintenance.

#### City of Champaign ####

The City of Champaign provided estimates for revenues through 2034 and projections that reflect an annual growth rate of 
approximately 0.8 percent between 2035 and 2050 for estimated total transportation revenues of $373,811,027.

#### City of Urbana ####

The City of Urbana provided estimates for revenues through 2026 and projections that reflect an annual overall growth rate 
of approximately 1.5 percent between 2027 and 2050 for estimated total transportation revenues of $244,423,882.

#### Village of Savoy ####

The Village of Savoy provided an estimate for revenues through 2050 that includes an annual growth rate of 0.5 percent 
between 2028 and 2050 for estimated total transportation revenues of $25,199,639.

#### Village of Mahomet ####

The only reasonably guaranteed transportation funding for the Village of Mahomet is through Motor Fuel Tax revenues. 
Estimated revenues through 2050 include an annual 1% growth rate for estimated total transportation revenues of $12,264,057.

#### Village of Tolono ####

The only reasonably guaranteed transportation funding for the Village of Tolono is through Motor Fuel Tax revenues. 
Estimated revenues through 2050 include an annual 1% growth rate for estimated total transportation revenues of $4,685,146.

#### Village of Bondville ####

The only reasonably guaranteed transportation funding for the Village of Bondville is through Motor Fuel Tax revenues. 
Estimated revenues through 2050 include an annual 1% growth rate for estimated total transportation revenues of $504,395.

#### Champaign County ####

Champaign County contributions within the MPA are on a project-by-project basis. Only those projects that have established 
agreements with the County for MPA transportation projects, as documented in the FY 2025-2028 TIP can be considered reasonably 
guaranteed. At this time, estimated funding from the County through 2050 is $862,500.

#### MTD ####

The Champaign-Urbana Mass Transit District (MTD) receives an allocation from the state and
uses its local revenues to cover operations and maintenance for its system. MTD provided
projections for local and state operating revenues through 2050 that reflect an annual growth
rate of approximately 4 percent for an estimated total of $2,540,640,000.

#### University ####

The University of Illinois has no reasonably guaranteed funds for transportation improvements or maintenance with the exception 
of what is listed in the FY 2025-2028 TIP. At this time, estimated transportation funding from the University through 2050 is $5,600,000.

#### Willard Airport ####

The University-owned Willard Airport in Savoy reported $4,771,750 in revenues for FY 2023. With highly fluctuating revenues over 
the last few years, CUUATS decided to use the average of FY 2022 and FY 2023 revenues and apply that without a rate increase through 
2050. $4,258,197 was assumed to be the annual budget each year from 2025 through 2050 for a total of $110,713,109.

#### Private Sources ####

The only reasonably guaranteed private transportation funds are found in the FY 2025-2028 TIP, where there is a project to expand 
the MTD Illinois Terminal that is partially funded with private funds totaling $50,000,000.

**2025-2050 total for all local programs: $3,368,703,755**  


## Forecasting Expenditures, 2025-2050

To operate, maintain, and expand our local area transportation network, local
agencies project they will spend all future revenues through 2050. The following 
table reflects our best estimate of revenues and expenditures through 2050.

### Summary of Revenue and Expenditure Projections, 2025-2050  

<rpc-table url="rev_exp_25-50.csv"
  text-alignment="c,c"></rpc-table>


## Financial Strategies to Fund Projects

Funding transportation projects can be a complex planning process that involves multiple agencies at federal, state, regional, and local levels, institutions, and private investors. Some projects wait decades before funding can be allocated, making the project more expensive as time passes. The following are some types of funding mechanisms that can be used individually or combined to support operations, maintenance, and improvements to the transportation system. Identifying needs and regionally significant projects in the LRTP gives local agencies time to proactively prioritize and find funding for those projects. 

### Federal grants

Federal grants can cover 50 to 100% of transportation project costs. Federal grant availability varies year to year and supports a variety of topics that reflect changing federal priorities over time. Grant offered during the current Bipartisan Infrastructure Law have focused on transportation initiatives that address climate change, resiliency, sustainability, economic opportunity, disadvantaged communities, new transportation technologies, and transportation modes such as bicycling and walking. 

### State grants

State grants are relatively dependable opportunities to fund transportation projects and planning processes and can cover 80 to 100% of project costs. Most opportunities come through the Illinois Department of Transportation and Illinois Department of Natural Resources. 

### User charges and fees

User charges and fees can be utilized to recoup some of the investments made to maintain or improve upon transportation facilities and infrastructure. Examples include Motor Fuel Tax, vehicle license and registration fees, transit fares, and parking fees. 

### Loans

Loans are available at federal, state and local levels to fund transportation projects. 
- Federal – Through the USDOT Build America Bureau, “the Transportation Infrastructure Finance and Innovation Act [(TIFIA)](https://www.transportation.gov/buildamerica/financing/tifia) program provides credit assistance for qualified projects of regional and national significance.” Credit assistance can be in the form of direct loans, loan guarantees, and standby lines of credit. TIFIA can provide credit for up to 49% of  funding gaps in local infrastructure projects valued at $10 million or more. 
- State – The IDOT Rail Freight Loan Program provides capital assistance to communities, railroads and shippers to preserve and improve rail freight service in Illinois. The USDA Rural Development Community Facilities Direct Loan and Grant Program provides loans and grants to communities with a population of no more than 20,000 residents, and public facilities such as street improvements are eligible. Currently awaiting the Governor’s signature is Senate Bill 3597, which would allow the Illinois Finance Authority Climate Bank to offer loans for clean energy infrastructure projects. 
- Local – Private banks can also extend loans to local agencies for infrastructure improvements. 

### Municipal bonds

Municipal bonds are issued by state and local governments and purchased by investors. Municipalities borrow capital from investors in the form of bonds, and repay the investments with interest, which is often tax-free. Capital from the bond sales can be used for infrastructure improvements. Green bonds or sustainability bonds are municipal bonds that must demonstrate how the financing will support environmental or clean energy projects.

### Tax Increment Financing (TIF) Districts

Units of local government can designate areas within their jurisdiction as TIF districts, and sales and property tax revenues within these Districts can be used to finance public infrastructure improvements. 

### Tax increases

Non home-rule municipalities in Illinois can place a measure on the ballot to have a percentage increase in sales or property taxes to fund local infrastructure improvements. Home rule municipalities can approve tax increases by local ordinance.

### Capital reserves and fund balances

Local governments often save money in capital reserve funds over time to pay for capital improvement projects or for emergency maintenance on transportation infrastructure. 

### Earmarks 

Also known as Congressionally Directed Spending (CDS), earmarks can provide one-time grants for community improvements. Some administrations do not allow earmarks. The practice was reintroduced in federal fiscal year 2022 and whether to allow earmarks is revisited annually.  

### Public-private partnerships

Public-private partnerships unite private businesses and philanthropists with public federal, state, and local funding opportunities to bring a project to fruition. A current example is the MTD’s Illinois Terminal proposed expansion, which includes federal, local, and private funding.

### In-kind contributions

In-kind contributions are non-monetary commitments that local governments and agencies make that are assigned a monetary value that goes toward finishing a transportation project or planning process. They are sometimes allowed to help defray local match requirements for a federal or state grant. For example, a municipality might offer to cover all copying and supplies needed for a project as an in-kind contribution. 