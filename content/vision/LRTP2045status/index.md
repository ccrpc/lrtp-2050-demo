---
title: "LRTP 2045 Status"
date: 2024-08-12
draft: false
Weight: 20
bannerHeading: LRTP 2045 Status
bannerText: >
  Our member agencies and staff have been working hard on LRTP 2045 implementation since the plan was approved in 2019.
---

## LRTP 2045 Regionally Significant Projects Status

The following regionally significant projects were highlighted in the LRTP 2045. While progress has been made, there is still work to do to bring these projects to fruition.

<rpc-table 
url="2045projects-status.csv"
rpc-table>

## LRTP 2045 Goal Strategies Status

For each of the LRTP 2045 goals, the tables below indicate the status of each 
strategy identified in 2019 to help achieve a better transportation system in the 
Champaign-Urbana Metropolitan Planning Area.

### LRTP 2045 Goal: Safety

#### The metropolitan planning area will maintain, preserve, and operate its existing transportation system to provide safe, efficient, and reliable movement of people, goods, and services in the short term, and in the long term, achieve the state’s goal of zero deaths and disabling injuries.

{{<image src="2045 Strategies - Safety.png"
  alt="Status of LRTP 2045 Safety goal strategies."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### LRTP 2045 Goal: Environment

#### The metropolitan planning area will reduce the transportation system’s significant contribution to greenhouse gas emissions and environmental degradation to maintain a high quality of human and environmental health in the region.

{{<image src="2045 Strategies - Environment.png"
  alt="Status of LRTP 2045 Environment goal strategies."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}} 

### LRTP 2045 Goal: Equity

#### The metropolitan planning area will aim to provide safe, affordable, and efficient transportation choices to all people to support a high quality of life in every part of the community.

{{<image src="2045 Strategies - Equity.png"
  alt="Status of LRTP 2045 Equity goal strategies."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}} 

### LRTP 2045 Goal: Economy

#### The metropolitan planning area will maximize the efficiency of the local transportation network for area employers, employees, materials, products, and clients at the local, regional, national, and global levels and facilitate strong inter-regional collaborations to realize large-scale transportation improvements.

{{<image src="2045 Strategies - Economy.png"
  alt="Status of LRTP 2045 Economy goal strategies."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### LRTP 2045 Goal: Multimodal Connectivity

#### The Champaign-Urbana area will increase accessibility, connectivity, and mobility of people and freight to all areas of the region by maintaining a multimodal system of transportation that is cost-effective for people, businesses, and institutions and will allow freedom of choice in all modes of transportation including active modes whenever possible.

{{<image src="2045 Strategies - Multimodal Connectivity.png"
  alt="Status of LRTP 2045 Multimodal Connectivity goal strategies."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}