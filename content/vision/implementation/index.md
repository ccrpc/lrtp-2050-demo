---
title: "Implementation"
date: 2024-07-19
draft: false 
Weight: 60
bannerHeading: Implementation
bannerText: >
  Implementing the LRTP is not only about completing the projects identified
  in the plan but also taking steps to incorporate the broader goals,
  objectives, and strategies.
---

The LRTP 2050 Vision is about more than just transportation. Public input
reinforces the idea that the transportation network is intricately tied to many
other conditions in the community including land-use, public health, the
environment, and the economy. The overall built environment operates most
effectively when all these different processes work together to facilitate
safe and efficient mobility to support our individual and collective
goals.

Plan implementation involves certain standard routine tasks that can be
considered on two levels: project-related implementation, and concept-related
implementation.

## Project-Related Tasks

### Continually seek and integrate public input

Every planning process CUUATS undertakes offers numerous opportunities for residents to share their ideas, needs, and priorities. The [LRTP 2050 Public Involvement process](https://ccrpc.gitlab.io/lrtp-2050/process/) included three rounds of outreach during the two-year planning process. During Round 2 (April through July 2024), residents identified their priority road, pedestrian, and bicycle improvement projects from a list created with input from CUUATS member agencies. The map below shows survey respondents’ top two road projects and top two bicycle/pedestrian projects for each time frame:

{{<image src="top_survey_projects_map.png"
  alt="Map of LRTP 2050 Round 2 Survey results showing the top road, pedestrian, and bicycle projects identified by survey respondents."
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

The survey respondents’ high priority projects generally reflect support for road projects improving connectivity and safety within the core of the communities rather than on the edges of the urban area. Their selections also reflect a desire to increase ease of travel in areas where population has grown significantly (southwest Champaign) and in high-traffic activity areas (North Prospect Avenue, North Lincoln Avenue). For the bicycle and pedestrian projects, there is more of a focus on the regional connections (Kickapoo Rail Trail and next to US45 between Champaign and Savoy) than smaller projects.

Even though the next 25 years of transportation improvements may not create all the road, bicycle, and pedestrian improvements the community would like to see, we will still share the survey results with all CUUATS member agencies. It’s possible that the survey results will prompt new conversations and change which projects each community prioritizes.

### Construction

The most tangible aspect of implementation is the construction of transportation improvements in our community. CUUATS has identified the following regionally significant projects that have funding secured for construction prior to 2030:

- <a href="1_Curtis_ProspectToFirst.png" target="_blank" >Curtis Road improvements from Prospect Avenue to First Street</a>
- <a href="2_US150-Prospect_MarketviewToSpringfield.png" target="_blank" >US150/Prospect Avenue improvements from Marketview Drive to Springfield Avenue</a>
- <a href="3_Neil_SpringfieldToCurtis.png" target="_blank" >Neil Street improvements from Springfield Avenue to Curtis Road</a>
- <a href="4_Kirby_DuncanToStaley.png" target="_blank" >Kirby Avenue bridge over I-57 and adjacent improvements</a>
- <a href="6_Mattis_CurtisToWindsor.png" target="_blank" >Mattis Avenue improvements from Curtis Road to Windsor Road</a>
- <a href="8_Lincoln_WascherToKillarney.png" target="_blank" >Lincoln Avenue improvements from Wascher Drive to Killarney Drive</a>
- <a href="9_Florida_WrightToHillcrest.png" target="_blank" >Florida Avenue improvements from Wright Street to Hillcrest Drive</a>
- <a href="10_Vine_MainToCalifornia.png" target="_blank" >Vine Street improvements from Main Street to California Avenue</a>


### Review Project Priorities Periodically

Projects in the LRTP have undergone an initial prioritization process; this
prioritization should be reviewed periodically to include new projects and
change priorities if new funding or information becomes available. Two existing
documents can serve this purpose at the regional level: the [Transportation
Improvement Program (TIP)](https://ccrpc.org/transportation/transportation_improvement_plan_(tip)/transportation_improvement_program_(tip)_fy_2023-2028.php) and the
[Project Priority Review Guidelines (PPR)](https://ccrpc.org/transportation/project_priority_review_ppr_guidelines.php).

The TIP is updated in full every three years and lists transportation projects anticipated to
be constructed in the metropolitan planning area during the next six years. The
program reflects the goals, objectives, and recommendations from the 25-year
Long Range Transportation Plan in the short term. By mandate, the TIP must
include transportation projects receiving federal and state funding; the
[CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) program also includes local
projects from our member agencies. Compiling the document requires a consensus
among all [CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) agencies, thus
establishing compliance with federal laws and eligibility for federal funding.

The [PPR Guidelines](https://ccrpc.org/transportation/project_priority_review_ppr_guidelines.php)
outline the process by which [CUUATS](https://ccrpc.org/divisions/planning_and_development/transportation/index.php) evaluates and documents consistency between the local use of federal Surface Transportation Block Grant
Program (STBGP) funds and federal and regional transportation goals as
established in the LRTP. Awarding STBGP funding is overseen by CUUATS. Local agencies seeking to use STBGP funds are required
to submit a project application to be reviewed by CUUATS staff and the Project
Priority Review working group who use a set of criteria based on federal and
regional transportation goals to score each project. The project scores are
intended to illustrate the level of consistency with federal and regional
transportation goals and also serve as a way to measure projects against one
another in the event that multiple projects apply for STBGP funding in the same
year.

### Seek New Funding Resources

Local agencies should continually seek new funding sources for those projects
that do not currently have funding. When the new federal or state transportation
bill is enacted, local agencies should determine potential funding sources and
pursue projects that fit the intent of the new bill.

## Concept-Related Tasks

Complete Benchmarks from Plan Performance Measures are based on the LRTP goals
and objectives as well as the project lists; they are measurable events that
signify progress and/or lack of progress toward defined goals. Every effort
should be made to update data sets related to the Performance Measures and
produce the LRTP Report Card each year.

### Update and Create New Policies and Strategies

In the next five years, CUUATS staff anticipate updating several existing plans and policies, and also creating new planning documents that support implementation of the LRTP 2050. 

#### Documents to be updated

- [CUUATS Project Priority Review Guidelines](https://ccrpc.org/transportation/project_priority_review_ppr_guidelines.php) for spending STPU/STBGP funds will be updated to reflect the [LRTP 2050 goals](https://ccrpc.gitlab.io/lrtp-2050/goals/) and any federal requirements established since the last PPR Guidelines were approved in 2022.

- The [Champaign-Urbana Urban Area Safety Plan](https://ccrpc.org/transportation/champaign-urbana_urban_area_safety_plan/index.php) and the [Rural Champaign County Area Safety Plan](https://ccrpc.org/transportation/rural_champaign_county_area_safety_plan/index.php) will be updated to reflect the findings from the Safe Streets for All grant project that CUUATS will begin in 2025.

#### New documents 
- Making progress on implementing the LRTP 2050 [Sustainability goal](https://ccrpc.gitlab.io/lrtp-2050/goals/sustainability/) will include creating climate change related policies and strategies identified during the development of the "Champaign County Regional Transportation System Vulnerability Assessment Under Climate Change" project made possible from a federal PROTECT grant. This planning process will begin in 2025.

- The LRTP 2050 [Equity and Quality of Life goal](https://ccrpc.gitlab.io/lrtp-2050/goals/equity_quality-of-life/) will progress by creating policies and strategies to address the impact of housing and transportation costs during the development of the "Housing and Transportation Affordability and Accessibility Index" project that was made possible by a grant from the IDOT Statewide Planning and Research Program. CUUATS staff started this project in early 2024 and expect it to be complete by the end of 2025.

- All five [LRTP 2050 goals](https://ccrpc.gitlab.io/lrtp-2050/goals/) will be addressed by identifying policies and strategies identified during the development of the CUUATS Active Transportation Plan that was made possible by a grant from the IDOT Statewide Planning and Research Program. This planning process will begin in 2025.

### Monitor Area Development

While the LRTP is based on local agencies’ most available knowledge at the time
of writing, many new developments may occur while other anticipated developments
may not occur on time or at all. Decision making processes and future updates to
the plan should reflect these changes.

### Evaluate Change

Proactive planning should be utilized to anticipate changes to the
transportation network. Where unanticipated changes occur, local agencies should
assess how unanticipated changes will affect the transportation system, and
react to those changes logically and efficiently.

### Update Models

The [CUUATS Modeling Suite](https://ccrpc.gitlab.io/lrtp2045/data/models/) must
be updated as continually as staff resources and funding permit. Updated traffic volumes,
population forecasts, employment forecasts, and other inputs are essential
elements to have accurate working models. Seek Funding for Implementation Some
of the goals and objectives will require significant staff time or other inputs
to be completed. Funding must be sought for both transportation projects and
concepts implementation.

## Obstacles to Implementation

Bringing the projects and concepts in this plan to fruition will require public officials, local government staff, and residents to seek a fresh perspective on how our community functions and how they want it to function in the future. If the transportation network fails to evolve to accommodate changing social, economic, and environmental trends, our community will experience longer travel times, more roadways with congestion, fewer opportunities to promote alternative transportation modes, and the inability to realize the goals set forth in this plan. To realize these goals, there are numerous obstacles to overcome.

### Safety Obstacles

- Human Behavior: Despite improvements in infrastructure and vehicle safety technologies, changing driver behavior remains a significant challenge. Human error, including distracted driving, impaired driving, and non-compliance with traffic laws, continues to be a leading cause of road fatalities.

- High Population Turnover: The high turnover rate of the student population in the Champaign-Urbana area makes it difficult to maintain consistent safety education and awareness efforts. New residents and students may not be familiar with local traffic laws and safe driving practices.

- Public Awareness and Engagement: Increasing public awareness and engagement regarding road safety can be challenging. Effective communication strategies are needed to educate the public about safe driving practices and the importance of adhering to traffic laws.

- Technological Integration: Integrating new safety technologies, such as advanced driver-assistance systems (ADAS) and vehicle-to-everything (V2X) communication, requires substantial investment and collaboration between various stakeholders, including government agencies, vehicle manufacturers, and technology providers.

### Reliability Obstacles

- Aging Infrastructure: Many transportation systems rely on aging infrastructure, which is prone to more frequent mechanical failures. Upgrading or replacing older vehicles and equipment requires significant investment and coordination across multiple agencies.

- Funding Constraints: Securing consistent and sufficient funding for regular maintenance and upgrades can be challenging. Budget limitations often lead to deferred maintenance, which can increase the likelihood of mechanical failures over time.

- Infrastructure Gaps: Existing infrastructure may not be well-connected or safe for non-motorized users, necessitating substantial upgrades and new construction. Gaps in sidewalks, bike lanes, and safe crossings can limit accessibility and deter people from using these modes of transportation.

- Land Use and Development Patterns: Dispersed land use patterns and low-density development can make it difficult to achieve significant improvements in access scores. Higher density, mixed-use development is needed to support non-motorized transportation options, but such changes require long-term planning and policy shifts.

### Sustainability Obstacles

- Public Engagement: Effective public engagement and education are essential to promote behaviors that reduce emissions, such as using public transportation, carpooling, and adopting electric vehicles. However, changing public behavior and increasing awareness about ozone levels and their health impacts can be challenging.

- Technological Implementation: Implementing and maintaining advanced monitoring and control technologies for emissions can be costly and require continuous updates and investments. Ensuring these technologies are accessible and effective across all regions, including disadvantaged communities, adds to the complexity.

- Infrastructure Development: Developing the necessary infrastructure for alternative fueling and charging stations requires substantial investment and strategic planning. Ensuring these stations are conveniently located and accessible to all communities is a logistical challenge.

- Climate Change Impacts: The transportation sector is both a major contributor to and heavily impacted by climate change. Rising temperatures, extreme weather events, and other climate-related issues can damage infrastructure and disrupt transportation systems, necessitating robust adaptation and resilience strategies. CUUATS received a PROTECT grant to develop a vulnerability assessment of the area transportation system based on climate change; this project will begin in 2025.

### Equity and Quality of Life Obstacles

- Equity in travel options: Access to employment can be hindered by a lack of travel options and high commute times. Ensuring shorter commute times in public transit and providing access to travel alternatives (e-bikes, ride sharing, etc.) can create more opportunities to improve quality of life for all users.

- Funding and Resource Allocation: Securing consistent funding for discounted bus passes can be challenging, especially when dependent on grants and donations. Fluctuations in funding availability can impact the ability to maintain or increase the number of passes distributed annually.

- Data Collection and Analysis: Accurately identifying and addressing equity issues requires comprehensive data collection and analysis. However, there are often challenges in obtaining high-quality, disaggregated data that reflects the experiences of different demographic groups. This can hinder the ability to monitor progress and make informed decisions.

### Connectivity Obstacles

- Coordination and Collaboration: Effective sidewalk improvements require coordination between multiple stakeholders, including local governments, private property owners, and utility companies. Achieving alignment on project goals, timelines, and funding responsibilities can be complex and time-consuming.

- Space Constraints: Urban areas often have limited space available for new bike facilities. Integrating bike lanes into existing roadways may require significant redesigning and can lead to conflicts with other road users, including motorists and pedestrians.

- Maintenance and Upkeep: Ensuring that new and existing connectivity infrastructure, such as bike lanes and sidewalks, is well-maintained is crucial for long-term success. Regular maintenance requires ongoing funding and effective management practices.

- Funding:  Funding for pedestrian and bicycle projects tend to lose priority behind roadway projects. In addition, adding active transportation mode facilities to the existing roadway network, such as public transit stops, bike facilities, and sidewalks, are mostly limited to grant funding, which is not a reliable funding source. These facilities should be added where possible during new construction or the reconstruction of existing roadways to create Complete Streets in accordance with the CUUATS Complete Streets Policy.