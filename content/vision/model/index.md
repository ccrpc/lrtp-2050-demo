---
title: "Scenario Modeling"
date: 2024-11-15
draft: false
Weight: 40
bannerHeading: Scenario Modeling
bannerText: >
  The modeling process, which considers the existing transportation, land use, safety, and environmental conditions of the region, helps determine what planning actions are most supportive of the goals and objectives identified for the planning horizon.
---

The combined input about the current and future transportation system conveyed strong public support for a set of overlapping ideas about the future of transportation: a more environmentally sustainable transportation system, additional pedestrian and bicycle infrastructure, shorter off-campus transit times, equitable access to transportation services, and a compact urban area that supports active transportation and limits sprawl development. 

CUUATS staff worked with the members of the LRTP 2050 Steering Committee developing three scenarios for 2050. Because of the complementary nature of these ideas, CUUATS staff, with the help of the LRTP 2050 Steering Committee, selected one preferred scenario for 2050 that incorporated these overarching ideas summarized by the LRTP 2050 goals, as well as emerging trends such as teleworking, electric vehicles, and connected automated vehicles. The following are some trends and external factors considered in the scenario modeling approach.

-	**Bicycle and pedestrian infrastructure:** 100 percent of recommended pedestrian and bicycle projects from local plans were incorporated into the preferred scenario for 2050. Although no local agency could feasibly commit to implementing all currently recommended pedestrian and bicycle projects in their respective jurisdictions over the next 25 years, the small scale and large volume of these projects made it time-prohibitive to select which projects should be excluded.

-	**A more environmentally-sustainable transportation system:** Staff used increased rates of active transportation as well as industry projections for the increase of electric vehicles to reduce vehicle emissions in the region.  

-	**Limited sprawl:** To discourage additional sprawl development and maintain short commute distances to services, restrictions were placed on peripheral
 	development and incentives were assumed for infill development. In addition, the selection of projects modeled for the future emphasize existing  	transportation network maintenance over new roadway construction.

- **Electric Vehicles (EVs):** The current EV adoption rate is less than one percent (of the total fleet) in Champaign County, but it is expected to increase significantly according to projections by the Illinois Department of Transportation (IDOT) and overall national trends. Based on industry projections and suggestions from the LRTP steering committee, CUUATS staff used progressively higher EV adoption rates for various scenarios.  

-	**Automated Vehicles (AVs):** CUUATS staff used industry projections as well as local research to estimate the timing and framework of future AV integration. 
 As a smaller urban area, staff used conservative estimates for when AV technology might start to arrive in the region.

-	**High Speed Rail:** Being able to travel to Chicago via high speed rail in 45 minutes has been a goal of residents for many years, as documented in the LRTP 2050 public input as well as the two previous LRTPs for 2045 and 2040. The technology is not new, but the expense is well beyond the scope of local agencies, and it's hard to predict how and when it would be possible to obtain that kind of funding. Due to this uncertainty, it was determined by the LRTP Steering Committee and CUUATS staff that it will be modeled in the most ambitious scenario of the LRTP 2050.

The three separate future scenarios modeled for the Metropolitan Planning Area (MPA) and compared with a 2020 baseline are briefly defined in the table below.

|Scenario Name|Scenario Description|
|---|---|
|2020 Baseline|Based on 2020 data, intended to reflect current conditions.|
|2050 Business-as-Usual Scenario|Forecasts how and where development will occur between 2020 and 2050 based on historic development trends, relatively certain future development projects and transportation system changes, as well as conservative integration of electric and autonomous vehicles.|
|2050 Optimistic Scenario|Designed to incorporate relatively certain future developments and transportation system changes as well as Federal, State, and local goals as summarized in the Goals, Future Conditions, and Future Projects sections. This scenario reflects ambitious implementation of bike and pedestrian recommendations in current plans, projected transit system changes, future environmental considerations and actions, and an emphasis on infill (over peripheral or sprawl) development. **It is the preferred scenario for the LRTP 2050.**|
|2050 Transformative Scenario|Incorporates the construction of a high speed rail (HSR) line to Chicago by the year 2045 and resulting growth in the core area. This scenario reflects more access to technology, i.e., more teleworking and a higher adoption rate of electric and automated vehicles.|

## LRTP 2050 Preferred Scenario

The 'Optimistic Scenario' was selected as the Preferred Scenario for the LRTP 2050 due to strong public support for a transportation system that prioritizes compact urban development, enhanced pedestrian and bicycle infrastructure, reduced transit times, equitable access, and environmental sustainability. This scenario aligns closely with the overarching goals of the LRTP 2050, including the integration of emerging trends like teleworking, electric vehicles, and connected automated vehicles.

Emphasizing the implementation of current plans and focusing on infill development, the Optimistic Scenario represents the most aligned vision for the region's future as supported by the community and the LRTP 2050 Steering Committee. Consequently, it was chosen to guide transportation planning and development through 2050.

## CUUATS Modeling Suite

The CUUATS modeling suite is designed to provide a holistic approach to planning analysis through the integration of localized transportation, land use, emissions, safety, accessibility, and mobility data. Each model addresses a specific area of concern at the necessary level of detail to make it appropriate for Champaign County or the metropolitan planning area. The synergy of the different models allows CUUATS staff to assess how different population changes and development patterns will impact the transportation system in the future. By quantifying the various impacts of potential transportation system changes, planners are able to compare different future development scenarios as well as develop individual performance measures. While all transportation improvements require some combination of labor, materials, and expertise to carry out, some desired improvements are more straightforward to measure and model than others. 
For instance, completing gaps in the bicycle network or improving curb ramps to improve multimodal connectivity can be counted, measured, and tracked as improvements are completed over time. Other desired improvements, like limiting sprawl development or reducing greenhouse gas emissions to improve environmental health, rely on a number of additional factors and future unknowns that are not as clearly measurable and are therefore more difficult to model for the future.

Two models serve as the foundation of the CUUATS modeling suite: a land use model and a travel demand model (TDM). The land use model projects population, employment, and growth areas into the future while the TDM estimates the number and location of auto and transit in the future. The TDM is integrated with the land use model to identify the relationships between land use changes and travel patterns in the region. Three additional models, MOVES, Access Score, and the Safety Forecasting Tool, use the outputs from the first two models and other external data to project vehicle emissions, transportation network accessibility for all modes, and the likelihood of crashes at different locations within the area. Models can't *predict* the future, but they can help us *imagine* the future and try to understand how our actions today could impact our transportation system down the road. Countless variables will determine the health and relative success of the region over the next 25 years. While the LRTP 2050 models and projections were carefully designed and validated whenever possible, 
they are not perfect reflections of the social and physical processes in play, and the input data used is imperfect. For more information about the different models and related inputs utilized in the development of the LRTP 2050 scenarios, see the [Appendices](https://ccrpc.gitlab.io/lrtp-2050/appendices/).

#### CUUATS LRTP 2050 Modeling Suite
{{<image src="CUUATS_modeling_suite_LRTP2050.jpg"
  alt="Diagram showing the types of software used to develop the future vision. CUUATS staff started with the Travel Demand Model and the Land Use Model. These two programs analyze the vehicle miles traveled, mode choice, traffic volume by road link, congestion speed, population projections, employment projections, and areas of future growth. From there, the data collect from the Travel Demand Model and Land Use Model are used in three other programs: Motor Vehicle Emissions Simulator (MOVES), Neighborhood Level Accessibility Analysis (Access Score), and the Safety Forecasting Tool. MOVES calculates GHG emissions, urban/rural emissions, and PM 25 and other emissions. Access Score measures the level of traffic stress, by transportation mode, accessibility score by transportation mode, and the accessibility scores by destination type. The Safety Forecasting Tool measures crash risk by risk levels and by segment."
  caption="Diagram of the modeling process used by CUUATS staff to develop the Long-Range Transportation Plan 2050 vision. Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### Land Use Modeling

Effective coordination of land use development and transportation planning can produce policies and strategies that preserve and enhance valued natural and cultural resources and facilitate healthy, sustainable communities and neighborhoods.

CUUATS uses the in-house Land Use Inventory and UrbanSim's open-access Python library to develop the future population and employment projections for different development scenarios. In 2020, CUUATS received funding to develop a consistent, accurate database of existing land use data for use in modeling. A web-based tool was also developed so that local agencies could update the database seamlessly. Development of the inventory was completed in 2022 and has been regularly updated since then. It has detailed and up-to-date information on the existing buildings (footprint, story, units, uses) and parcels (footprint, uses) in the C-U area. This data was one of the primary building blocks of the CUUATS Land Use Model. Since the database is editable, the web tool can only be accessed with proper authentication. CUUATS can provide more information on the inventory if requested.

In 2020, CUUATS staff also developed a custom, parcel-based land use model using open-source tools developed by UrbanSim. UrbanSim is an open-source microsimulation system designed to support the regional transportation planning process by forecasting future scenarios based on the interactions of individual persons and households. The modeling system employs a data-driven methodology to capture the interdependencies within dynamic urban systems. It consists of interconnected models that allow planning agencies to analyze anticipated and possible future changes in land use and development patterns, regulations, and growth rates over a designated planning horizon. The outputs forecasted by the model can vary based on the wide array of inputs. To learn more about the inputs used in the land use model, visit the [CUUATS land use model site on GitLab](https://gitlab.com/ccrpc/land-use-model/-/wikis/home). For more about UrbanSim and its core models, visit the [UrbanSim documentation site](https://udst.github.io/urbansim/).

CUUATS employed a parcel-level analysis to evaluate the future scenarios from the base year, 2020, to the horizon year, 2050. The model relies on a set of core tables to create these predictions, including the parcel geometry, residential units, households, persons, and jobs. The households and persons tables are generated using a population synthesizer, which uses census block-group and census-tract level American Community Survey (ACS) estimates to distribute households and individual people to the parcels. A hybrid approach was adopted to develop the 2020 base year data for the Champaign-Urbana area, particularly for population and household estimates. The 2020 Decennial Census data for this region showed some unusual population counts, likely due to undercounts. This discrepancy is attributed to the absence of many students and faculty from the University of Illinois at Urbana-Champaign during 2020 as a result of online classes and other travel restrictions. To address this issue, the approach combined data from the 2019 American Community Survey (ACS), the 2020 Census, 
previous LRTP 2045 estimations, and input from the LRTP 2050 steering committee, which included valuable local knowledge, to develop accurate population and household data for the area. CUUATS utilized a combination of sources for baseline employment data, including the Census Longitudinal Employer Household Dynamics (LEHD) data, Lightcast business data, Bureau of Labor Statistics (BLS) employment data, and ACS employment estimates. This data was then further processed and refined, incorporating additional research and local knowledge to ensure accuracy and relevance for the region. 

The land use model was used to analyze and predict population, households, employment by industry, residential units, and future growth areas for each scenario. Population and employment projections are especially critical in visualizing future regional characteristics.

<rpc-chart
url="projected-population-in-champaign-county.csv"
type="line"
switch="false"
columns="1,2,3,4"
rows="1,2,3,4,5,6,7,8"
x-label="Population"
legend-position="top"
legend="true"
grid-lines="true"
source="CUUATS Land Use Model"
chart-title="Projected Population in Champaign County"></rpc-chart>

<rpc-chart
url="projected-employment-in-champaign-county.csv"
type="line"
switch="false"
columns="1,2,3,4"
rows="1,2,3,4,5,6,7,8"
x-label="Employment Projection"
legend-position="top"
legend="true"
grid-lines="true"
source=" CUUATS Land Use Model"
chart-title="Projected Employment in Champaign County"></rpc-chart>

The 2050 Business-as-Usual Scenario uses the historical growth trend to project future population and employment in the county. The 2050 Optimistic Scenario assumes more infill development and anticipates higher growth in both employment and population. It considered the local comprehensive plans and zoning data to identify future growth areas, and additional growths were assumed in those areas. Additionally, input from agency members was incorporated to refine and guide the growth area projections. Greater population and employment growth are projected for the 2050 Transformative Scenario, primarily induced by the introduction of High Speed Rail in the area by 2045. Based on the Midwest High Speed Rail Association Network Benefits Study and the University of Illinois HSR Feasibility 
Study, the scenario assumes that new households and jobs would be attracted to the area following the completion of the HSR project. These increases can be seen in the population and employment projections table above.    

The following two maps highlight the population and employment growth areas (i.e., TAZs) in the Metropolitan Planning Area. The model defines an increase of less than 100 people in a TAZ between 2020 and 2050 as a 'Minor Growth' and greater than 250 people as 'High Growth.'   

{{<image src="population_growth_areas.png"
alt="Map highlights Census blocks projected to see population growth by 2050."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

{{<image src="employment_growth_areas.png"
alt="Map highlights Census blocks projected to see employment growth by 2050."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

The Optimistic Scenario assumes that the growth will be focused on the core urban area with less fringe development. The Transformative scenario assumes even higher growth of population and employment in the core urban area, especially near the Illinois Terminal area, and moderate development at the edges of the urban area. The following map shows the TAZs where higher growth is estimated for the Transformative Scenario compared to the Optimistic Scenario.

{{<image src="transformative_optimistic_pop_diff.png"
alt="Map highlights the TAZs where higher growth is estimated for the Transformative Scenario compared to the Optimistic Scenario."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

The output from the land use model is linked with the other tools in the CUUATS modeling suite. It has the strongest connection with the Travel Demand Model. The distributed population, employment, and household values from the model create the socioeconomic inputs needed to run the TDM.

### Travel Demand Model (TDM)

CUUATS built the first travel demand model (TDM) for the Champaign-Urbana urban area in 2003, which was later expanded to include all of Champaign County. The current Champaign County TDM is the foundation of the CUUATS modeling suite, producing fundamental information needed to plan for the future transportation system such as the current and projected transportation demand of persons and goods in Champaign County. Since 2003, CUUATS has been maintaining, documenting, and expanding the TDM capabilities to serve the planning needs of the region and to support and advance travel demand modeling initiatives across
the state.

The Champaign County travel demand model (TDM) is a person trip model built using the Cube Voyager software platform. The TDM employs the four-step travel forecasting process to evaluate auto and transit trips for both daily and peak hour scenarios. The countywide travel demand model is integrated with the land use model to identify the relationships between land use changes and travel patterns in the region. Due to the unique relationship between the TDM and land use 
model, multiple horizon year forecasts can be evaluated. The base year for the model is 2020.

To forecast trips and roadway volumes for each of the future scenarios, the TDM relies on a set of core inputs, including population and employment projections generated from  land use model, future roadway network and transit service projections identified with state and local roadway agencies, and other variables including overall vehicle stock and fuel economy. The CUUATS TDM also considers the advent of connected and/or autonomous vehicles (C/AV) and their impact on regional travel patterns and transportation infrastructure, detailed in the Data and Models section.

The TDM models trips for all purposes, including work, school, shopping, and other trips. The following two figures report the TDM's vehicle miles traveled (VMT) and mode share projections for the 2020 Baseline and the three 2050 scenarios.

{{<image src="VMT_4scenarios.png"
alt="Chart shows the TDM's vehicle miles traveled (VMT) for the 2020 Baseline and the three 2050 scenarios."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

{{<image src="Mode_share_4scenarios.png"
alt="Chart shows the TDM's mode share for the 2020 Baseline and the three 2050 scenarios."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

The total VMT is projected to increase by approximately 22%, 30%, and 38% between 2020 and 2050 under the Business-As-Usual Scenario (BAU), Optimistic Scenario, and Transformative Scenario, respectively. These increases in VMT are primarily attributed to projected increases in population and employment, as well as additional induced trips from C/AVs. Furthermore, the expansion of bike and pedestrian infrastructures promotes more trips using those modes in the Optimistic and Transformative Scenarios.

The CUUATS TDM was utilized to measure levels of congestion during peak hours within the urban area roadway network. Levels of congestion were determined based on Volume to Capacity (V/C) ratio values of different roadway segments during the peak hours. A Volume to Capacity ratio compares demand (roadway vehicle volumes) with supply (roadway carrying capacity). The following maps show modeled congested links in baseline year 2020 and projected congested links in 2050 where the volume of traffic on the roadway threatens to exceed (a value of 0.9-1) or exceeds (a value greater than 1) the capacity of the roadway during peak travel times in the MPA.

The baseline map shows areas of congestion for the baseline year 2020. The highest level of congestion is found on a small roadway segment near Market Place Shopping Center. Roads with medium congestion include North Prospect Avenue, the northern Campustown area, and a small section of University Avenue.

{{<image src="VCRatio_baseline2020map.png"
alt="Map shows areas of congestion for the Baseline year 2020."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

The BAU map shows areas of projected congestion for Business-As-Usual 2050. Roads with medium congestion include North Prospect Avenue, North Market Street, Stadium Drive, South Lincoln Avenue, South Vine Street, South Race Street, and areas in downtown Champaign. Roads with the highest congestion include East Green Street, West Vine Street, University Avenue, a roadway segment near Market Place Shopping Center, and a small portion of US 45 near the Village of Tolono. 

{{<image src="VCRatio_BAU2050map.png"
alt="Map shows areas of congestion for the 2050 Business-As-Usual scenario."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

The Optimistic scenario map shows areas of projected congestion for the Optimistic scenario 2050. Roads with medium congestion include North Prospect Avenue, South Lincoln Avenue, North Market Street, South Goodwin Avenue, Stadium Drive, South Race Street, South Vine Street, IL 130, West University Avenue, areas in downtown Champaign, and areas in Campustown. Roads with the highest congestion include a small roadway segment near Marketplace Shopping Mall, east University Avenue, areas in downtown Champaign, and a small portion of US 45 near the Village of Tolono.

{{<image src="VCRatio_optimistic2050map.png"
alt="Map shows areas of congestion for the 2050 Optimistic (Preferred) scenario."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

The Transformative scenario map shows areas of projected congestion for the Transformative scenario. Roads with medium congestion include South Lincoln Avenue, University Avenue, the I-74 ramp from Prospect Avenue, downtown Champaign, Campustown, and a small portion of US 45 near the Village of Tolono. One road expected to experience the highest congestion is a small roadway segment near Market Place Shopping Center.

{{<image src="VCRatio_transformative2050map.png"
alt="Map shows areas of congestion for the 2050 Transformative scenario."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

### Mobile Source Emissions (MOVES)

The [MOtor Vehicle Emission Simulator (MOVES)](https://www.epa.gov/moves)model was developed by the Environmental Protection Agency’s (EPA) Office of Transportation and Air Quality. MOVES estimates emissions for
mobile sources at the national, county, and project level for criteria air pollutants, greenhouse gases, and air toxics. MOVES is required for use by states and metropolitan planning organizations (MPO) where measurements of one or more pollutants exceed the maximum allowable levels under the National Ambient Air Quality Standards (NAAQS) and must be run at the county scale for all non-attainment areas. 

Although the Champaign-Urbana urban area is currently an attainment area for all emissions quality standards, CUUATS staff proactively includes MOVES in the modeling suite to estimate the environmental impact of alternative planning scenarios. This data also allows the region to continually track and better understand how ongoing development affects emissions in order to remain an attainment area. 

Before the LRTP 2045 modeling process officially began in 2016, CUUATS was one of only four agencies from around the country chosen by the EPA to provide critical data for a project looking at air quality and greenhouse gas emissions caused by transportation. This work helped CUUATS staff prepare local MOVES functionality as part of the modeling suite used for the LRTP 2050.

Several calculated assumptions impact the MOVES 2050 outputs including [increased temperature](https://www.vox.com/a/weather-climate-change-us-cities-global-warming), a significant increase in the share of [electric, connected, and autonomous vehicles](https://ccrpc.gitlab.io/lrtp-2050/appendices/tdm/#modeling-for-the-advent-of-electric-connected-and-autonomous-vehicles), and [transportation network recommendations](https://ccrpc.gitlab.io/lrtp-2050/vision/futureprojects/).

The figure below illustrates the modeled annual mobile vehicle emissions in Champaign County. The increase in emission rates of certain pollutants (e.g., N2O) from heavy-duty vehicles for future years (source: https://www.epa.gov/system/files/documents/2023-08/moves4-plan-update-nh3-n2o-webinar-2023-07-20.pdf), along with the increased VMT, leads to a corresponding rise in some pollutants. The increased adoption of electric vehicles in future scenarios is projected to notably reduce total PM2.5 emissions. However, despite varying rates of electric vehicle penetration among the three future scenarios, the differences are not significant. This is because the overall increase in total PM2.5 emissions from vehicle activity and the additional particulates produced by electric vehicles tire wear (due to their increased weight) offset the reductions in exhaust emissions from electric vehicle adoption.

{{<image src="GHGemissions.png"
alt="Chart shows Greenhouse Gas emissions in Champaign County for the four LRTP 2050 scenarios."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

### Access Score

Many of the long-range transportation planning assessment processes are concerned with data and trends that occur at the regional level. While this is beneficial for understanding the overall future direction of the community, it is not localized enough to help identify specific limitations in the
transportation network. To help address this spatial mismatch and make the CUUATS transportation planning and modeling processes more complete, staff
developed a geography neutral, multimodal accessibility assessment known as Access Score. This tool utilizes level of traffic stress (LTS) assessments for
each mode and travel time to calculated accessibility scores to several destination types. These accessibility scores help staff to assess the current
and potential future status of accessibility in the Champaign-Urbana region, to identify areas in need of improvement, and to observe potential benefits from
the construction of new infrastructure.

To develop Access Score, staff utilized an existing bicycle level of traffic stress (BLTS) assessment from the [Mineta Transportation Institute](http://transweb.sjsu.edu/research/low-stress-bicycling-and-network-connectivity), and an existing pedestrian level of traffic stress (PLTS) assessment from the [Oregon Department of Transportation](https://www.oregon.gov/ODOT/Planning/Documents/APMv2_Ch14.pdf). Automobile level of traffic stress (ALTS) is assessed using an in-house analysis created by CUUATS staff to emulate the assessments for BLTS and PLTS by considering elements of the automobile transportation network and its
interactions with other modes. In each of these LTS assessments, network infrastructure characteristics were assessed and assigned a level of stress, one
(1) being the lowest stress and four (4) being the highest. Each segment in the network was assigned an overall level of stress based on the highest, or most
stressful, score it received for any one of the characteristics considered. Transit level of traffic stress (TLTS) is assessed using the Pandana accessibility tool, which uses general transit feed specification (GTFS) and transit headway and schedule data to assess transit trips based on the time required to reach a destination, which is then combined with the pedestrian score required to get from the point of origin to the nearest bus stop.

Once the modal LTS scores were complete, accessibility was calculated by multiplying the LTS scores by the travel time. The assessment includes accessibility to the following ten destination types: grocery stores, health facilities, jobs, parks, public facilities, retail stores, restaurants, schools, arts and entertainment, and services.

Access Score allows not only for the assessment of current accessibility, but also for the analysis of future scenarios. To evaluate future accessibility for future scenarios, staff geocoded all bicycle, pedestrian, and automobile projects recommended in current plans that would impact infrastructure considered in one or more of the LTS scores. Other LTS characteristics were also updated using the relevant TDM scenario outputs. Overall, the addition of new infrastructure from 2020 to 2050 had positive impacts on bicycle and pedestrian access scores, with slight decreases to automobile scores. Transit accessibility increased slightly. This increase can be attributed to improved scores for the pedestrian portions of those trips. The scores for the 2020 and 2050 scenarios can be seen in the Access Score application embedded below.

<iframe src="https://ccrpc.gitlab.io/access-score/"
    width="100%" height="600" allowfullscreen="true">
    Access Score interactive map.
    </iframe>

### Safety Forecasting Tool

Transportation planners and engineers often find it difficult to list and prioritize transportation projects based on safety measures as the required data and tools are not readily available to conduct regular and comprehensive safety evaluations for the region. CUUATS staff developed the Safety Forecasting Tool, which includes a binary crash model to predict the likelihood of a crash, and a multiclass model to predict the likelihood of low crash frequency (no crash), medium crash frequency (at least one crash), or high crash frequency (two or more crashes).

{{<image src="SFT_process_diagram.png"
    alt="Diagram showing Safety Forecasting Tool workflow."
    caption="Diagram showing Safety Forecasting Tool workflow. Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
    attr="CUUATS" attrlink="https://ccrpc.org/"
    position="full">}}

To check the accuracy of the models, crashes for the Champaign-Urbana Metropolitan Planning Area (MPA) for the year 2022 were predicted and then compared with the observed crashes for 2022.

This project also predicted crashes for future scenarios. Three scenarios were used for the year 2050:
* Business As Usual: historic growth trends and mode choices continue.
* Optimistic: Moderate, compact growth with fewer single-occupancy vehicles
* Transformative: Increased core and fringe growth with innovation-based mode choices

Although these scenarios have a lot of elements to them, our models mainly used population and Annual Average Daily Traffic (AADT) of the roadway network for these different scenarios. Crashes and risk factors for different roadways for urban and rural roads in Champaign County were predicted for the above three scenarios. The future population and AADT values for 2050 were used from the CUUATS Land Use Model and Travel Demand Model (TDM). 

Across all three scenarios, a consistent pattern emerges where local roads or streets account for the highest percentage of predicted crashes, while minor collectors represent the lowest crash percentages. This trend is aligned with the general understanding of urban traffic patterns where local roads tend to see more interaction points, pedestrian movement, and complex intersections, contributing to higher crash probabilities. Conversely, minor collectors, serving a more limited traffic volume, experience fewer crashes.

Looking to 2050, the binary model that calculates predicted crash percentages by roadway functional class suggests that all future scenarios will see increases in crashes compared to the 2022 observed crashes. The Transformative Scenario would be the most beneficial for Interstates, Major Arterials, Minor Arterials, Major Collectors. However, Local Roads or Streets, where most crashes occur, would most benefit from implementation of the Optimistic Scenario, which is the preferred scenario in the Long-Range Transportation Plan 2050.

The multiclass model that calculates predicted crash percentages by level of risk suggests that all future scenarios will see increases in low-risk roadways (predicted to have no crashes) and decreases in medium (predicted to have at least one crash) and high risk (predicted to have two or more crashes) roadways. It is apparent that significant improvements in risk will occur no matter which scenario plays out by 2050, but the Transformative scenario shows the most improvement by a slight margin compared to the Business-As-Usual and Optimistic scenarios.

### Comparing the Scenarios

The following table compares the baseline 2020 scenario with the modeled future scenarios. The preferred Optimistic scenario column is highlighted for reference. 

{{<image src="scenarios_comparison.png"
alt="Table shows a comparison of modeling indicators for the four LRTP 2050 scenarios."
caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
attr="CUUATS" attrlink="https://ccrpc.org/"
position="full">}}

