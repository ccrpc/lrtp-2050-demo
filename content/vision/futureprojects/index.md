---
title: "Future Projects"
date: 2024-08-01
draft: false
Weight: 30
bannerHeading: Future Projects
bannerText: >
  This chapter sets forth a vision for each mode of transportation in the
  Metropolitan Planning Area through the year 2050.
---
<style>
 .usa-accordion__heading{
   background-color: #4682B4;
 }
</style>

CUUATS staff used [public input from the first round of LRTP 2050 outreach](https://ccrpc.gitlab.io/lrtp-2050/process/phase-one/), input from local agencies, and local planning documents to establish the LRTP 2050 goals (safety, reliability, sustainability, equity, and connectivity) along with a list of transportation system changes that could contribute to the realization of the goals. Public input regarding the *current* transportation system called
attention to locations in the system that people thought were functioning well and locations where people suggested specific infrastructure improvements. Public input regarding the *future* transportation system came from responses to questions asking people to prioritize their most important factors that will shape our transportation system over the next 25 years. The combined input about the current and future transportation system conveyed strong public support about the future of transportation in the Champaign-Urbana region: a more environmentally sustainable transportation system, additional pedestrian and bicycle infrastructure, shorter off-campus transit times, equitable access to transportation services, and a compact urban area that supports active transportation and limits sprawl development.

Future projects were identified by discussing projects with municipal and other agencies' staff members based on existing community plans, including but not limited to:
* City of Champaign Comprehensive Plan (2021)
* City of Champaign 2025 Capital Improvements Plan for FY 2024/25 – 2033/34
* City of Champaign Bike Implementation Project Descriptions, July 2023
* City of Urbana Comprehensive Plan (draft 2024)
* City of Urbana Capital Improvement Plan FY 2025-2029 (2024)
* City of Urbana Pedestrian Master Plan (2020)
* City of Urbana Bicycle Master Plan (2016)
* Village of Savoy Comprehensive Plan (2019)
* Village of Savoy FY 2024-2025 Annual Budget
* Village of Savoy Bike & Pedestrian Plan (2017)
* Village of Mahomet Comprehensive Plan (2016)
* Village of Mahomet Multi-year Plan FY2024 Budget Book (2023)
* University of Illinois 2024 Capital Summer Projects (2024)
* Champaign County Forest Preserve District Comprehensive Plan (2024)
* Champaign Park District Trails Master Plan (2017)
* Urbana Park District Trails Master Plan (2016)
* Champaign County Greenways & Trails Plan (2014)
* IDOT FY 2025-2030 Proposed Highway & Multimodal Improvement Program
* IDOT FY 2025 Annual Highway Improvement Program
* IDOT FY 2024-2027 Statewide Transportation Improvement Program 
* IDOT Long-Range Transportation Plan 2019<br>

## Future Projects: Regionally Significant Vision Projects

The graphic below highlights the LRTP 2050 goals alongside some of the regionally-significant transportation projects as part of the LRTP 2050 future vision. The regionally-significant projects and collectively-defined goals are connected in the graphic to illustrate how individual improvement projects can complement other projects to improve the overall system over time. Some projects are already funded and in progress while others are considered an illustrative, unfunded part of the LRTP vision for 2050.

{{<image src="LRTP_VisionBoard_2050.png"
  alt="Chart of LRTP 2050 goals listed with regionally significant vision projects and an illustrative map"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

### Project Profiles: Regionally Significant Projects

Project profiles were created for some of the regionally significant projects planned to be constructed prior to 2050. Some of these projects have funding and are slated for construction soon, while others do not yet have funding allocated. All are considered regionally significant because they will improve connectivity, safety, and travel time for pedestrians, bicyclists, transit, and motorists along heavily traveled commuter routes in the region.

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="Curtis Road from Prospect Avenue to First Street"%}}
  <a href="1- Curtis Road Grade Separation.png" target="_blank" >Project Profile: Curtis Road from Prospect Avenue to First Street</a>

- Additional details: 
[Village of Savoy](https://www.savoy.illinois.gov/departments/public_works/curtisroad.php)

  {{%/accordion-content%}}
  {{%accordion-content title="US150/Prospect Avenue from Marketview Drive to Springfield Avenue"%}}
<a href="2- US 150 Prospect Safety Improvements.png" target="_blank" >Project Profile: US150/Prospect Avenue from Marketview Drive to Springfield Avenue</a>

- Additional details: 
[IDOT FY 2025-2030 Proposed Highway & Multimodal Improvement Program](https://idot.illinois.gov/transportation-system/transportation-management/transportation-improvement-programs/myp/publication.html)
  {{%/accordion-content%}}
  {{%accordion-content title="Neil Street from Springfield Avenue to Curtis Road"%}}
<a href="3- Neil Street Improvements.png" target="_blank" >Project Profile: Neil Street from Springfield Avenue to Curtis Road</a>

- Additional details: 
[News Gazette](https://www.news-gazette.com/news/local/roadwork/10-6-million-in-improvements-planned-for-neil-street-between-springfield-and-curtis/article_c367e4db-943b-589f-872b-944888bbf7dd.html?utm_source=ground.news&utm_medium=referral)
 {{%/accordion-content%}}
 {{%accordion-content title="Kirby Avenue from Duncan Road to Staley Road"%}}
<a href="4- Kirby Avenue from Duncan to Staley.png" target="_blank" >Project Profile: Kirby Avenue from Duncan Road to Staley Road</a>

- Additional details: 
[City of Champaign FY 25-34 Capital Improvements Plan](https://documents.ci.champaign.il.us/object?id=1VAIFttelQjSxhUYd7NzlcR-2ktb5zfQF)
 {{%/accordion-content%}}
 {{%accordion-content title="Duncan Road from Springfield Avenue to Kirby Avenue"%}}
<a href="5- Duncan Rd from Springfield Ave to Kirby Ave.png" target="_blank" >Project Profile: Duncan Road from Springfield Avenue to Kirby Avenue</a>

- Additional details: 
[City of Champaign FY 25-34 Capital Improvements Plan](https://documents.ci.champaign.il.us/object?id=1VAIFttelQjSxhUYd7NzlcR-2ktb5zfQF)
  {{%/accordion-content%}}
  {{%accordion-content title="Mattis Avenue from Curtis Road to Windsor Road"%}}
<a href="6- Mattis Ave from Curtis Rd to Windsor Rd.png" target="_blank" >Project Profile: Mattis Avenue from Curtis Road to Windsor Road</a>

- Additional details: 
[City of Champaign FY 25-34 Capital Improvements Plan](https://documents.ci.champaign.il.us/object?id=1VAIFttelQjSxhUYd7NzlcR-2ktb5zfQF)
  {{%/accordion-content%}}
  {{%accordion-content title="Duncan Road from Curtis Road to Meadows West Drive"%}}
<a href="7- Duncan Rd from Curtis Rd to Meadows West Drive.png" target="_blank" >Project Profile: Duncan Road from Curtis Road to Meadows West Drive</a>

- Additional details: 
[City of Champaign FY 25-34 Capital Improvements Plan](https://documents.ci.champaign.il.us/object?id=1VAIFttelQjSxhUYd7NzlcR-2ktb5zfQF)
  {{%/accordion-content%}}
  {{%accordion-content title="Lincoln Avenue from Wascher Drive to Killarney Drive"%}}
<a href="8- N Lincoln Ave from Wascher to Killarney.png" target="_blank" >Project Profile: Lincoln Avenue from Wascher Drive to Killarney Drive</a>

- Additional details: 
[City of Urbana FY 25-29 Capital Improvements Plan](https://urbanaillinois.us/departments/public-works/about-public-works/engineering/capital-improvement-plan)
  {{%/accordion-content%}}
 {{%accordion-content title="Florida Avenue from Wright Street to Hillcrest Drive"%}}
<a href="9- Florida Ave from Wright to Hillcrest.png" target="_blank" >Project Profile: Florida Avenue from Wright Street to Hillcrest Drive</a>

- Additional details: 
[City of Urbana FY 25-29 Capital Improvements Plan](https://urbanaillinois.us/departments/public-works/about-public-works/engineering/capital-improvement-plan)
  {{%/accordion-content%}}
   {{%accordion-content title="Vine Street from Main Street to California Avenue"%}}
<a href="10- Vine St from Main St to California Ave.png" target="_blank" >Project Profile: Vine Street from Main Street to California Avenue</a>

- Additional details: 
[City of Urbana FY 25-29 Capital Improvements Plan](https://urbanaillinois.us/departments/public-works/about-public-works/engineering/capital-improvement-plan)
  {{%/accordion-content%}}
   {{%accordion-content title="Kickapoo Rail Trail from Urbana to Mahomet"%}}
<a href="11- Kickapoo Rail Trail.png" target="_blank" >Project Profile: Kickapoo Rail Trail from Urbana to Mahomet</a>

- Additional details: 
[https://www.onekrt.org/home](https://www.onekrt.org/home)
  {{%/accordion-content%}}
{{</accordion>}}



## Future Projects: Fiscally Constrained

As part of the LRTP, the Federal Highway Administration requires a list of the fiscally constrained projects that are part of the overall vision for the urban area. The fiscally constrained projects are those that have either guaranteed or reasonably guaranteed funding secured for the completion of the project. A separate federally-required document for the region, the Transportation Improvement Program (TIP), lists fiscally-constrained transportation projects anticipated to be constructed in the metropolitan planning area during the next six years. The program reflects the goals, objectives, and recommendations from the 25-year Long Range Transportation Plan (LRTP) in the short term. By mandate, the TIP must include projects receiving federal and state funding; the CUUATS TIP also includes local projects from our member agencies. Compiling the document requires a consensus among all CUUATS agencies, thus establishing compliance with federal laws and eligibility for federal funding. Each year, the Illinois Department of Transportation (IDOT) publishes a fiscally-constrained six-year program that details transportation investments in the state and local highway system. The State of Illinois [Proposed Highway & Multimodal Improvement Program](https://idot.illinois.gov/transportation-system/transportation-management/transportation-improvement-programs/myp/publication.html) for fiscal years 2025 through 2030 was released in June 2024. The local TIP will be amended to reflect additional State-funded transportation projects as they are released. The following table includes a summary of local projects reflected in the current TIP for fiscal years 2023-2028. Visit the online [TIP map and database](https://ccrpc.org/transportation/transportation_improvement_plan_(tip)/transportation_improvement_program_(tip)_fy_2023-2028.php) for more details about these projects and other transportation expenditures in the region.

<rpc-table url="TIP_summary_table.csv"
  table-title="CUUATS TIP Summary"
  text-alignment="c,c"></rpc-table>

Further, the City of Champaign, City of Urbana, Village of Savoy, and
Village of Mahomet identify additional infrastructure maintenance and
improvement projects in their respective Capital Improvement Plans and Programs. 
The table below shows projects that are beyond the timeline of the CUUATS TIP 
but have been slated for funding in the local agency’s Capital Improvement Plan.

<rpc-table url="CIPprojects.csv"
  table-title="Local Agency Projects with Allocated Funding"
  text-alignment="c,c"></rpc-table>

## Future Projects: Local and Unfunded

Each agency in the region has its own set of transportation priorities and goals
to improve mobility in their respective jurisdiction alone and in conjunction
with surrounding jurisdictions. The following list includes local project
priorities for the future that are currently unfunded. The projects are
numbered in the map for identification purposes only and do not indicate
priority.

<iframe src="https://maps.ccrpc.org/lrtp2050-roadway-vision-projects/"
  width="100%" height="600" allowfullscreen="true">
  Map showing roadway vision projects.
</iframe>

<rpc-table url="LocalUnfunded.csv"
  table-title="Future Projects: Local and Unfunded"
  text-alignment="c,c"></rpc-table>

Bicycling and walking projects were identified separately from roadway projects
due to their large volume and small scale. The recommendations from the
following plans were incorporated into the map below:

* 2023 City of Champaign Bike Implementation Project Descriptions, July 2023
* 2020 Urbana Pedestrian Master Plan - approved August 2020
* 2017 Champaign Park District Trails Master Plan (CTMP) – approved June 2017
* 2017 Savoy Bike & Pedestrian Plan – approved April 2017
* 2016 Urbana Bicycle Master Plan (UBMP) – approved December 2016
* 2016 Urbana Park District Trails Master Plan (UTMP) – approved January 2016
* 2014 Champaign County Greenways & Trails Plan – approved June 2014

<iframe src="https://maps.ccrpc.org/lrtp2050-vision-bike-ped/"
  width="100%" height="600" allowfullscreen="true">
  Map of the future pedestrian, bike, and trail projects included in 2017 Champaign Park 
  District Trails Master Plan (CTMP), 2017 Savoy Bike & Pedestrian Plan, 2016 Urbana Bicycle 
  Master Plan (UBMP), 2016 Urbana Park District Trails Master Plan (UTMP), and 2014 Champaign 
  County Greenways & Trails Plan.
</iframe>

The implementation of the local projects described in the tables are
not the responsibility of CUUATS or the Champaign County Regional Planning
Commission. These projects have been included in the LRTP to highlight local
improvement projects that will enhance the quality of the overall transportation
network and will help connect the local transportation network to the planned
regionally significant improvements for the future.